--	Fixed Priest S15 Healer Set not removing Holy Spark Proc
INSERT INTO `spell_linked_spell` (`spell_trigger`, `spell_effect`, `type`, `comment`) VALUES (2061, -131567, 0, 'Remove Holy Spark on Flash Light');
INSERT INTO `spell_linked_spell` (`spell_trigger`, `spell_effect`, `type`, `comment`) VALUES (2060, -131567, 0, 'Remove Holy Spark on Holy Light');
INSERT INTO `spell_linked_spell` (`spell_trigger`, `spell_effect`, `type`, `comment`) VALUES (88684, -131567, 0, 'Remove Holy Spark on Word: Serenity');
