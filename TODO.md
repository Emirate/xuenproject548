## Syloxus' TODO List:

- Test which spells aren't working.
- Script remaining spells/items and fix any existing ones.
- Update defines to MoP as they are out of date.
- Test EVERY battleground and arena completely to make sure they are working correctly. (Important)
- Check which opcodes /aren't/ working or don't have structures yet.
- Complete the faction/race change opcodes.
- Attempt to get Loss of Control opcodes working and the visual on the players screen when they are CC'd. Working with the CC mechanics structure and opcodes. (Not a big priority)
- The pet bar for Hunters is messed up, it gives the Mage's Water Elemental spells on the bar instead of the Hunter's Pet ones, needs looking into.
- Can't logout during a flight path (not too important).
- Complete the LFG/LFR opcode structures and make sure the code is up to MoP standards.
- Redo the database structures in the core, and then change the database to reflect the changes.
- Reduce the database errors.
- In the database and the core, redo Language.h and the 'trinity_string' table, they're out of date and a mess.
- Ret Paladin's passive 'Sanctity of Battle' reduces cooldowns of spells correctly through haste, but when a player teleports to a different map, the effect goes away.
- PvE scripts have been implemented. Need to now test the PvE scripts in every dungeon/raid.
- Also need to check spawns in all dungeons and raids (both normal and heroic modes).
- Redo the staff commands as they are out of date and a mess at the moment.
- Add more useful commands from the other core that we don't have (e.g. '.cheat all').
- Fix any crashes that are found in the crash logs.
- Completely redo the quest system up to MoP standards, and look into any other systems that need redoing.
_______________________________________________________________________________________________

## Denis' TODO List:

# General:

- Cross-Realm Feature
- Virtual-Realm Names
- Creature Cooldown System
- BNET Login System
- In-Game Shop Feature

# PvP:

- Battleground Zone
- Arena Zone
- Loss of Control
- War Games Feature
- Pet Battle System

# PvE:

- Auction House
- LFG System
- RDF System
- Scenario System
- Game Events
- Archeology System
- Instances/Dungeons/Raids:
                            - Vanilla
                            - The Burning Crusade
                            - Wrath of the Lich King
                            - Cataclysm
                            - Mists of Pandaria

							
							test