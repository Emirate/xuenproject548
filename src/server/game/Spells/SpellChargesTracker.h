/*
* Copyright (C) 2008-2016 TrinityCore <http://www.trinitycore.org/>
*
* This program is free software; you can redistribute it and/or modify it
* under the terms of the GNU General Public License as published by the
* Free Software Foundation; either version 3 of the License, or (at your
* option) any later version.
*
* This program is distributed in the hope that it will be useful, but WITHOUT
* ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
* FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
* more details.
*
* You should have received a copy of the GNU General Public License along
* with this program. If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef TrinityCore_GAME_SPELL_CHARGES_TRACKER_H
#define TrinityCore_GAME_SPELL_CHARGES_TRACKER_H

#include <cstdint>
#include <unordered_map>

namespace Trinity
{
    class SpellChargesTracker final
    {
        struct ChargeData final
        {
            std::uint32_t consumedCharges;
            std::uint32_t currRegenTimer;
            std::uint32_t baseRegenTimer;
        };

        public:
            void consume(std::uint32_t spellId, std::uint32_t regenTimer);

            std::uint32_t consumedCount(std::uint32_t spellId) const;

            void update(std::uint32_t diff);

        private:
            std::unordered_map<std::uint32_t, ChargeData> spellChargesMap_;
    };

    inline std::uint32_t SpellChargesTracker::consumedCount(std::uint32_t spellId) const
    {
        auto const i = spellChargesMap_.find(spellId);
        return (i != spellChargesMap_.end()) ? i->second.consumedCharges : 0;
    }
}
// namespace Trinity

#endif // TrinityCore_GAME_SPELL_CHARGES_TRACKER_H
