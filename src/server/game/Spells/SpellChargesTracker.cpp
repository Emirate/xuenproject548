/*
* Copyright (C) 2008-2016 TrinityCore <http://www.trinitycore.org/>
*
* This program is free software; you can redistribute it and/or modify it
* under the terms of the GNU General Public License as published by the
* Free Software Foundation; either version 3 of the License, or (at your
* option) any later version.
*
* This program is distributed in the hope that it will be useful, but WITHOUT
* ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
* FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
* more details.
*
* You should have received a copy of the GNU General Public License along
* with this program. If not, see <http://www.gnu.org/licenses/>.
*/

#include "SpellChargesTracker.h"

namespace Trinity
{
    void SpellChargesTracker::consume(std::uint32_t spellId, std::uint32_t regenTimer)
    {
        auto const res = spellChargesMap_.insert(std::make_pair(spellId, ChargeData()));
        auto &chargeData = res.first->second;

        ++chargeData.consumedCharges;

        if (res.second)
        {
            chargeData.currRegenTimer = regenTimer;
            chargeData.baseRegenTimer = regenTimer;
        }
    }

    void SpellChargesTracker::update(std::uint32_t diff)
    {
        for (auto i = spellChargesMap_.begin(); i != spellChargesMap_.end();)
        {
            auto &chargeData = i->second;

            // First, wait till it's time to replenish consumed charge
            if (chargeData.currRegenTimer > diff)
            {
                chargeData.currRegenTimer -= diff;
                ++i;
                continue;
            }

            // If we replenish last consumed charge, simply erase charge data
            if (--chargeData.consumedCharges == 0)
            {
                i = spellChargesMap_.erase(i);
                continue;
            }

            // Otherwise, we have to start countdown again
            std::uint32_t const rem = diff - chargeData.currRegenTimer;
            chargeData.currRegenTimer = chargeData.baseRegenTimer - rem;
            ++i;
        }
    }
}
