/*
 * Copyright (C) 2008-2016 TrinityCore <http://www.trinitycore.org/>
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 3 of the License, or (at your
 * option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once

#include "Log.h"

#define sSpellRegulator SpellRegulator::instance()

class SpellRegulator
{
public:
    static SpellRegulator* instance()
    {
        static SpellRegulator instance;
        return &instance;
    }

    void Regulate(uint32& damage, uint32 spellId)
    {
        if (RegulatorContainer.find(spellId) == RegulatorContainer.end())
            return;


        float val = RegulatorContainer[spellId];

        if (!val || val == 100.0f)
            return;

        damage = (damage / 100.0f) * val;
    }

    void LoadFromDB()
    {
        RegulatorContainer.clear();
        uint32 msTime = getMSTime();
        QueryResult result = WorldDatabase.Query("SELECT * FROM spell_regulator");

        if (!result)
            return;

        uint32 count = 0;
        do{
            Field* fields = result->Fetch();
            RegulatorContainer[fields[0].GetUInt32()] = fields[1].GetFloat();
            ++count;
        }
        while (result->NextRow());

        TC_LOG_INFO("server.loading", "Loaded %u regulated spells in %u ms", count, GetMSTimeDiffToNow(msTime));
    }

private:
    std::unordered_map<uint32, float> RegulatorContainer; // spellid, percentage
};

