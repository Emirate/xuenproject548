/*
 * Copyright (C) 2008-2016 TrinityCore <http://www.trinitycore.org/>
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 3 of the License, or (at your
 * option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#include "BuildBooster/BuildBooster.h"
#include "Transport.h"
#include "Battleground.h"
#include "WaypointMovementGenerator.h"
#include "InstanceSaveMgr.h"
#include "MovementStructures.h"
#include "BattlePetMgr.h"
#include "Vehicle.h"
#include "WardenWin.h"
#include "Appender.h"

// Movement Anticheat defines
//#define ANTICHEAT_DEBUG
#define ANTICHEAT_EXCEPTION_INFO

#define MOVEMENT_PACKET_TIME_DELAY 0

void WorldSession::HandleMoveWorldportAckOpcode(WorldPacket & /*recvData*/)
{
    HandleMoveWorldportAckOpcode();
}

void WorldSession::HandleMoveWorldportAckOpcode()
{
    // Ignore unexpected far teleports
    if (!GetPlayer()->IsBeingTeleportedFar())
        return;

    GetPlayer()->SetSemaphoreTeleportFar(false);
    GetPlayer()->SetIgnoreMovementCount(5);

    // Get the teleport destination
    WorldLocation const& loc = GetPlayer()->GetTeleportDest();

    // Possible errors in the coordinate validity check
    if (!MapManager::IsValidMapCoord(loc))
    {
        LogoutPlayer(false);
        return;
    }

    // Get the destination map entry, not the current one, this will fix homebind and reset greeting
    MapEntry const* mEntry = sMapStore.LookupEntry(loc.GetMapId());
    InstanceTemplate const* mInstance = sObjectMgr->GetInstanceTemplate(loc.GetMapId());

    // Reset instance validity, except if going to an instance inside an instance
    if (GetPlayer()->m_InstanceValid == false && !mInstance)
        GetPlayer()->m_InstanceValid = true;

    Map* oldMap = GetPlayer()->GetMap();
    
    if (GetPlayer()->IsInWorld())
        oldMap->RemovePlayerFromMap(GetPlayer(), false);

    // Relocate the player to the teleport destination
    Map* newMap = sMapMgr->CreateMap(loc.GetMapId(), GetPlayer());
    // The CanEnter checks are done in TeleporTo but conditions may change
    // While the player is in transit, for example the map may get full
    if (!newMap || !newMap->CanEnter(GetPlayer()))
    {
        GetPlayer()->TeleportTo(GetPlayer()->m_homebindMapId, GetPlayer()->m_homebindX, GetPlayer()->m_homebindY, GetPlayer()->m_homebindZ, GetPlayer()->GetOrientation());
        return;
    }
    else
        GetPlayer()->Relocate(&loc);

    GetPlayer()->ResetMap();
    GetPlayer()->SetMap(newMap);

    GetPlayer()->SendInitialPacketsBeforeAddToMap();
    if (!GetPlayer()->GetMap()->AddPlayerToMap(GetPlayer()))
    {
        GetPlayer()->ResetMap();
        GetPlayer()->SetMap(oldMap);
        GetPlayer()->TeleportTo(GetPlayer()->m_homebindMapId, GetPlayer()->m_homebindX, GetPlayer()->m_homebindY, GetPlayer()->m_homebindZ, GetPlayer()->GetOrientation());
        return;
    }

    // Battleground state prepare (in case join to BG), at relogin/tele player not invited
    // Only add to bg group and object, if the player was invited (else he entered through command)
    if (_player->InBattleground())
    {
        // Cleanup setting if outdated
        if (!mEntry->IsBattlegroundOrArena())
        {
            // We're not in BG
            _player->SetBattlegroundId(0, BATTLEGROUND_TYPE_NONE);
            // reset destination bg team
            _player->SetBGTeam(0);
            _player->SetByteValue(PLAYER_FIELD_ARENA_FACTION, 3, 0);
        }
        // Join to bg case
        else if (Battleground* bg = _player->GetBattleground())
        {
            if (_player->IsInvitedForBattlegroundInstance(_player->GetBattlegroundId()))
                bg->AddPlayer(_player);
        }
    }

    GetPlayer()->SendInitialPacketsAfterAddToMap();

    // Flight fast teleport case
    if (GetPlayer()->GetMotionMaster()->GetCurrentMovementGeneratorType() == FLIGHT_MOTION_TYPE)
    {
        if (!_player->InBattleground())
        {
            // Short preparations to continue flight
            FlightPathMovementGenerator* flight = (FlightPathMovementGenerator*)(GetPlayer()->GetMotionMaster()->top());
            flight->Initialize(GetPlayer());
            return;
        }

        // Battleground state prepare, stop flight
        GetPlayer()->GetMotionMaster()->MovementExpired();
        GetPlayer()->CleanupAfterTaxiFlight();
    }

    // resurrect character at enter into instance where his corpse exist after add to map
    Corpse* corpse = GetPlayer()->GetCorpse();
    if (corpse && corpse->GetType() != CORPSE_BONES && corpse->GetMapId() == GetPlayer()->GetMapId())
    {
        if (mEntry->IsDungeon())
        {
            GetPlayer()->ResurrectPlayer(0.5f, false);
            GetPlayer()->SpawnCorpseBones();
        }
    }

    bool allowMount = !mEntry->IsDungeon() || mEntry->IsBattlegroundOrArena();
    if (mInstance)
    {
        Difficulty diff = GetPlayer()->GetDifficulty(mEntry->IsRaid());
        if (MapDifficulty const* mapDiff = GetMapDifficultyData(mEntry->MapID, diff))
        {
            if (mapDiff->resetTime)
            {
                if (time_t timeReset = sInstanceSaveMgr->GetResetTimeFor(mEntry->MapID, diff))
                {
                    uint32 timeleft = uint32(timeReset - time(NULL));
                    GetPlayer()->SendInstanceResetWarning(mEntry->MapID, diff, timeleft, true);
                }
            }
        }
        allowMount = mInstance->AllowMount;
    }

    // Mount allow check
    if (!allowMount)
        _player->RemoveAurasByType(SPELL_AURA_MOUNTED);

    // Update zone immediately, otherwise leave channel will cause crash in mtmap
    uint32 newzone, newarea;
    GetPlayer()->GetZoneAndAreaId(newzone, newarea);
    GetPlayer()->UpdateZone(newzone, newarea);

    for (uint8 i = 0; i < 9; ++i)
        GetPlayer()->UpdateSpeed(UnitMoveType(i), true);

    // Honorless target
    if (GetPlayer()->pvpInfo.IsHostile)
        GetPlayer()->CastSpell(GetPlayer(), 2479, true);

    // In friendly area
    else if (GetPlayer()->IsPvP() && !GetPlayer()->HasFlag(PLAYER_FIELD_PLAYER_FLAGS, PLAYER_FLAGS_IN_PVP))
        GetPlayer()->UpdatePvP(false, false);

    // Resummon pet
    GetPlayer()->ResummonPetTemporaryUnSummonedIfAny();

    // Resummon battle pet
    GetPlayer()->GetBattlePetMgr()->ResummonLastBattlePet();

    // Lets process all delayed operations on successful teleport
    GetPlayer()->ProcessDelayedOperations();
}

void WorldSession::HandleMoveTeleportAck(WorldPacket& recvPacket)
{
    //TC_LOG_DEBUG("network", "CMSG_MOVE_TELEPORT_ACK");

    ObjectGuid guid;
    uint32 flags, time;
    recvPacket >> time >> flags;

    recvPacket.ReadGuidMask(guid, 0, 7, 3, 5, 4, 6, 1, 2);

    recvPacket.ReadGuidBytes(guid, 4, 1, 6, 7, 0, 2, 5, 3);

    Player* plMover = _player->m_mover->ToPlayer();

    if (!plMover || !plMover->IsBeingTeleportedNear())
        return;

    if (guid != plMover->GetGUID())
        return;

    plMover->SetSemaphoreTeleportNear(false);
    plMover->SetIgnoreMovementCount(5);

    uint32 old_zone = plMover->GetZoneId();

    WorldLocation const& dest = plMover->GetTeleportDest();

    plMover->UpdatePosition(dest, true);

    uint32 newzone, newarea;
    plMover->GetZoneAndAreaId(newzone, newarea);
    plMover->UpdateZone(newzone, newarea);

    // New zone
    if (old_zone != newzone)
    {
        // Honorless target
        if (plMover->pvpInfo.IsHostile)
            plMover->CastSpell(plMover, 2479, true);

        // In friendly area
        else if (plMover->IsPvP() && !plMover->HasFlag(PLAYER_FIELD_PLAYER_FLAGS, PLAYER_FLAGS_IN_PVP))
            plMover->UpdatePvP(false, false);
    }

    // Resummon pet
    GetPlayer()->ResummonPetTemporaryUnSummonedIfAny();

    // Resummon battle pet
    GetPlayer()->GetBattlePetMgr()->ResummonLastBattlePet();

    // Lets process all delayed operations on successful teleport
    GetPlayer()->ProcessDelayedOperations();
}

void WorldSession::HandleMovementOpcodes(WorldPacket& recvPacket)
{
    uint16 opcode = recvPacket.GetOpcode();

    Unit* mover = _player->m_mover;

    ASSERT(mover != NULL);                        // There must always be a mover

    Player* plrMover = mover->ToPlayer();

    // Ignore, waiting processing in WorldSession::HandleMoveWorldportAckOpcode and WorldSession::HandleMoveTeleportAck
    if (plrMover && plrMover->IsBeingTeleported())
    {
        recvPacket.rfinish();                     // Prevent warnings spam
        return;
    }

    if (sWorld->getBoolConfig(CONFIG_MMAPS_SPLINE_LOCKING) && plrMover && plrMover->IsSplineEnabled() && !plrMover->movespline->Finalized())
    {
        recvPacket.rfinish();                     // Prevent warnings spam
        return;
    }

    // Sometime, client send movement packet after teleporation with position before teleportation, so we ignore 3 first movement packet after teleporation
    // TODO: find a better way to check that, may be a new CMSG send by client ?
    /*if (plrMover && plrMover->GetIgnoreMovementCount() && opcode != CMSG_CAST_SPELL)
    {
        plrMover->SetIgnoreMovementCount(plrMover->GetIgnoreMovementCount() - 1);
        recvPacket.rfinish();                     // Prevent warnings spam
        return;
    }*/

    /* Extract packet */
    MovementInfo movementInfo;
    GetPlayer()->ReadMovementInfo(recvPacket, &movementInfo);

    if (!mover)
        return;

    // Prevent tampered movement data
    if (movementInfo.guid != mover->GetGUID())
    {
        TC_LOG_ERROR("network", "HandleMovementOpcodes: guid error");
        return;
    }

    if (!movementInfo.pos.IsPositionValid())
    {
        TC_LOG_ERROR("network", "HandleMovementOpcodes: Invalid Position");
        return;
    }

    /// Currently causes crashes, commented until a fix is done
    // Hackfix for flying movement system
    //if ((plrMover->NoFlyingArea(plrMover->GetAreaId()) || plrMover->NoFlyingZone(plrMover->GetZoneId())) && !plrMover->IsGameMaster())
        //plrMover->SetCanFly(false);

    /* Handle special cases */
    if (movementInfo.transport.guid)
    {
        // Transports size limited.
        // Also received at zeppelin leave by some reason with transport.* as absolute in continent coordinates, can be safely skipped.
        if (movementInfo.transport.pos.GetPositionX() > 50 || movementInfo.transport.pos.GetPositionY() > 50 || movementInfo.transport.pos.GetPositionZ() > 50)
        {
            recvPacket.rfinish();                 // Prevent warnings spam
            return;
        }

        if (!Trinity::IsValidMapCoord(movementInfo.pos.GetPositionX() + movementInfo.transport.pos.GetPositionX(), movementInfo.pos.GetPositionY() + movementInfo.transport.pos.GetPositionY(), movementInfo.pos.GetPositionZ() + movementInfo.transport.pos.GetPositionZ(), movementInfo.pos.GetOrientation() + movementInfo.transport.pos.GetOrientation()))
        {
            recvPacket.rfinish();                 // Prevent warnings spam
            return;
        }

        // If we boarded a transport, add us to it
        if (plrMover)
        {
            if (!plrMover->GetTransport())
            {
                // Elevators also cause the client to send MOVEMENTFLAG_ONTRANSPORT - just dismount if the guid can be found in the transport list
                for (MapManager::TransportSet::const_iterator iter = sMapMgr->m_Transports.begin(); iter != sMapMgr->m_Transports.end(); ++iter)
                {
                    if ((*iter)->GetGUID() == movementInfo.transport.guid)
                    {
                        plrMover->m_transport = *iter;
                        (*iter)->AddPassenger(plrMover);
                        break;
                    }
                }
            }
            else if (plrMover->GetTransport()->GetGUID() != movementInfo.transport.guid)
            {
                bool foundNewTransport = false;
                plrMover->m_transport->RemovePassenger(plrMover);
                for (MapManager::TransportSet::const_iterator iter = sMapMgr->m_Transports.begin(); iter != sMapMgr->m_Transports.end(); ++iter)
                {
                    if ((*iter)->GetGUID() == movementInfo.transport.guid)
                    {
                        foundNewTransport = true;
                        plrMover->m_transport = *iter;
                        (*iter)->AddPassenger(plrMover);
                        break;
                    }
                }

                if (!foundNewTransport)
                {
                    plrMover->m_transport = NULL;
                    movementInfo.transport.pos.Relocate(0.0f, 0.0f, 0.0f, 0.0f);
                    movementInfo.transport.time = 0;
                    movementInfo.transport.seat = -1;
                }
            }
        }

        if (!mover->GetTransport() && !mover->GetVehicle())
        {
            GameObject* go = mover->GetMap()->GetGameObject(movementInfo.transport.guid);
            if (!go || go->GetGoType() != GAMEOBJECT_TYPE_TRANSPORT)
                movementInfo.transport.guid = 0;
        }
    }
    else if (plrMover && plrMover->GetTransport())                // If we were on a transport, leave
    {
        plrMover->m_transport->RemovePassenger(plrMover);
        plrMover->m_transport = NULL;
        movementInfo.transport.pos.Relocate(0.0f, 0.0f, 0.0f, 0.0f);
        movementInfo.transport.time = 0;
        movementInfo.transport.seat = -1;
        movementInfo.transport.guid = 0LL;
    }

    // Fall damage generation (ignore in flight case that can be triggered also at lags in moment teleportation to another map).
    if (opcode == MSG_MOVE_FALL_LAND && plrMover && !plrMover->IsInFlight())
        plrMover->HandleFall(movementInfo);

    // Now client does not include swimming flag in case of jumping under water.
    if (plrMover && ((movementInfo.flags & MOVEMENTFLAG_SWIMMING) != 0) != plrMover->IsInWater())
        plrMover->SetInWater(!plrMover->IsInWater() || plrMover->GetBaseMap()->IsUnderWater(movementInfo.pos.GetPositionX(), movementInfo.pos.GetPositionY(), movementInfo.pos.GetPositionZ()));

    //if (plrMover)
    //    sAnticheatMgr->StartHackDetection(plrMover, movementInfo, opcode);

    uint32 mstime = getMSTime();
    if (m_clientTimeDelay == 0)
        m_clientTimeDelay = mstime - movementInfo.time;

    movementInfo.time = movementInfo.time + m_clientTimeDelay + MOVEMENT_PACKET_TIME_DELAY;
    movementInfo.guid = mover->GetGUID();
    mover->m_movementInfo = movementInfo;

    // This is almost never true (not sure why it is sometimes, but it is), normally use mover->IsVehicle()
    if (mover->GetVehicle())
    {
        mover->SetOrientation(movementInfo.pos.GetOrientation());
        return;
    }

    mover->UpdatePosition(movementInfo.pos);

    /* Process position-change */
    WorldPacket data(SMSG_PLAYER_MOVE, recvPacket.size());
    //movementInfo.Alive32 = movementInfo.time; // Hack, but it works on 5.0.5 in this way...
    mover->WriteMovementInfo(data);
    mover->SendMessageToSet(&data, _player);
    
    if (plrMover)                                            // Nothing is charmed, or player charmed
    {
        // Clear unit emote state.
        plrMover->ClearEmotes();

        plrMover->UpdateFallInformationIfNeed(movementInfo, opcode);

        AreaTableEntry const* zone = GetAreaEntryByAreaID(plrMover->GetAreaId());
        float depth = zone ? zone->MaxDepth : -500.0f;
        if (movementInfo.pos.GetPositionZ() < depth)
        {
            if (!(plrMover->GetBattleground() && plrMover->GetBattleground()->HandlePlayerUnderMap(_player)))
            {
                // NOTE: This is actually called many times while falling, even after the player has been teleported away.
                // TODO: Discard movement packets after the player is rooted.
                if (plrMover->IsAlive())
                {
                    plrMover->EnvironmentalDamage(DAMAGE_FALL_TO_VOID, GetPlayer()->GetMaxHealth());
                    // Player can be alive if GM/etc. change the death state to CORPSE to prevent the death timer from starting in the next player update
                    if (!plrMover->IsAlive())
                        plrMover->KillPlayer();
                }
            }
        }
    }
}

void WorldSession::HandleForceSpeedChangeAck(WorldPacket &recvData)
{
    uint32 opcode = recvData.GetOpcode();

    /* Extract packet */
    MovementInfo movementInfo;
    static MovementStatusElements const speedElement = MSEExtraFloat;
    Movement::ExtraMovementStatusElement extras(&speedElement);
    GetPlayer()->ReadMovementInfo(recvData, &movementInfo, &extras);

    // Now can skip not our packet
    if (_player->GetGUID() != movementInfo.guid)
    {
        recvData.rfinish();                   // Prevent warnings spam
        return;
    }

    float newspeed = extras.Data.floatData;

    // Client ACK send one packet for mounted/run case and need skip all except last from its
    // In other cases anti-cheat check can be fail in false case
    UnitMoveType move_type       = MOVE_WALK;
    UnitMoveType force_move_type = MOVE_WALK;

    static char const* move_type_name[MAX_MOVE_TYPE] = { "Walk", "Run", "RunBack", "Swim", "SwimBack", "TurnRate", "Flight", "FlightBack", "PitchRate" };

    // Skip all forced speed changes except last and unexpected
    // In run/mounted case used one ACK and it must be skipped.m_forced_speed_changes[MOVE_RUN} store both.
    if (_player->m_forced_speed_changes[force_move_type] > 0)
    {
        --_player->m_forced_speed_changes[force_move_type];
        if (_player->m_forced_speed_changes[force_move_type] > 0)
            return;
    }

    if (!_player->GetTransport() && fabs(_player->GetSpeed(move_type) - newspeed) > 0.01f)
    {
        if (_player->GetSpeed(move_type) > newspeed)         // Must be greater - just correct
            _player->SetSpeed(move_type, _player->GetSpeedRate(move_type), true);
        else                                                 // Must be lesser - cheating
            _player->GetSession()->KickPlayer();
    }
}

void WorldSession::HandleSetActiveMoverOpcode(WorldPacket& recvPacket)
{
    //TC_LOG_DEBUG("network", "WORLD: Recvd CMSG_SET_ACTIVE_MOVER");

    ObjectGuid guid;   

    recvPacket.ReadBit();

    recvPacket.ReadGuidMask(guid, 3, 0, 2, 1, 5, 4, 7, 6);

    recvPacket.ReadGuidBytes(guid, 3, 4, 5, 2, 7, 0, 1, 6);

    /*if (guid && GetPlayer()->IsInWorld())
    {
        if (_player->m_mover->GetGUID() != guid)
            TC_LOG_DEBUG("network", "HandleSetActiveMoverOpcode: incorrect mover guid: mover is " UI64FMTD " (%s - Entry: %u) and should be " UI64FMTD, uint64(guid), GetLogNameForGuid(guid), GUID_ENPART(guid), _player->m_mover->GetGUID());
    }*/
}

void WorldSession::HandleMoveNotActiveMover(WorldPacket &recvData)
{
    //TC_LOG_DEBUG("network", "WORLD: Recvd CMSG_MOVE_NOT_ACTIVE_MOVER");

    MovementInfo mi;
    GetPlayer()->ReadMovementInfo(recvData, &mi);
    _player->m_movementInfo = mi;
}

void WorldSession::HandleMountSpecialAnimOpcode(WorldPacket& /*recvData*/)
{
    ObjectGuid guid = GetPlayer()->GetGUID();

    WorldPacket data(SMSG_MOUNT_SPECIAL_ANIM, 1 + 8);
    
    data.WriteGuidMask(guid, 5, 7, 0, 3, 2, 1, 4, 6);

    data.WriteGuidBytes(guid, 7, 2, 0, 4, 5, 6, 1, 3);

    GetPlayer()->SendMessageToSet(&data, false);
}

void WorldSession::HandleMoveKnockBackAck(WorldPacket& recvData)
{
    //TC_LOG_DEBUG("network", "CMSG_MOVE_KNOCK_BACK_ACK");

    MovementInfo movementInfo;
    GetPlayer()->ReadMovementInfo(recvData, &movementInfo);

    if (_player->m_mover->GetGUID() != movementInfo.guid)
        return;

    // Save movement flags
    _player->SetUnitMovementFlags(movementInfo.flags);

    _player->m_movementInfo = movementInfo;
    _player->m_anti_Last_HSpeed = movementInfo.jump.xyspeed;
    _player->m_anti_Last_VSpeed = movementInfo.jump.zspeed < 3.2f ? movementInfo.jump.zspeed - 1.0f : 3.2f;

    const uint32 dt = (_player->m_anti_Last_VSpeed < 0) ? int(ceil(_player->m_anti_Last_VSpeed / -25) * 1000) : int(ceil(_player->m_anti_Last_VSpeed / 25) * 1000);
    _player->m_anti_LastSpeedChangeTime = movementInfo.time + dt + 1000;

    WorldPacket data(SMSG_MOVE_UPDATE_KNOCK_BACK, 66);
    _player->WriteMovementInfo(data);
    _player->SendMessageToSet(&data, false);
}

void WorldSession::HandleMoveHoverAck(WorldPacket& recvData)
{
    //TC_LOG_DEBUG("network", "CMSG_MOVE_HOVER_ACK");

    uint64 guid;                                            // Guid - unused
    recvData.readPackGUID(guid);

    recvData.read_skip<uint32>();                           // Unk

    MovementInfo movementInfo;
    GetPlayer()->ReadMovementInfo(recvData, &movementInfo);

    // SAVE MOVEMENT FLAGS
    _player->SetUnitMovementFlags(movementInfo.flags);

    _player->m_movementInfo = movementInfo;
    _player->m_anti_Last_HSpeed = movementInfo.jump.xyspeed;
    _player->m_anti_Last_VSpeed = movementInfo.jump.zspeed < 3.2f ? movementInfo.jump.zspeed - 1.0f : 3.2f;

    const uint32 dt = (_player->m_anti_Last_VSpeed < 0) ? int(ceil(_player->m_anti_Last_VSpeed / -25) * 1000) : int(ceil(_player->m_anti_Last_VSpeed / 25) * 1000);
    _player->m_anti_LastSpeedChangeTime = movementInfo.time + dt + 1000;

    recvData.read_skip<uint32>();                           // Unk2
}

void WorldSession::HandleMoveWaterWalkAck(WorldPacket& recvData)
{
    //TC_LOG_DEBUG("network", "CMSG_MOVE_WATER_WALK_ACK");

    uint64 guid;                                            // Guid - unused
    recvData.readPackGUID(guid);

    recvData.read_skip<uint32>();                           // Unk

    MovementInfo movementInfo;
    GetPlayer()->ReadMovementInfo(recvData, &movementInfo);

    recvData.read_skip<uint32>();                          // Unk2
}

void WorldSession::HandleMoveSetFly(WorldPacket& recvData)
{
    // TODO: Find out what are unknown booleans and use ReadMovementInfo
    //TC_LOG_DEBUG("network", "CMSG_MOVE_SET_FLY");
    ObjectGuid playerGuid;
    ObjectGuid transportGuid;

    recvData.read_skip<float>(); // Position Y
    recvData.read_skip<float>(); // Position Z
    recvData.read_skip<float>(); // Position X
    playerGuid[5] = recvData.ReadBit();
    bool hasTransportData = recvData.ReadBit();
    playerGuid[3] = recvData.ReadBit();
    uint32 unkCounter = recvData.ReadBits(22);
    bool unkBool2 = !recvData.ReadBit();
    recvData.ReadBit();
    bool unkBool3 = recvData.ReadBit();
    playerGuid[6] = recvData.ReadBit();
    recvData.ReadBit();
    playerGuid[7] = recvData.ReadBit();
    bool unkBool4 = !recvData.ReadBit();
    recvData.ReadGuidMask(playerGuid, 0, 2);
    bool unkBool5 = !recvData.ReadBit();
    bool unkBool11 = !recvData.ReadBit();
    playerGuid[1] = recvData.ReadBit();
    recvData.ReadBit();
    bool unkBool12 = !recvData.ReadBit();
    bool unkBool6 = !recvData.ReadBit();
    playerGuid[4] = recvData.ReadBit();
    bool hasMovementFlag = !recvData.ReadBit();

    bool unkBool8, unkBool9 = false;
    if (hasTransportData)
    {
        recvData.ReadGuidMask(transportGuid, 1, 3, 5);
        unkBool8 = recvData.ReadBit();
        recvData.ReadGuidMask(transportGuid, 6, 7, 2, 4);
        unkBool9 = recvData.ReadBit();
        transportGuid[0] = recvData.ReadBit();
    }

    if (unkBool2)
        recvData.ReadBits(13);

    bool unkBool10 = false;
    if (unkBool3)
        unkBool10 = recvData.ReadBit();

    if (hasMovementFlag)
        _player->SetUnitMovementFlags(recvData.ReadBits(30));

    recvData.ReadGuidBytes(playerGuid, 1, 6, 5, 2, 4, 0, 7, 3);

    for (uint32 i = 0; i < unkCounter; i++)
        recvData.read_skip<uint32>();

    if (hasTransportData)
    {
        recvData.ReadGuidBytes(transportGuid, 7, 5, 1);
        recvData.read_skip<uint32>();
        recvData.read_skip<uint8>();

        if (unkBool9)
            recvData.read_skip<uint32>();

        recvData.ReadGuidBytes(transportGuid, 0, 4);
        recvData.read_skip<float>();
        recvData.ReadByteSeq(transportGuid[3]);
        recvData.read_skip<float>();
        recvData.ReadGuidBytes(transportGuid, 2, 6);
        recvData.read_skip<float>();
        recvData.read_skip<float>();

        if (unkBool8)
            recvData.read_skip<uint32>();
    }

    if (unkBool3)
    {
        if (unkBool10)
        {
            recvData.read_skip<float>();
            recvData.read_skip<float>();
            recvData.read_skip<float>();
        }

        recvData.read_skip<float>();
        recvData.read_skip<uint32>();
    }

    if (unkBool4)
        recvData.read_skip<uint32>();

    if (unkBool6)
        recvData.read_skip<uint32>();

    if (unkBool5)
        recvData.read_skip<float>();

    if (unkBool11)
        recvData.read_skip<float>();

    if (unkBool12)
        recvData.read_skip<float>();
}

void WorldSession::HandleSummonResponseOpcode(WorldPacket& recvData)
{
    if (!_player->IsAlive() || _player->IsInCombat())
        return;

    ObjectGuid SummonerGUID;

    SummonerGUID[1] = recvData.ReadBit();
    SummonerGUID[3] = recvData.ReadBit();
    SummonerGUID[5] = recvData.ReadBit();
    SummonerGUID[2] = recvData.ReadBit();
    bool Accept = recvData.ReadBit();
    SummonerGUID[7] = recvData.ReadBit();
    SummonerGUID[0] = recvData.ReadBit();
    SummonerGUID[4] = recvData.ReadBit();
    SummonerGUID[6] = recvData.ReadBit();

    recvData.ReadByteSeq(SummonerGUID[0]);
    recvData.ReadByteSeq(SummonerGUID[1]);
    recvData.ReadByteSeq(SummonerGUID[6]);
    recvData.ReadByteSeq(SummonerGUID[3]);
    recvData.ReadByteSeq(SummonerGUID[5]);
    recvData.ReadByteSeq(SummonerGUID[4]);
    recvData.ReadByteSeq(SummonerGUID[2]);
    recvData.ReadByteSeq(SummonerGUID[7]);

    _player->SummonIfPossible(Accept);
}

void WorldSession::HandleSetCollisionHeightAck(WorldPacket& recvPacket)
{
    //TC_LOG_DEBUG("network", "CMSG_MOVE_SET_COLLISION_HEIGHT_ACK");

    static MovementStatusElements const heightElements[] = { MSEExtraFloat, MSEExtra2Bits };
    Movement::ExtraMovementStatusElement extra(heightElements);
    MovementInfo movementInfo;
    GetPlayer()->ReadMovementInfo(recvPacket, &movementInfo, &extra);
}
