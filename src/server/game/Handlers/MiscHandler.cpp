/*
 * Copyright (C) 2008-2016 TrinityCore <http://www.trinitycore.org/>
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 3 of the License, or (at your
 * option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#include "BuildBooster/BuildBooster.h"
#include "GossipDef.h"
#include "GuildMgr.h"
#include "BigNumber.h"
#include "SHA1.h"
#include "UpdateData.h"
#include "LootMgr.h"
#include "Chat.h"
#include "zlib.h"
#include "Item.h"
#include "Battleground.h"
#include "OutdoorPvP.h"
#include "ReputationMgr.h"
#include "SocialMgr.h"
#include "CellImpl.h"
#include "AccountMgr.h"
#include "Vehicle.h"
#include "CreatureAI.h"
#include "DBCEnums.h"
#include "ScriptMgr.h"
#include "InstanceScript.h"
#include "GameObjectAI.h"
#include "AccountMgr.h"
#include "BattlegroundMgr.h"
#include "BattleShop.h"
#include "Battlefield.h"
#include "BattlefieldMgr.h"
#include "DB2Stores.h"

void WorldSession::HandleRepopRequestOpcode(WorldPacket& recvData)
{
    //TC_LOG_DEBUG("network", "WORLD: Recvd CMSG_REPOP_REQUEST Message");

    recvData.read_skip<uint8>();

    if (GetPlayer()->IsAlive() || GetPlayer()->HasFlag(PLAYER_FIELD_PLAYER_FLAGS, PLAYER_FLAGS_GHOST))
        return;

    if (GetPlayer()->HasAuraType(SPELL_AURA_PREVENT_RESURRECTION))
        return; // Silently return, client should display the error by itself

    // The world update order is sessions, players, creatures
    // The netcode runs in parallel with all of these
    // Creatures can kill players
    // So if the server is lagging enough the player can
    // Release spirit after he's killed but before he is updated
    if (GetPlayer()->getDeathState() == JUST_DIED)
    {
        //TC_LOG_DEBUG("network", "HandleRepopRequestOpcode: Got request after player %s(%d) was killed and before he was updated", GetPlayer()->GetName().c_str(), GetPlayer()->GetGUIDLow());
        GetPlayer()->KillPlayer();
    }

    // This is spirit release confirm?
    GetPlayer()->RemovePet(NULL, PET_SLOT_OTHER_PET, true, true);
    GetPlayer()->BuildPlayerRepop();
    GetPlayer()->RepopAtGraveyard();
}

void WorldSession::HandleGossipSelectOptionOpcode(WorldPacket& recvData)
{
    //TC_LOG_DEBUG("network", "WORLD: CMSG_GOSSIP_SELECT_OPTION");

    ObjectGuid guid;
    uint32 gossipListId;
    uint32 menuId;
    uint8 boxTextLength = 0;
    std::string code = "";

    recvData >> gossipListId >> menuId;

    recvData.ReadGuidMask(guid, 3, 0, 1, 4, 7, 5, 6);
    boxTextLength = recvData.ReadBits(8);
    guid[2] = recvData.ReadBit();

    recvData.ReadGuidBytes(guid, 7, 3, 4, 6, 0, 5);

    if (_player->PlayerTalkClass->IsGossipOptionCoded(gossipListId))
        code = recvData.ReadString(boxTextLength);

    recvData.ReadGuidBytes(guid, 2, 1);

    Creature* unit = NULL;
    GameObject* go = NULL;
    if (IS_CRE_OR_VEH_GUID(guid))
    {
        unit = GetPlayer()->GetNPCIfCanInteractWith(guid, UNIT_NPC_FLAG_NONE);
        if (!unit)
        {
            //TC_LOG_DEBUG("network", "WORLD: HandleGossipSelectOptionOpcode - Unit (GUID: %u) not found or you can't interact with him.", uint32(GUID_LOPART(guid)));
            return;
        }
    }
    else if (IS_GAMEOBJECT_GUID(guid))
    {
        go = _player->GetMap()->GetGameObject(guid);
        if (!go)
        {
            //TC_LOG_DEBUG("network", "WORLD: HandleGossipSelectOptionOpcode - GameObject (GUID: %u) not found.", uint32(GUID_LOPART(guid)));
            return;
        }
    }
    else
    {
        //TC_LOG_DEBUG("network", "WORLD: HandleGossipSelectOptionOpcode - unsupported GUID type for highguid %u. lowpart %u.", uint32(GUID_HIPART(guid)), uint32(GUID_LOPART(guid)));
        return;
    }

    // remove fake death
    if (GetPlayer()->HasUnitState(UNIT_STATE_DIED))
        GetPlayer()->RemoveAurasByType(SPELL_AURA_FEIGN_DEATH);

    if ((unit && unit->GetCreatureTemplate()->ScriptID != unit->LastUsedScriptID) || (go && go->GetGOInfo()->ScriptId != go->LastUsedScriptID))
    {
        //TC_LOG_DEBUG("network", "WORLD: HandleGossipSelectOptionOpcode - Script reloaded while in use, ignoring and set new scipt id");
        if (unit)
            unit->LastUsedScriptID = unit->GetCreatureTemplate()->ScriptID;
        if (go)
            go->LastUsedScriptID = go->GetGOInfo()->ScriptId;
        _player->PlayerTalkClass->SendCloseGossip();
        return;
    }
    if (!code.empty())
    {
        if (unit)
        {
            unit->AI()->sGossipSelectCode(_player, menuId, gossipListId, code.c_str());
            if (!sScriptMgr->OnGossipSelectCode(_player, unit, _player->PlayerTalkClass->GetGossipOptionSender(gossipListId), _player->PlayerTalkClass->GetGossipOptionAction(gossipListId), code.c_str()))
                _player->OnGossipSelect(unit, gossipListId, menuId);
        }
        else
        {
            go->AI()->GossipSelectCode(_player, menuId, gossipListId, code.c_str());
            sScriptMgr->OnGossipSelectCode(_player, go, _player->PlayerTalkClass->GetGossipOptionSender(gossipListId), _player->PlayerTalkClass->GetGossipOptionAction(gossipListId), code.c_str());
        }
    }
    else
    {
        if (unit)
        {
            unit->AI()->sGossipSelect(_player, menuId, gossipListId);
            if (!sScriptMgr->OnGossipSelect(_player, unit, _player->PlayerTalkClass->GetGossipOptionSender(gossipListId), _player->PlayerTalkClass->GetGossipOptionAction(gossipListId)))
                _player->OnGossipSelect(unit, gossipListId, menuId);
        }
        else
        {
            go->AI()->GossipSelect(_player, menuId, gossipListId);
            if (!sScriptMgr->OnGossipSelect(_player, go, _player->PlayerTalkClass->GetGossipOptionSender(gossipListId), _player->PlayerTalkClass->GetGossipOptionAction(gossipListId)))
                _player->OnGossipSelect(go, gossipListId, menuId);
        }
    }
}

void WorldSession::HandleWhoOpcode(WorldPacket& recvData)
{
    //CMSG_WHO
    //TC_LOG_DEBUG("network", "WORLD: Recvd CMSG_WHO Message");

    time_t pNow = time(NULL);
    if (pNow - timerWhoOpcode < 7)
    {
        ++countWhoOpcode;
        if (countWhoOpcode >= 3)
            return;
    }
    else
    {
        timerWhoOpcode = pNow;
        countWhoOpcode = 1;
    }

    uint32 levelMin, levelMax, raceMask, classMask, zonesCount, wordCount;
    uint32 zoneIds[10];                                     // 10 is client limit
    bool requestServerInfo, showEnemies, showArenaPlayers, exactName;
    uint8 playerLen, guildLen, realmNameLen, guildRealmNameLen;
    std::string playerName, guildName, realmName, guildRealmName;

    recvData >> classMask;                                  // Race mask
    recvData >> raceMask;                                   // Minimal player level, default 100 (MAX_LEVEL)
    recvData >> levelMax;                                   // Maximal player level, default 0
    recvData >> levelMin;                                   // Class mask

    showEnemies = recvData.ReadBit();
    exactName = recvData.ReadBit();
    requestServerInfo = recvData.ReadBit();
    guildRealmNameLen = recvData.ReadBits(9);
    showArenaPlayers = recvData.ReadBit();
    playerLen = recvData.ReadBits(6);

    zonesCount = recvData.ReadBits(4);                      // Zones count, client limit = 10 (2.0.10)
    if (zonesCount > 10)                                    // Can't be received from real client or broken packet
        return;

    realmNameLen = recvData.ReadBits(9);
    guildLen = recvData.ReadBits(7);

    wordCount = recvData.ReadBits(3);
    if (wordCount > 4)
        return;                                             // Can't be received from real client or broken packet

    uint8* wordLens = new uint8[wordCount];
    std::string* words = new std::string[wordCount];

    for (uint8 i = 0; i < wordCount; i++)
        wordLens[i] = recvData.ReadBits(7);

    recvData.FlushBits();

    std::wstring wWords[4];
    for (uint32 i = 0; i < wordCount; ++i)
    {
        std::string temp;
        recvData >> temp;                                   // User entered string, it used as universal search pattern(guild+player name)?

        if (!Utf8toWStr(temp, wWords[i]))
            continue;

        wstrToLower(wWords[i]);

        //TC_LOG_DEBUG("network", "String %u: %s", i, temp.c_str());
    }

    guildRealmName = recvData.ReadString(guildRealmNameLen);

    for (uint32 i = 0; i < zonesCount; ++i)
    {
        recvData >> zoneIds[i];                             // Zone id, 0 if zone is unknown...
        //TC_LOG_DEBUG("network", "Zone %u: %u", i, zoneIds[i]);
    }

    // NYI
    playerName = recvData.ReadString(playerLen);
    realmName = recvData.ReadString(realmNameLen);
    guildName = recvData.ReadString(guildLen);

    uint32 virtualRealmAddress = 0;
    int32 faction = 0, locale = 0;

    if (requestServerInfo)
    {
        recvData >> locale;
        recvData >> virtualRealmAddress;
        recvData >> faction;
    }

    //TC_LOG_DEBUG("network", "Minlvl %u, maxlvl %u, name %s, guild %s, racemask %u, classmask %u, zones %u, strings %u", levelMin, levelMax, playerName.c_str(), guildName.c_str(), raceMask, classMask, zonesCount, wordCount);

    std::wstring wPlayerName, wGuildName;
    if (!(Utf8toWStr(playerName, wPlayerName) && Utf8toWStr(guildName, wGuildName)))
        return;

    wstrToLower(wPlayerName);
    wstrToLower(wGuildName);

    // SMSG_WHO
    // Client send in case not set max level value 100 but Trinity supports 255 max level,
    // Update it to show GMs with characters after 100 level
    if (levelMax >= MAX_LEVEL)
        levelMax = STRONG_MAX_LEVEL;

    uint32 team = _player->GetTeam();
    uint32 security = GetSecurity();
    //bool allowTwoSideWhoList = sWorld->getBoolConfig(CONFIG_ALLOW_TWO_SIDE_WHO_LIST);
    uint32 gmLevelInWhoList  = sWorld->getIntConfig(CONFIG_GM_LEVEL_IN_WHO_LIST);
    uint8 displaycount = 0, matchcount = 0;

    ByteBuffer bytesData;
    WorldPacket data(SMSG_WHO);

    size_t pos = data.bitwpos();
    data.WriteBits(displaycount, 6);

    TRINITY_READ_GUARD(HashMapHolder<Player>::LockType, *HashMapHolder<Player>::GetLock());
    HashMapHolder<Player>::MapType const& m = sObjectAccessor->GetPlayers();
    for (HashMapHolder<Player>::MapType::const_iterator itr = m.begin(); itr != m.end(); ++itr)
    {
        Player* target = itr->second;
        // Player can see member of other team only if CONFIG_ALLOW_TWO_SIDE_WHO_LIST
        if (target->GetTeam() != team && !HasPermission(rbac::RBAC_PERM_TWO_SIDE_WHO_LIST))
            continue;

        // Player can see MODERATOR, GAME MASTER, ADMINISTRATOR only if CONFIG_GM_IN_WHO_LIST
        if (!HasPermission(rbac::RBAC_PERM_WHO_SEE_ALL_SEC_LEVELS) && target->GetSession()->GetSecurity() > AccountTypes(gmLevelInWhoList))
            continue;

        // Do not process players which are not in world
        if (!target->IsInWorld())
            continue;

        // Check if target is globally visible for player
        if (!target->IsVisibleGloballyFor(_player))
            continue;

        // Check if target's level is in level range
        uint8 level = target->getLevel();
        if (level < levelMin || level > levelMax)
            continue;

        // Check if class matches classmask
        uint8 class_ = target->getClass();
        if (!(classMask & (1 << class_)))
            continue;

        // Check if race matches racemask
        uint32 race = target->getRace();
        if (!(raceMask & (1 << race)))
            continue;

        uint32 zoneId = target->GetZoneId();
        uint8 gender = target->getGender();

        bool z_show = true;
        for (uint32 i = 0; i < zonesCount; ++i)
        {
            if (zoneIds[i] == zoneId)
            {
                z_show = true;
                break;
            }

            z_show = false;
        }
        if (!z_show)
            continue;

        std::string pname = target->GetName();
        std::wstring wpname;
        if (!Utf8toWStr(pname, wpname))
            continue;
        wstrToLower(wpname);

        if (!(wPlayerName.empty() || wpname.find(wPlayerName) != std::wstring::npos))
            continue;

        std::string gname = sGuildMgr->GetGuildNameById(target->GetGuildId());
        std::wstring wgname;
        if (!Utf8toWStr(gname, wgname))
            continue;
        wstrToLower(wgname);

        if (!(wGuildName.empty() || wgname.find(wGuildName) != std::wstring::npos))
            continue;

        std::string aname;
        if (AreaTableEntry const* areaEntry = GetAreaEntryByAreaID(zoneId))
            aname = areaEntry->area_name[GetSessionDbcLocale()];

        bool s_show = true;
        for (uint32 i = 0; i < wordCount; ++i)
        {
            if (!wWords[i].empty())
            {
                if (wgname.find(wWords[i]) != std::wstring::npos ||
                    wpname.find(wWords[i]) != std::wstring::npos ||
                    Utf8FitTo(aname, wWords[i]))
                {
                    s_show = true;
                    break;
                }
                s_show = false;
            }
        }
        if (!s_show)
            continue;

        // 49 is maximum player count sent to client - can be overridden through config, but is unstable
        if ((matchcount++) >= sWorld->getIntConfig(CONFIG_MAX_WHO))
            continue;

        ObjectGuid playerGuid = target->GetGUID();
        ObjectGuid accountId = target->GetSession()->GetAccountId();
        ObjectGuid guildGuid = target->GetGuild() ? target->GetGuild()->GetGUID() : 0;

        data.WriteBit(accountId[2]);
        data.WriteBit(playerGuid[2]);
        data.WriteBit(accountId[7]);
        data.WriteBit(guildGuid[5]);
        data.WriteBits(gname.size(), 7);
        data.WriteGuidMask(accountId, 1, 5);
        data.WriteBit(guildGuid[7]);
        data.WriteBit(playerGuid[5]);
        data.WriteBit(false);
        data.WriteBit(guildGuid[1]);
        data.WriteBit(playerGuid[6]);
        data.WriteBit(guildGuid[2]);
        data.WriteBit(playerGuid[4]);
        data.WriteGuidMask(guildGuid, 0, 3);
        data.WriteBit(accountId[6]);
        data.WriteBit(false);
        data.WriteBit(playerGuid[1]);
        data.WriteBit(guildGuid[4]);
        data.WriteBit(accountId[0]);

        if (DeclinedName const* names = itr->second->GetDeclinedNames())
        {
            for (uint8 i = 0; i < MAX_DECLINED_NAME_CASES; ++i)
                data.WriteBits(names->name[i].size(), 14);
        }
        else
        {
            for (uint8 i = 0; i < MAX_DECLINED_NAME_CASES; ++i)
                data.WriteBits(0, 7);
        }

        data.WriteBit(playerGuid[3]);
        data.WriteBit(guildGuid[6]);
        data.WriteBit(playerGuid[0]);
        data.WriteGuidMask(accountId, 4, 3);
        data.WriteBit(playerGuid[7]);
        data.WriteBits(pname.size(), 6);

        bytesData.WriteByteSeq(playerGuid[1]);
        bytesData << uint32(realmID);
        bytesData.WriteByteSeq(playerGuid[7]);
        bytesData << uint32(realmID);
        bytesData.WriteByteSeq(playerGuid[4]);
        bytesData.WriteString(pname);
        bytesData.WriteByteSeq(guildGuid[1]);
        bytesData.WriteByteSeq(playerGuid[0]);
        bytesData.WriteGuidBytes(guildGuid, 2, 0, 4);
        bytesData.WriteByteSeq(playerGuid[3]);
        bytesData.WriteByteSeq(guildGuid[6]);
        bytesData << uint32(target->GetSession()->GetAccountId());
        bytesData.WriteString(gname);
        bytesData.WriteByteSeq(guildGuid[3]);
        bytesData.WriteByteSeq(accountId[4]);
        bytesData << uint8(class_);
        bytesData.WriteByteSeq(accountId[7]);
        bytesData.WriteGuidBytes(playerGuid, 6, 2);

        if (DeclinedName const* names = itr->second->GetDeclinedNames())
            for (uint8 i = 0; i < MAX_DECLINED_NAME_CASES; ++i)
                bytesData.WriteString(names->name[i]);

        bytesData.WriteGuidBytes(accountId, 2, 3);
        bytesData << uint8(race);
        bytesData.WriteByteSeq(guildGuid[7]);
        bytesData.WriteGuidBytes(accountId, 1, 5, 6);
        bytesData.WriteByteSeq(playerGuid[5]);
        bytesData.WriteByteSeq(accountId[0]);
        bytesData << uint8(gender);
        bytesData.WriteByteSeq(guildGuid[5]);
        bytesData << uint8(level);
        bytesData << int32(zoneId);

        ++displaycount;
    }

    data.FlushBits();
    data.PutBits(pos, displaycount, 6);
    data.append(bytesData);

    SendPacket(&data);

    delete [] words, wordLens;

    //TC_LOG_DEBUG("network", "WORLD: Send SMSG_WHO Message");
}

void WorldSession::HandleLogoutRequestOpcode(WorldPacket& /*recvData*/)
{
    //TC_LOG_DEBUG("network", "WORLD: Recvd CMSG_LOGOUT_REQUEST Message, security - %u", GetSecurity());

    if (uint64 lguid = GetPlayer()->GetLootGUID())
        DoLootRelease(lguid);

    bool instantLogout = (GetPlayer()->HasFlag(PLAYER_FIELD_PLAYER_FLAGS, PLAYER_FLAGS_RESTING) && !GetPlayer()->IsInCombat()) || GetPlayer()->IsInFlight() || GetSecurity() >= AccountTypes(sWorld->getIntConfig(CONFIG_INSTANT_LOGOUT));

    /// TODO: Possibly add RBAC permission to log out in combat
    bool canLogoutInCombat = GetPlayer()->HasFlag(PLAYER_FIELD_PLAYER_FLAGS, PLAYER_FLAGS_RESTING);

    uint32 reason = 0;
    if (GetPlayer()->IsInCombat() && !canLogoutInCombat)
        reason = 1;
    else if (GetPlayer()->IsFalling())
        reason = 3;                                           // Is jumping or falling
    else if (GetPlayer()->duel || GetPlayer()->HasAura(9454)) // Is dueling or frozen by GM via freeze command
        reason = 2;                                           // FIXME - Need the correct value

    WorldPacket data(SMSG_LOGOUT_RESPONSE, 1+4);
    data << uint32(reason);
    data.WriteBit(instantLogout);
    data.FlushBits();
    SendPacket(&data);

    if (reason)
    {
        LogoutRequest(0);
        return;
    }

    // Instant logout in taverns/cities or on taxi or for admins, gm's, mod's if its enabled in worldserver.conf
    if (instantLogout)
    {
        LogoutPlayer(true);
        return;
    }

    // Not set flags if player can't free move to prevent lost state at logout cancel
    if (GetPlayer()->CanFreeMove())
    {
        GetPlayer()->SetStandState(UNIT_STAND_STATE_SIT);
        GetPlayer()->SetRooted(true);
        GetPlayer()->SetFlag(UNIT_FIELD_FLAGS, UNIT_FLAG_STUNNED);
    }

    LogoutRequest(time(NULL));
}

void WorldSession::HandlePlayerLogoutOpcode(WorldPacket& /*recvData*/)
{
    //TC_LOG_DEBUG("network", "WORLD: Recvd CMSG_PLAYER_LOGOUT Message");
}

void WorldSession::HandleLogoutCancelOpcode(WorldPacket& /*recvData*/)
{
    //TC_LOG_DEBUG("network", "WORLD: Recvd CMSG_LOGOUT_CANCEL Message");

    // Player have already logged out serverside, too late to cancel
    if (!GetPlayer())
        return;

    LogoutRequest(0);

    WorldPacket data(SMSG_LOGOUT_CANCEL_ACK, 0);
    SendPacket(&data);

    // Not remove flags if can't free move - its not set in Logout request code.
    if (GetPlayer()->CanFreeMove())
    {
        // We can move again
        GetPlayer()->SetRooted(false);

        // Stand Up
        GetPlayer()->SetStandState(UNIT_STAND_STATE_STAND);

        // DISABLE_ROTATE
        GetPlayer()->RemoveFlag(UNIT_FIELD_FLAGS, UNIT_FLAG_STUNNED);
    }

    //TC_LOG_DEBUG("network", "WORLD: Sent SMSG_LOGOUT_CANCEL_ACK Message");
}

void WorldSession::HandleSetPvP(WorldPacket& recvData)
{
    if (recvData.size() == 1)
    {
        bool newPvPStatus;
        newPvPStatus = recvData.ReadBit();
        GetPlayer()->ApplyModFlag(PLAYER_FIELD_PLAYER_FLAGS, PLAYER_FLAGS_IN_PVP, newPvPStatus);
        GetPlayer()->ApplyModFlag(PLAYER_FIELD_PLAYER_FLAGS, PLAYER_FLAGS_PVP_TIMER, !newPvPStatus);
    }
}
void WorldSession::HandleTogglePvP(WorldPacket& recvData)
{
    GetPlayer()->ToggleFlag(PLAYER_FIELD_PLAYER_FLAGS, PLAYER_FLAGS_IN_PVP);
    GetPlayer()->ToggleFlag(PLAYER_FIELD_PLAYER_FLAGS, PLAYER_FLAGS_PVP_TIMER);

    if (GetPlayer()->HasFlag(PLAYER_FIELD_PLAYER_FLAGS, PLAYER_FLAGS_IN_PVP))
    {
        if (!GetPlayer()->IsPvP() || GetPlayer()->pvpInfo.EndTimer)
            GetPlayer()->UpdatePvP(true, true);
    }
    else
    {
        if (!GetPlayer()->pvpInfo.IsHostile && GetPlayer()->IsPvP())
            GetPlayer()->pvpInfo.EndTimer = time(NULL);     // Start toggle-off
    }

    //if (OutdoorPvP* pvp = _player->GetOutdoorPvP())
    //    pvp->HandlePlayerActivityChanged(_player);
}

void WorldSession::HandleZoneUpdateOpcode(WorldPacket& recvData)
{
    uint32 newZone;
    recvData >> newZone;

    //TC_LOG_DEBUG("network", "WORLD: Recvd ZONE_UPDATE: %u", newZone);

    // Use server side data, but only after update the player position. See Player::UpdatePosition().
    uint32 newzone, newarea;
    GetPlayer()->GetZoneAndAreaId(newzone, newarea);
    GetPlayer()->UpdateZone(newzone, newarea);
    //GetPlayer()->SendInitWorldStates(true, newZone);
}

void WorldSession::HandleReturnToGraveyard(WorldPacket& /*recvPacket*/)
{
    if (GetPlayer()->IsAlive() || !GetPlayer()->HasFlag(PLAYER_FIELD_PLAYER_FLAGS, PLAYER_FLAGS_GHOST))
        return;

    GetPlayer()->RepopAtGraveyard();
}

void WorldSession::HandleSetSelectionOpcode(WorldPacket& recvData)
{
    ObjectGuid guid;

    recvData.ReadGuidMask(guid, 7, 6, 5, 4, 3, 2, 1, 0);

    recvData.ReadGuidBytes(guid, 0, 7, 3, 5, 1, 4, 6, 2);

    _player->SetSelection(guid);
}

void WorldSession::HandleStandStateChangeOpcode(WorldPacket& recvData)
{
    //TC_LOG_DEBUG("network", "WORLD: Received CMSG_STANDSTATECHANGE"); -- too many spam in log at lags/debug stop

    uint32 animstate;
    recvData >> animstate;

    _player->SetStandState(animstate);
}

void WorldSession::HandleContactListOpcode(WorldPacket& recvData)
{
    recvData.read_skip<uint32>(); // Always 1
    //TC_LOG_DEBUG("network", "WORLD: Received CMSG_CONTACT_LIST");
    _player->GetSocial()->SendSocialList(_player);
}

void WorldSession::HandleAddFriendOpcode(WorldPacket& recvData)
{
    //TC_LOG_DEBUG("network", "WORLD: Received CMSG_ADD_FRIEND");

    std::string friendName = GetTrinityString(LANG_FRIEND_IGNORE_UNKNOWN);
    std::string friendNote;

    recvData >> friendName;
    recvData >> friendNote;

    if (!normalizePlayerName(friendName))
        return;

    //TC_LOG_DEBUG("network", "WORLD: %s asked to add friend : '%s'", GetPlayer()->GetName().c_str(), friendName.c_str());

    PreparedStatement* stmt = CharacterDatabase.GetPreparedStatement(CHAR_SEL_GUID_RACE_ACC_BY_NAME);

    stmt->setString(0, friendName);

    _addFriendCallback.SetParam(friendNote);
    _addFriendCallback.SetFutureResult(CharacterDatabase.AsyncQuery(stmt));
}

void WorldSession::HandleAddFriendOpcodeCallBack(PreparedQueryResult result, std::string const& friendNote)
{
    if (!GetPlayer())
        return;

    uint64 friendGuid;
    uint32 friendAccountId;
    uint32 team;
    FriendsResult friendResult;

    friendResult = FRIEND_NOT_FOUND;
    friendGuid = 0;

    if (result)
    {
        Field* fields = result->Fetch();

        friendGuid = MAKE_NEW_GUID(fields[0].GetUInt32(), 0, HIGHGUID_PLAYER);
        team = Player::TeamForRace(fields[1].GetUInt8());
        friendAccountId = fields[2].GetUInt32();

        if (HasPermission(rbac::RBAC_PERM_ALLOW_GM_FRIEND) || sAccountMgr->IsPlayerAccount(sAccountMgr->GetSecurity(friendAccountId, realmID)))
        {
            if (friendGuid)
            {
                if (friendGuid == GetPlayer()->GetGUID())
                    friendResult = FRIEND_SELF;
                else if (GetPlayer()->GetTeam() != team && !HasPermission(rbac::RBAC_PERM_TWO_SIDE_ADD_FRIEND))
                    friendResult = FRIEND_ENEMY;
                else if (GetPlayer()->GetSocial()->HasFriend(GUID_LOPART(friendGuid)))
                    friendResult = FRIEND_ALREADY;
                else
                {
                    Player* pFriend = ObjectAccessor::FindPlayer(friendGuid);
                    if (pFriend && pFriend->IsVisibleGloballyFor(GetPlayer()))
                        friendResult = FRIEND_ADDED_ONLINE;
                    else
                        friendResult = FRIEND_ADDED_OFFLINE;
                    if (!GetPlayer()->GetSocial()->AddToSocialList(GUID_LOPART(friendGuid), false))
                    {
                        friendResult = FRIEND_LIST_FULL;
                        //TC_LOG_DEBUG("network", "WORLD: %s's friend list is full.", GetPlayer()->GetName().c_str());
                    }
                }
                GetPlayer()->GetSocial()->SetFriendNote(GUID_LOPART(friendGuid), friendNote);
            }
        }
    }

    sSocialMgr->SendFriendStatus(GetPlayer(), friendResult, GUID_LOPART(friendGuid), false);

    //TC_LOG_DEBUG("network", "WORLD: Sent (SMSG_FRIEND_STATUS)");
}

void WorldSession::HandleDelFriendOpcode(WorldPacket& recvData)
{
    uint64 FriendGUID;

    //TC_LOG_DEBUG("network", "WORLD: Received CMSG_DEL_FRIEND");

    recvData >> FriendGUID;

    _player->GetSocial()->RemoveFromSocialList(GUID_LOPART(FriendGUID), false);

    sSocialMgr->SendFriendStatus(GetPlayer(), FRIEND_REMOVED, GUID_LOPART(FriendGUID), false);

    //TC_LOG_DEBUG("network", "WORLD: Sent motd (SMSG_FRIEND_STATUS)");
}

void WorldSession::HandleAddIgnoreOpcode(WorldPacket& recvData)
{
    //TC_LOG_DEBUG("network", "WORLD: Received CMSG_ADD_IGNORE");

    std::string ignoreName = GetTrinityString(LANG_FRIEND_IGNORE_UNKNOWN);

    recvData >> ignoreName;

    if (!normalizePlayerName(ignoreName))
        return;

    //TC_LOG_DEBUG("network", "WORLD: %s asked to Ignore: '%s'", GetPlayer()->GetName().c_str(), ignoreName.c_str());

    PreparedStatement* stmt = CharacterDatabase.GetPreparedStatement(CHAR_SEL_GUID_BY_NAME);

    stmt->setString(0, ignoreName);

    _addIgnoreCallback = CharacterDatabase.AsyncQuery(stmt);
}

void WorldSession::HandleAddIgnoreOpcodeCallBack(PreparedQueryResult result)
{
    if (!GetPlayer())
        return;

    uint64 IgnoreGuid;
    FriendsResult ignoreResult;

    ignoreResult = FRIEND_IGNORE_NOT_FOUND;
    IgnoreGuid = 0;

    if (result)
    {
        IgnoreGuid = MAKE_NEW_GUID((*result)[0].GetUInt32(), 0, HIGHGUID_PLAYER);

        if (IgnoreGuid)
        {
            if (IgnoreGuid == GetPlayer()->GetGUID())              // Not add yourself
                ignoreResult = FRIEND_IGNORE_SELF;
            else if (GetPlayer()->GetSocial()->HasIgnore(GUID_LOPART(IgnoreGuid)))
                ignoreResult = FRIEND_IGNORE_ALREADY;
            else
            {
                ignoreResult = FRIEND_IGNORE_ADDED;

                // ignore list full
                if (!GetPlayer()->GetSocial()->AddToSocialList(GUID_LOPART(IgnoreGuid), true))
                    ignoreResult = FRIEND_IGNORE_FULL;
            }
        }
    }

    sSocialMgr->SendFriendStatus(GetPlayer(), ignoreResult, GUID_LOPART(IgnoreGuid), false);

    //TC_LOG_DEBUG("network", "WORLD: Sent (SMSG_FRIEND_STATUS)");
}

void WorldSession::HandleDelIgnoreOpcode(WorldPacket& recvData)
{
    uint64 IgnoreGUID;

    //TC_LOG_DEBUG("network", "WORLD: Received CMSG_DEL_IGNORE");

    recvData >> IgnoreGUID;

    _player->GetSocial()->RemoveFromSocialList(GUID_LOPART(IgnoreGUID), true);

    sSocialMgr->SendFriendStatus(GetPlayer(), FRIEND_IGNORE_REMOVED, GUID_LOPART(IgnoreGUID), false);

    //TC_LOG_DEBUG("network", "WORLD: Sent motd (SMSG_FRIEND_STATUS)");
}

void WorldSession::HandleSetContactNotesOpcode(WorldPacket& recvData)
{
    //TC_LOG_DEBUG("network", "CMSG_SET_CONTACT_NOTES");
    uint64 guid;
    std::string note;
    recvData >> guid >> note;
    _player->GetSocial()->SetFriendNote(GUID_LOPART(guid), note);
}

void WorldSession::HandleBugOpcode(WorldPacket& recvData)
{
    uint32 suggestion, contentlen, typelen;
    std::string content, type;

    recvData >> suggestion >> contentlen;
    content = recvData.ReadString(contentlen);

    recvData >> typelen;
    type = recvData.ReadString(typelen);

    if (suggestion == 0)
        TC_LOG_DEBUG("network", "WORLD: Received CMSG_BUG [Bug Report]");
    else
        TC_LOG_DEBUG("network", "WORLD: Received CMSG_BUG [Suggestion]");

    //TC_LOG_DEBUG("network", "%s", type.c_str());
    //TC_LOG_DEBUG("network", "%s", content.c_str());

    PreparedStatement* stmt = CharacterDatabase.GetPreparedStatement(CHAR_INS_BUG_REPORT);

    stmt->setString(0, type);
    stmt->setString(1, content);

    CharacterDatabase.Execute(stmt);
}

void WorldSession::HandleReclaimCorpseOpcode(WorldPacket& recvData)
{
    //TC_LOG_DEBUG("network", "WORLD: Received CMSG_RECLAIM_CORPSE");
    
    ObjectGuid guid;
    
    recvData.ReadGuidMask(guid, 1, 5, 7, 2, 6, 3, 0, 4);
        
    recvData.ReadGuidBytes(guid, 2, 5, 4, 6, 1, 0, 7, 3);

    if (GetPlayer()->IsAlive())
        return;

    // Do not allow corpse reclaim in arena
    if (GetPlayer()->InArena())
        return;

    // Body not released yet
    if (!GetPlayer()->HasFlag(PLAYER_FIELD_PLAYER_FLAGS, PLAYER_FLAGS_GHOST))
        return;

    Corpse* corpse = GetPlayer()->GetCorpse();

    if (!corpse)
        return;

    // Prevent resurrect before 30-sec delay after body release not finished
    if (time_t(corpse->GetGhostTime() + GetPlayer()->GetCorpseReclaimDelay(corpse->GetType() == CORPSE_RESURRECTABLE_PVP)) > time_t(time(NULL)))
        return;

    if (!corpse->IsWithinDistInMap(GetPlayer(), CORPSE_RECLAIM_RADIUS, true))
        return;

    // Resurrect
    GetPlayer()->ResurrectPlayer(GetPlayer()->InBattleground() ? 1.0f : 0.5f);

    // Spawn bones
    GetPlayer()->SpawnCorpseBones();
}

void WorldSession::HandleResurrectResponseOpcode(WorldPacket& recvData)
{
    //TC_LOG_DEBUG("network", "WORLD: Received CMSG_RESURRECT_RESPONSE");

    ObjectGuid guid;
    uint32 status;

    recvData >> status;

    guid[3] = recvData.ReadBit();
    guid[0] = recvData.ReadBit();
    guid[6] = recvData.ReadBit();
    guid[4] = recvData.ReadBit();
    guid[5] = recvData.ReadBit();
    guid[2] = recvData.ReadBit();
    guid[1] = recvData.ReadBit();
    guid[7] = recvData.ReadBit();

    recvData.ReadByteSeq(guid[7]);
    recvData.ReadByteSeq(guid[0]);
    recvData.ReadByteSeq(guid[1]);
    recvData.ReadByteSeq(guid[3]);
    recvData.ReadByteSeq(guid[4]);
    recvData.ReadByteSeq(guid[6]);
    recvData.ReadByteSeq(guid[2]);
    recvData.ReadByteSeq(guid[5]);

    if (GetPlayer()->IsAlive())
        return;

    if (status == 1)
    {
        GetPlayer()->ClearResurrectRequestData();           // Reject
        return;
    }

    if (!GetPlayer()->IsRessurectRequestedBy(guid))
        return;

    GetPlayer()->ResurectUsingRequestData();
}

void WorldSession::SendAreaTriggerMessage(const char* Text, ...)
{
    va_list ap;
    char szStr [1024];
    szStr[0] = '\0';

    va_start(ap, Text);
    vsnprintf(szStr, 1024, Text, ap);
    va_end(ap);

    uint32 length = strlen(szStr)+1;
    WorldPacket data(SMSG_AREA_TRIGGER_MESSAGE, 4+length);
    data << length;
    data << szStr;
    SendPacket(&data);
}

void WorldSession::HandleAreaTriggerOpcode(WorldPacket& recvData)
{
    uint32 triggerId;
    uint8 unk1, unk2;
    recvData >> triggerId;
    unk1 = recvData.ReadBit();
    unk2 = recvData.ReadBit();

    //TC_LOG_DEBUG("network", "CMSG_AREA_TRIGGER. Trigger ID: %u", triggerId);

    Player* player = GetPlayer();
    if (player->IsInFlight())
    {
        //TC_LOG_DEBUG("network", "HandleAreaTriggerOpcode: Player '%s' (GUID: %u) in flight, ignore Area Trigger ID:%u", player->GetName().c_str(), player->GetGUIDLow(), triggerId);
        return;
    }

    AreaTriggerEntry const* atEntry = sAreaTriggerStore.LookupEntry(triggerId);
    if (!atEntry)
    {
        //TC_LOG_DEBUG("network", "HandleAreaTriggerOpcode: Player '%s' (GUID: %u) send unknown (by DBC) Area Trigger ID:%u", player->GetName().c_str(), player->GetGUIDLow(), triggerId);
        return;
    }

    if (!player->IsInAreaTriggerRadius(atEntry))
    {
        //TC_LOG_DEBUG("network", "HandleAreaTriggerOpcode: Player '%s' (GUID: %u) too far, ignore Area Trigger ID: %u", player->GetName().c_str(), player->GetGUIDLow(), triggerId);
        return;
    }

    if (player->isDebugAreaTriggers)
        ChatHandler(player->GetSession()).PSendSysMessage(LANG_DEBUG_AREATRIGGER_REACHED, triggerId);

    if (sScriptMgr->OnAreaTrigger(player, atEntry))
        return;

    if (player->IsAlive())
        if (uint32 questId = sObjectMgr->GetQuestForAreaTrigger(triggerId))
            if (player->GetQuestStatus(questId) == QUEST_STATUS_INCOMPLETE)
                player->AreaExploredOrEventHappens(questId);

    if (sObjectMgr->IsTavernAreaTrigger(triggerId))
    {
        // Set resting flag we are in the inn
        player->SetFlag(PLAYER_FIELD_PLAYER_FLAGS, PLAYER_FLAGS_RESTING);
        player->InnEnter(time(nullptr), atEntry->id);
        player->SetRestType(REST_TYPE_IN_TAVERN);

        if (sWorld->IsFFAPvPRealm())
            player->RemoveByteFlag(UNIT_FIELD_SHAPESHIFT_FORM, 1, UNIT_BYTE2_FLAG_FFA_PVP);

        return;
    }

    if (Battleground* bg = player->GetBattleground())
        if (bg->GetStatus() == STATUS_IN_PROGRESS)
            bg->HandleAreaTrigger(player, triggerId);

    if (OutdoorPvP* pvp = player->GetOutdoorPvP())
        if (pvp->HandleAreaTrigger(_player, triggerId))
            return;

    AreaTriggerStruct const* at = sObjectMgr->GetAreaTrigger(triggerId);
    if (!at)
        return;

    bool teleported = false;
    if (player->GetMapId() != at->target_mapId)
    {
        if (!sMapMgr->CanPlayerEnter(at->target_mapId, player, false))
            return;

        if (Group* group = player->GetGroup())
            if (group->isLFGGroup() && player->GetMap()->IsDungeon())
                teleported = player->TeleportToBGEntryPoint();
    }

    if (!teleported)
        player->TeleportTo(at->target_mapId, at->target_X, at->target_Y, at->target_Z, at->target_Orientation, TELE_TO_NOT_LEAVE_TRANSPORT);
}

void WorldSession::HandleUpdateAccountData(WorldPacket& recvData)
{
    //TC_LOG_DEBUG("network", "WORLD: Received CMSG_UPDATE_ACCOUNT_DATA");

    uint32 timestamp = 0, decompressedSize = 0, compCount = 0;
    uint8 type = 0;
    recvData >> decompressedSize >> timestamp >> compCount;

    if (decompressedSize == 0)                               // Erase
    {
        SetAccountData(AccountDataType(type), 0, "");

        WorldPacket data(SMSG_UPDATE_ACCOUNT_DATA_COMPLETE, 4 + 4);
        type = recvData.ReadBits(3);
        data << uint32(type);
        data << uint32(0);
        SendPacket(&data);

        return;
    }

    if (decompressedSize > 0xFFFF)
    {
        recvData.rfinish();                   // Unneeded warning spam in this case
        //TC_LOG_DEBUG("network", "UAD: Account data packet too big, size %u", decompressedSize);
        return;
    }

    ByteBuffer dest;
    dest.resize(decompressedSize);

    uLongf realSize = decompressedSize;
    if (uncompress(const_cast<uint8*>(dest.contents()), &realSize, const_cast<uint8*>(recvData.contents() + recvData.rpos()), compCount) != Z_OK)
    {
        recvData.rfinish();                   // Unneeded warning spam in this case
        //TC_LOG_DEBUG("network", "UAD: Failed to decompress account data");
        return;
    }

    recvData.rpos(recvData.rpos() + compCount);

    type = recvData.ReadBits(3);

    if (type > NUM_ACCOUNT_DATA_TYPES)
        return;

    std::string adata;
    dest >> adata;

    //sLog->outError("general", "data  %u %s", type, adata.c_str());
    SetAccountData(AccountDataType(type), timestamp, adata);

    WorldPacket data(SMSG_UPDATE_ACCOUNT_DATA_COMPLETE, 4 + 4);
    data << uint32(type);
    data << uint32(0);
    SendPacket(&data);
}

void WorldSession::HandleRequestAccountData(WorldPacket& recvData)
{
    //TC_LOG_DEBUG("network", "WORLD: Received CMSG_REQUEST_ACCOUNT_DATA");

    uint32 type = recvData.ReadBits(3);

    //TC_LOG_DEBUG("network", "RAD: type %u", type);

    if (type > NUM_ACCOUNT_DATA_TYPES)
        return;

    AccountData* adata = GetAccountData(AccountDataType(type));

    uint32 size = adata->Data.size();

    uLongf destSize = compressBound(size);

    ByteBuffer dest;
    dest.resize(destSize);

    if (size && compress(dest.contents(), &destSize, (uint8 const*)adata->Data.c_str(), size) != Z_OK)
    {
        //TC_LOG_DEBUG("network", "RAD: Failed to compress account data");
        return;
    }

    dest.resize(destSize);

    WorldPacket data(SMSG_UPDATE_ACCOUNT_DATA, 8 + 4 + 4 + 4 + destSize);

    ObjectGuid guid;

    data.WriteBits(type, 3);       // Type (0-7)
    data.WriteBit(guid[5]);
    data.WriteBit(guid[1]);
    data.WriteBit(guid[3]);
    data.WriteBit(guid[7]);
    data.WriteBit(guid[0]);
    data.WriteBit(guid[4]);
    data.WriteBit(guid[2]);
    data.WriteBit(guid[6]);

    data.WriteByteSeq(guid[3]);
    data.WriteByteSeq(guid[1]);
    data.WriteByteSeq(guid[5]);
    data << uint32(size);        // Decompressed length
    data << uint32(destSize);
    data.append(dest);
    data.WriteByteSeq(guid[7]);
    data.WriteByteSeq(guid[4]);
    data.WriteByteSeq(guid[0]);
    data.WriteByteSeq(guid[6]);
    data.WriteByteSeq(guid[2]);
    data << uint32(adata->Time); // Unix time
    
    SendPacket(&data);
}

int32 WorldSession::HandleEnableNagleAlgorithm()
{
    // Instructs the server we wish to receive few amounts of large packets (SMSG_MULTIPLE_PACKETS?)
    // Instead of large amount of small packets
    return 0;
}

void WorldSession::HandleSetActionButtonOpcode(WorldPacket& recvData)
{
    TC_LOG_DEBUG("network", "WORLD: Received CMSG_SET_ACTION_BUTTON");
    uint8 button;

    recvData >> button;

    ObjectGuid packetData;
    
    recvData.ReadGuidMask(packetData, 7, 0, 5, 2, 1, 6, 3, 4);

    recvData.ReadGuidBytes(packetData, 6, 7, 3, 5, 2, 1, 4, 0);

    ActionButtonPacket* packet = reinterpret_cast<ActionButtonPacket*>(&packetData);
    packet->type >>= 24;

    TC_LOG_INFO("network", "BUTTON: %u ACTION: %u TYPE: %u", button, packet->id, packet->type);
    if (!packet->id)
    {
        TC_LOG_INFO("network", "MISC: Remove action from button %u", button);
        GetPlayer()->removeActionButton(button);
    }
    else
    {
        switch (packet->type)
        {
            case ACTION_BUTTON_MACRO:
            case ACTION_BUTTON_CMACRO:
                TC_LOG_INFO("network", "MISC: Added Macro %u into button %u", packet->id, button);
                break;
            case ACTION_BUTTON_EQSET:
                TC_LOG_INFO("network", "MISC: Added EquipmentSet %u into button %u", packet->id, button);
                break;
            case ACTION_BUTTON_SPELL:
                TC_LOG_INFO("network", "MISC: Added Spell %u into button %u", packet->id, button);
                break;
            case ACTION_BUTTON_SUB_BUTTON:
                TC_LOG_INFO("network", "MISC: Added sub buttons %u into button %u", packet->id, button);
                break;
            case ACTION_BUTTON_ITEM:
                TC_LOG_INFO("network", "MISC: Added Item %u into button %u", packet->id, button);
                break;
            default:
                TC_LOG_INFO("network", "MISC: Unknown action button type %u for action %u into button %u for player %s (GUID: %u)", packet->type, packet->id, button, _player->GetName().c_str(), _player->GetGUIDLow());
                return;
        }
        GetPlayer()->addActionButton(button, packet->id, packet->type);
    }
}

void WorldSession::HandleCompleteCinematic(WorldPacket& /*recvData*/)
{
    //TC_LOG_DEBUG("network", "WORLD: Received CMSG_COMPLETE_CINEMATIC");
}

void WorldSession::HandleNextCinematicCamera(WorldPacket& /*recvData*/)
{
    //TC_LOG_DEBUG("network", "WORLD: Received CMSG_NEXT_CINEMATIC_CAMERA");
}

void WorldSession::HandleMoveTimeSkippedOpcode(WorldPacket& recvData)
{
    //TC_LOG_DEBUG("network", "WORLD: Received CMSG_MOVE_TIME_SKIPPED");

    ObjectGuid guid;
    uint32 time;
    recvData >> time;

    recvData.ReadGuidMask(guid, 5, 1, 3, 0, 2, 6, 7, 4);

    recvData.ReadGuidBytes(guid, 3, 1, 2, 7, 6, 0, 5, 4);
    recvData.ReadByteSeq(guid[5]);

    // TODO!
    //GetPlayer()->ModifyLastMoveTime(-int32(time_skipped));
}

void WorldSession::HandleFeatherFallAck(WorldPacket& recvData)
{
    //TC_LOG_DEBUG("network", "WORLD: CMSG_MOVE_FEATHER_FALL_ACK");

    // Not used
    recvData.rfinish();                       // Prevent warnings spam
}

void WorldSession::HandleMoveUnRootAck(WorldPacket& recvData)
{
    // Not used
    recvData.rfinish();                       // Prevent warnings spam

    /*uint64 guid;
    recvData >> guid;

    // now can skip not our packet
    if (_player->GetGUID() != guid)
    {
        recvData.rfinish();                   // Prevent warnings spam
        return;
    }

    TC_LOG_DEBUG("network", "WORLD: CMSG_FORCE_MOVE_UNROOT_ACK");

    recvData.read_skip<uint32>();                          // Unk

    MovementInfo movementInfo;
    movementInfo.guid = guid;
    ReadMovementInfo(recvData, &movementInfo);
    recvData.read_skip<float>();                           // Unk2*/
}

void WorldSession::HandleMoveRootAck(WorldPacket& recvData)
{
    // Not used
    recvData.rfinish();                       // Prevent warnings spam

    /*uint64 guid;
    recvData >> guid;

    // now can skip not our packet
    if (_player->GetGUID() != guid)
    {
        recvData.rfinish();                   // Prevent warnings spam
        return;
    }

    TC_LOG_DEBUG("network", "WORLD: CMSG_FORCE_MOVE_ROOT_ACK");

    recvData.read_skip<uint32>();             // Unk

    MovementInfo movementInfo;
    ReadMovementInfo(recvData, &movementInfo);*/
}

void WorldSession::HandleSetActionBarToggles(WorldPacket& recvData)
{
    uint8 actionBar;

    recvData >> actionBar;

    if (!GetPlayer())                                        // Ignore until not logged (check needed because STATUS_AUTHED)
    {
        if (actionBar != 0)
            TC_LOG_DEBUG("network", "WorldSession::HandleSetActionBarToggles in not logged state with value: %u, ignored", uint32(actionBar));
        return;
    }

    GetPlayer()->SetByteValue(PLAYER_FIELD_LIFETIME_MAX_RANK, 2, actionBar);
}

void WorldSession::HandlePlayedTime(WorldPacket& recvData)
{
    uint8 unk1;
    recvData >> unk1;                                       // 0 or 1 expected

    WorldPacket data(SMSG_PLAYED_TIME, 4 + 4 + 1);
    data << uint32(_player->GetTotalPlayedTime());
    data << uint32(_player->GetLevelPlayedTime());
    data << uint8(unk1);                                    // 0 - Will not show in chat frame
    SendPacket(&data);
}

void WorldSession::HandlePandarenFactionChoiceOpcode(WorldPacket& recvData)
{
    if (_player->getRace() != RACE_PANDAREN_NEUTRAL)
    {
        recvData.rfinish();
        return;
    }

    uint32 race;
    recvData >> race;

    uint32 const* languageSpells;
    WorldLocation location;
    uint32 homebindLocation;

    if (race)
    {
        race = RACE_PANDAREN_ALLIANCE;
        languageSpells = pandarenLanguageSpellsAlliance;
        location = WorldLocation(870, 1300.529785f, -2492.593018f, 143.600220f, 2.755214f);
        homebindLocation = 9;
    }
    else
    {
        race = RACE_PANDAREN_HORDE;
        languageSpells = pandarenLanguageSpellsHorde;
        location = WorldLocation(870, 1300.529785f, -2492.593018f, 143.600220f, 2.755214f);
        homebindLocation = 363;
    }

    _player->SetRace(race);
    _player->setFactionForRace(race);
    _player->TeleportTo(location);
    _player->SetHomebind(location, homebindLocation);

    for (uint8 i = 0; i < PANDAREN_FACTION_LANGUAGE_COUNT; i++)
        _player->learnSpell(languageSpells[i], false);

    WorldPacket data(SMSG_PANDAREN_FACTION_CHOSEN, 4 + 1);
    data << (uint32)race;
    data.WriteBit(1); // Unk bool
    data.FlushBits();
    SendPacket(&data);

    _player->GetReputationMgr().UpdateReputationFlags();

    _player->GetReputationMgr().SendInitialReputations();
    _player->SendFeatureSystemStatus();
    _player->GetReputationMgr().SendForceReactions();
}

void WorldSession::HandleInspectOpcode(WorldPacket& recvData)
{
    ObjectGuid guid;

    recvData.ReadGuidMask(guid, 0, 3, 7, 2, 5, 1, 4, 6);
    recvData.ReadGuidBytes(guid, 3, 5, 2, 4, 1, 6, 0, 7);

    TC_LOG_DEBUG("network", "WORLD: Received CMSG_INSPECT");

    _player->SetSelection(guid);

    Player* player = ObjectAccessor::FindPlayer(guid);
    if (!player)
    {
        TC_LOG_DEBUG("network", "CMSG_INSPECT: No player found from GUID: " UI64FMTD, (uint64)guid);
        return;
    }

    _player->SendInspectResult(player);
}

void WorldSession::HandleInspectHonorStatsOpcode(WorldPacket& recvData)
{
    ObjectGuid guid;

    guid[4] = recvData.ReadBit();
    guid[3] = recvData.ReadBit();
    guid[6] = recvData.ReadBit();
    guid[1] = recvData.ReadBit();
    guid[0] = recvData.ReadBit();
    guid[2] = recvData.ReadBit();
    guid[5] = recvData.ReadBit();
    guid[7] = recvData.ReadBit();

    recvData.ReadByteSeq(guid[0]);
    recvData.ReadByteSeq(guid[5]);
    recvData.ReadByteSeq(guid[1]);
    recvData.ReadByteSeq(guid[4]);
    recvData.ReadByteSeq(guid[2]);
    recvData.ReadByteSeq(guid[6]);
    recvData.ReadByteSeq(guid[7]);
    recvData.ReadByteSeq(guid[3]);

    Player* player = ObjectAccessor::FindPlayer(guid);

    if (!player)
    {
        TC_LOG_DEBUG("network", "CMSG_INSPECT_HONOR_STATS: No player found from GUID: " UI64FMTD, (uint64)guid);
        return;
    }

    ObjectGuid playerGuid = player->GetGUID();
    WorldPacket data(SMSG_INSPECT_HONOR_STATS, 8 + 1 + 4 + 4);

    data << uint32(player->GetUInt32Value(PLAYER_FIELD_LIFETIME_HONORABLE_KILLS));
    data << uint16(player->GetUInt16Value(PLAYER_FIELD_YESTERDAY_HONORABLE_KILLS, 1));  // Yesterday's kills
    data << uint16(player->GetUInt16Value(PLAYER_FIELD_YESTERDAY_HONORABLE_KILLS, 0));  // Today's kills
    data << uint8(0);                                                                   // PLAYER_FIELD_LIFETIME_MAX_RANK, offset 0 - Lifetime PVP Rank

    data.WriteBit(playerGuid[2]);
    data.WriteBit(playerGuid[1]);
    data.WriteBit(playerGuid[6]);
    data.WriteBit(playerGuid[4]);
    data.WriteBit(playerGuid[5]);
    data.WriteBit(playerGuid[3]);
    data.WriteBit(playerGuid[7]);
    data.WriteBit(playerGuid[0]);

    data.WriteByteSeq(playerGuid[1]);
    data.WriteByteSeq(playerGuid[3]);
    data.WriteByteSeq(playerGuid[6]);
    data.WriteByteSeq(playerGuid[7]);
    data.WriteByteSeq(playerGuid[2]);
    data.WriteByteSeq(playerGuid[4]);
    data.WriteByteSeq(playerGuid[5]);
    data.WriteByteSeq(playerGuid[0]);

    SendPacket(&data);
}

void WorldSession::HandleWorldTeleportOpcode(WorldPacket& recvData)
{
    uint32 time;
    uint32 mapid;
    float PositionX;
    float PositionY;
    float PositionZ;
    float Orientation;

    recvData >> time;                                      // time in m.sec.
    recvData >> mapid;
    recvData >> PositionX;
    recvData >> PositionY;
    recvData >> PositionZ;
    recvData >> Orientation;                               // o (3.141593 = 180 degrees)

    //TC_LOG_DEBUG("network", "WORLD: Received CMSG_WORLD_TELEPORT");

    if (GetPlayer()->IsInFlight())
    {
        //TC_LOG_DEBUG("network", "Player '%s' (GUID: %u) in flight, ignore worldport command.", GetPlayer()->GetName().c_str(), GetPlayer()->GetGUIDLow());
        return;
    }

    //TC_LOG_DEBUG("network", "CMSG_WORLD_TELEPORT: Player = %s, Time = %u, map = %u, x = %f, y = %f, z = %f, o = %f", GetPlayer()->GetName().c_str(), time, mapid, PositionX, PositionY, PositionZ, Orientation);

    if (HasPermission(rbac::RBAC_PERM_OPCODE_WORLD_TELEPORT))
        GetPlayer()->TeleportTo(mapid, PositionX, PositionY, PositionZ, Orientation);
    else
        SendNotification(LANG_YOU_NOT_HAVE_PERMISSION);
}

void WorldSession::HandleWhoisOpcode(WorldPacket& recvData)
{
    //TC_LOG_DEBUG("network", "Received opcode CMSG_WHOIS");

    std::string charname;
    recvData >> charname;

    if (!HasPermission(rbac::RBAC_PERM_OPCODE_WHOIS))
    {
        SendNotification(LANG_YOU_NOT_HAVE_PERMISSION);
        return;
    }

    if (charname.empty() || !normalizePlayerName (charname))
    {
        SendNotification(LANG_NEED_CHARACTER_NAME);
        return;
    }

    Player* player = sObjectAccessor->FindConnectedPlayerByName(charname);

    if (!player)
    {
        SendNotification(LANG_PLAYER_NOT_EXIST_OR_OFFLINE, charname.c_str());
        return;
    }

    uint32 accid = player->GetSession()->GetAccountId();

    PreparedStatement* stmt = LoginDatabase.GetPreparedStatement(LOGIN_SEL_ACCOUNT_WHOIS);

    stmt->setUInt32(0, accid);

    PreparedQueryResult result = LoginDatabase.Query(stmt);

    if (!result)
    {
        SendNotification(LANG_ACCOUNT_FOR_PLAYER_NOT_FOUND, charname.c_str());
        return;
    }

    Field* fields = result->Fetch();
    std::string acc = fields[0].GetString();
    if (acc.empty())
        acc = "Unknown";
    std::string email = fields[1].GetString();
    if (email.empty())
        email = "Unknown";
    std::string lastip = fields[2].GetString();
    if (lastip.empty())
        lastip = "Unknown";

    std::string msg = charname + "'s " + "account is " + acc + ", e-mail: " + email + ", last ip: " + lastip;

    WorldPacket data(SMSG_WHOIS, msg.size()+1);
    data << msg;
    SendPacket(&data);

    //TC_LOG_DEBUG("network", "Received whois command from player %s for character %s", GetPlayer()->GetName().c_str(), charname.c_str());
}

void WorldSession::HandleComplainOpcode(WorldPacket& recvData)
{
    //TC_LOG_DEBUG("network", "WORLD: CMSG_COMPLAIN");

    uint8 spam_type;                                        // 0 - Mail, 1 - Chat
    uint64 spammer_guid;
    uint32 unk1 = 0;
    uint32 unk2 = 0;
    uint32 unk3 = 0;
    uint32 unk4 = 0;
    std::string description = "";
    recvData >> spam_type;                                 // Unk 0x01 const, may be spam type (mail/chat)
    recvData >> spammer_guid;                              // Player guid
    switch (spam_type)
    {
        case 0:
            recvData >> unk1;                              // Const 0
            recvData >> unk2;                              // Probably mail id
            recvData >> unk3;                              // Const 0
            break;
        case 1:
            recvData >> unk1;                              // Probably language
            recvData >> unk2;                              // Message type?
            recvData >> unk3;                              // Probably channel id
            recvData >> unk4;                              // Time
            recvData >> description;                       // Spam description string (messagetype, channel name, player name, message)
            break;
    }

    // NOTE: All chat messages from this spammer automatically ignored by spam reporter until logout in case chat spam.
    // If it's mail spam - ALL mails from this spammer automatically removed by client

    // Complaint Received message
    WorldPacket data(SMSG_COMPLAIN_RESULT, 2);
    data << uint8(0); // Value 1 resets CGChat::m_complaintsSystemStatus in client. (unused?)
    data << uint8(0); // Value 0xC generates a "CalendarError" in client.
    SendPacket(&data);

    //TC_LOG_DEBUG("network", "REPORT SPAM: type %u, guid %u, unk1 %u, unk2 %u, unk3 %u, unk4 %u, message %s", spam_type, GUID_LOPART(spammer_guid), unk1, unk2, unk3, unk4, description.c_str());
}

void WorldSession::HandleRealmSplitOpcode(WorldPacket& recvData)
{
    //TC_LOG_DEBUG("network", "CMSG_REALM_SPLIT");

    uint32 unk;
    std::string split_date = "01/01/01";
    recvData >> unk;

    WorldPacket data(SMSG_REALM_SPLIT, 4+4+split_date.size()+1);
    data << unk;
    data << uint32(0x00000000);                             // Realm split state

    // Split states:
    // 0x0 realm normal
    // 0x1 realm split
    // 0x2 realm split pending
    data.WriteBits(split_date.size(), 7);
    data.WriteString(split_date);
    SendPacket(&data);

    //TC_LOG_DEBUG("response sent %u", unk);
}

void WorldSession::HandleFarSightOpcode(WorldPacket& recvData)
{
    //TC_LOG_DEBUG("network", "WORLD: CMSG_FAR_SIGHT");

    bool apply;
    recvData >> apply;

    if (apply)
    {
        //TC_LOG_DEBUG("network", "Added FarSight " UI64FMTD " to player %u", _player->GetUInt64Value(PLAYER_FIELD_FARSIGHT_OBJECT), _player->GetGUIDLow());
        if (WorldObject* target = _player->GetViewpoint())
            _player->SetSeer(target);
        else
            TC_LOG_DEBUG("network", "Player %s (GUID: %u) requests non-existing seer " UI64FMTD, _player->GetName().c_str(), GUID_LOPART(_player->GetGUID()), _player->GetUInt64Value(PLAYER_FIELD_FARSIGHT_OBJECT));
    }
    else
    {
        //TC_LOG_DEBUG("network", "Player %u set vision to self", _player->GetGUIDLow());
        _player->SetSeer(_player);
    }

    GetPlayer()->UpdateVisibilityForPlayer();
}

void WorldSession::HandleSetTitleOpcode(WorldPacket& recvData)
{
    TC_LOG_DEBUG("network", "CMSG_SET_TITLE");

    uint32 title;
    recvData >> title;

    // -1 at none
    if (title > 0 && title < MAX_TITLE_INDEX)
    {
        if (!GetPlayer()->HasTitle(title))
            return;
    }
    else
        title = 0;

    GetPlayer()->SetUInt32Value(PLAYER_FIELD_PLAYER_TITLE, title);
}

void WorldSession::HandleTimeSyncResp(WorldPacket& recvData)
{
    //TC_LOG_DEBUG("network", "CMSG_TIME_SYNC_RESP");

    uint32 counter, clientTicks;
    recvData >> counter >> clientTicks;

    if (counter != _player->m_timeSyncQueue.front())
        TC_LOG_DEBUG("network", "Wrong time sync counter from player %s (cheater?)", _player->GetName().c_str());

    //TC_LOG_DEBUG("network", "Time sync received: counter %u, client ticks %u, time since last sync %u", counter, clientTicks, clientTicks - _player->m_timeSyncClient);

    uint32 ourTicks = clientTicks + (getMSTime() - _player->m_timeSyncServer);

    // Diff should be small
    //TC_LOG_DEBUG("network", "Our ticks: %u, diff %u, latency %u", ourTicks, ourTicks - clientTicks, GetLatency());

    _player->m_timeSyncClient = clientTicks;
    _player->m_timeSyncQueue.pop();
}

void WorldSession::HandleResetInstancesOpcode(WorldPacket& /*recvData*/)
{
    //TC_LOG_DEBUG("network", "WORLD: CMSG_RESET_INSTANCES");

    if (Group* group = _player->GetGroup())
    {
        if (group->IsLeader(_player->GetGUID()))
            group->ResetInstances(INSTANCE_RESET_ALL, false, _player);
    }
    else
        _player->ResetInstances(INSTANCE_RESET_ALL, false);
}

void WorldSession::HandleSetDungeonDifficultyOpcode(WorldPacket& recvData)
{
    //TC_LOG_DEBUG("network", "MSG_SET_DUNGEON_DIFFICULTY");

    uint32 mode;
    recvData >> mode;

    if (mode >= MAX_DUNGEON_DIFFICULTY)
    {
        //TC_LOG_DEBUG("network", "WorldSession::HandleSetDungeonDifficultyOpcode: player %d sent an invalid instance mode %d!", _player->GetGUIDLow(), mode);
        return;
    }

    if (Difficulty(mode) == _player->GetDungeonDifficulty())
        return;

    // Cannot reset while in an instance
    Map* map = _player->FindMap();
    if (map && map->IsDungeon())
    {
        //TC_LOG_DEBUG("network", "WorldSession::HandleSetDungeonDifficultyOpcode: player (Name: %s, GUID: %u) tried to reset the instance while player is inside!", _player->GetName().c_str(), _player->GetGUIDLow());
        return;
    }

    Group* group = _player->GetGroup();
    if (group)
    {
        if (group->IsLeader(_player->GetGUID()))
        {
            for (GroupReference* itr = group->GetFirstMember(); itr != NULL; itr = itr->next())
            {
                Player* groupGuy = itr->GetSource();
                if (!groupGuy)
                    continue;

                if (!groupGuy->IsInMap(groupGuy))
                    return;

                if (groupGuy->GetMap()->IsNonRaidDungeon())
                {
                    //TC_LOG_DEBUG("network", "WorldSession::HandleSetDungeonDifficultyOpcode: player %d tried to reset the instance while group member (Name: %s, GUID: %u) is inside!", _player->GetGUIDLow(), groupGuy->GetName().c_str(), groupGuy->GetGUIDLow());
                    return;
                }
            }
            // The difficulty is set even if the instances can't be reset
            //_player->SendDungeonDifficulty();
            group->ResetInstances(INSTANCE_RESET_CHANGE_DIFFICULTY, false, _player);
            group->SetDungeonDifficulty(Difficulty(mode));
            group->SendUpdate();
        }
    }
    else
    {
        _player->ResetInstances(INSTANCE_RESET_CHANGE_DIFFICULTY, false);
        _player->SetDungeonDifficulty(Difficulty(mode));
        _player->SendDungeonDifficulty();
    }
}

void WorldSession::HandleSetRaidDifficultyOpcode(WorldPacket& recvData)
{
    //TC_LOG_DEBUG("network", "MSG_SET_RAID_DIFFICULTY");

    uint32 mode;
    recvData >> mode;

    if (mode >= MAX_RAID_DIFFICULTY)
    {
        //TC_LOG_DEBUG("network", "WorldSession::HandleSetRaidDifficultyOpcode: player %d sent an invalid instance mode %d!", _player->GetGUIDLow(), mode);
        return;
    }

    // Cannot reset while in an instance
    Map* map = _player->FindMap();
    if (map && map->IsDungeon())
    {
        //TC_LOG_DEBUG("network", "WorldSession::HandleSetRaidDifficultyOpcode: player %d tried to reset the instance while inside!", _player->GetGUIDLow());
        return;
    }

    if (Difficulty(mode) == _player->GetRaidDifficulty())
        return;

    Group* group = _player->GetGroup();
    if (group)
    {
        if (group->IsLeader(_player->GetGUID()))
        {
            for (GroupReference* itr = group->GetFirstMember(); itr != NULL; itr = itr->next())
            {
                Player* groupGuy = itr->GetSource();
                if (!groupGuy)
                    continue;

                if (!groupGuy->IsInMap(groupGuy))
                    return;

                if (groupGuy->GetMap()->IsRaid())
                {
                    //TC_LOG_DEBUG("network", "WorldSession::HandleSetRaidDifficultyOpcode: player %d tried to reset the instance while inside!", _player->GetGUIDLow());
                    return;
                }
            }
            // The difficulty is set even if the instances can't be reset
            //_player->SendDungeonDifficulty(true);
            group->ResetInstances(INSTANCE_RESET_CHANGE_DIFFICULTY, true, _player);
            group->SetRaidDifficulty(Difficulty(mode));
            group->SendUpdate();
        }
    }
    else
    {
        _player->ResetInstances(INSTANCE_RESET_CHANGE_DIFFICULTY, true);
        _player->SetRaidDifficulty(Difficulty(mode));
        _player->SendRaidDifficulty();
    }
}

void WorldSession::HandleCancelMountAuraOpcode(WorldPacket& /*recvData*/)
{
    //TC_LOG_DEBUG("network", "WORLD: CMSG_CANCEL_MOUNT_AURA");

    // If player is not mounted, so go out :)
    if (!_player->IsMounted())                              // Not blizz like; no messages on blizz
    {
        ChatHandler(this).SendSysMessage(LANG_CHAR_NON_MOUNTED);
        return;
    }

    if (_player->IsInFlight())                               // Not blizz like; no messages on blizz
    {
        ChatHandler(this).SendSysMessage(LANG_YOU_IN_FLIGHT);
        return;
    }

    _player->RemoveAurasByType(SPELL_AURA_MOUNTED); // Calls Dismount()
}

void WorldSession::HandleMoveSetCanFlyAckOpcode(WorldPacket& recvData)
{
    // Fly mode on/off
    //TC_LOG_DEBUG("network", "WORLD: CMSG_MOVE_SET_CAN_FLY_ACK");

    MovementInfo movementInfo;
    _player->ReadMovementInfo(recvData, &movementInfo);
    _player->m_mover->m_movementInfo.flags = movementInfo.GetMovementFlags();
}

void WorldSession::HandleRequestPetInfoOpcode(WorldPacket& /*recvData */)
{
    /*
        TC_LOG_DEBUG("network", "WORLD: CMSG_REQUEST_PET_INFO");
        recvData.hexlike();
    */
}

void WorldSession::HandleSetTaxiBenchmarkOpcode(WorldPacket& recvData)
{
    //TC_LOG_DEBUG("network", "WORLD: CMSG_SET_TAXI_BENCHMARK_MODE");

    uint8 mode;
    recvData >> mode;

    mode ? _player->SetFlag(PLAYER_FIELD_PLAYER_FLAGS, PLAYER_FLAGS_TAXI_BENCHMARK) : _player->RemoveFlag(PLAYER_FIELD_PLAYER_FLAGS, PLAYER_FLAGS_TAXI_BENCHMARK);

    //TC_LOG_DEBUG("network", "Client used \"/timetest %d\" command", mode);
}

void WorldSession::HandleQueryInspectAchievements(WorldPacket& recvData)
{
    ObjectGuid guid;
    recvData.ReadGuidMask(guid, 2, 7, 1, 5, 4, 0, 3, 6);

    recvData.ReadGuidBytes(guid, 7, 2, 0, 4, 1, 5, 6, 3);

    Player* player = ObjectAccessor::FindPlayer(guid);
    if (!player)
    {
        //TC_LOG_DEBUG("network", "CMSG_QUERY_INSPECT_ACHIEVEMENTS: Inspected Player " UI64FMTD, (uint64)guid);
        return;
    }

    if (!GetPlayer()->IsWithinDistInMap(player, INSPECT_DISTANCE, false))
        return;

    if (GetPlayer()->IsValidAttackTarget(player))
        return;

    player->SendRespondInspectAchievements(_player);
}

void WorldSession::HandleGuildAchievementProgressQuery(WorldPacket& recvData)
{
    uint32 achievementId;
    recvData >> achievementId;

    if (Guild* guild = _player->GetGuild())
        guild->GetAchievementMgr().SendAchievementInfo(_player, achievementId);
}

void WorldSession::HandleWorldStateUITimerUpdate(WorldPacket& /*recvData*/)
{
    // Empty opcode
    //TC_LOG_DEBUG("network", "WORLD: CMSG_WORLD_STATE_UI_TIMER_UPDATE");

    WorldPacket data(SMSG_WORLD_STATE_UI_TIMER_UPDATE, 4);
    data << uint32(time(NULL));
    SendPacket(&data);
}

void WorldSession::HandleReadyForAccountDataTimes(WorldPacket& /*recvData*/)
{
    // Empty opcode
    //TC_LOG_DEBUG("network", "WORLD: CMSG_READY_FOR_ACCOUNT_DATA_TIMES");

    SendAccountDataTimes(GLOBAL_CACHE_MASK);
}

void WorldSession::SendSetPhaseShift(std::set<uint32> const& phaseIds, std::set<uint32> const& terrainswaps, std::set<uint32> const& worldMapAreas)
{
    ObjectGuid guid = _player->GetGUID();

    WorldPacket data(SMSG_SET_PHASE_SHIFT, 1 + 8 + 2 * phaseIds.size() + 4 + 2 * worldMapAreas.size() + 2 * terrainswaps.size() + 4);
    data.WriteGuidMask(guid, 0, 3, 1, 4, 6, 2, 7, 5);

    data.WriteGuidBytes(guid, 4, 3, 2);

    data << uint32(phaseIds.size()) * 2;          // Phase.dbc ids
    for (std::set<uint32>::const_iterator itr = phaseIds.begin(); itr != phaseIds.end(); ++itr)
        data << uint16(*itr);

    data.WriteGuidBytes(guid, 0, 6);

    data << uint32(0);                            // Inactive terrain swaps
    //for (uint8 i = 0; i < inactiveSwapsCount; ++i)
    //    data << uint16(0);

    data.WriteGuidBytes(guid, 1, 7);

    data << uint32(worldMapAreas.size()) * 2;     // WorldMapArea.dbc id (controls map display)
    for (std::set<uint32>::const_iterator itr = worldMapAreas.begin(); itr != worldMapAreas.end(); ++itr)
        data << uint16(*itr);

    data << uint32(terrainswaps.size()) * 2;      // Active terrain swaps
    for (std::set<uint32>::const_iterator itr = terrainswaps.begin(); itr != terrainswaps.end(); ++itr)
        data << uint16(*itr);

    data.WriteByteSeq(guid[5]);

    data << uint32(phaseIds.size() ? 0 : 8);      // Flags (not phasemask)

    SendPacket(&data);
}

// Battlefield and Battleground
void WorldSession::HandleAreaSpiritHealerQueryOpcode(WorldPacket& recvData)
{
    //TC_LOG_DEBUG("network", "WORLD: CMSG_AREA_SPIRIT_HEALER_QUERY");

    Battleground* bg = _player->GetBattleground();

    ObjectGuid guid;

    uint8 bitOrder[8] = { 5, 6, 0, 4, 1, 2, 7, 3};
    recvData.ReadBitInOrder(guid, bitOrder);

    recvData.FlushBits();

    recvData.ReadGuidBytes(guid, 0, 2, 6, 7, 1, 5, 3, 4);

    Creature* unit = GetPlayer()->GetMap()->GetCreature(guid);
    if (!unit)
        return;

    if (!unit->IsSpiritService())                            // it's not spirit service
        return;

    if (bg)
        sBattlegroundMgr->SendAreaSpiritHealerQueryOpcode(_player, bg, guid);

    if (Battlefield* bf = sBattlefieldMgr->GetBattlefieldToZoneId(_player->GetZoneId()))
        bf->SendAreaSpiritHealerQueryOpcode(_player, guid);
}

void WorldSession::HandleAreaSpiritHealerQueueOpcode(WorldPacket& recvData)
{
    //TC_LOG_DEBUG("network", "WORLD: CMSG_AREA_SPIRIT_HEALER_QUEUE");

    ObjectGuid guid;
    Battleground* bg = _player->GetBattleground();

    guid[5] = recvData.ReadBit();
    guid[4] = recvData.ReadBit();
    guid[0] = recvData.ReadBit();
    guid[2] = recvData.ReadBit();
    guid[7] = recvData.ReadBit();
    guid[1] = recvData.ReadBit();
    guid[6] = recvData.ReadBit();
    guid[3] = recvData.ReadBit();

    recvData.ReadByteSeq(guid[1]);
    recvData.ReadByteSeq(guid[7]);
    recvData.ReadByteSeq(guid[6]);
    recvData.ReadByteSeq(guid[2]);
    recvData.ReadByteSeq(guid[4]);
    recvData.ReadByteSeq(guid[3]);
    recvData.ReadByteSeq(guid[0]);
    recvData.ReadByteSeq(guid[5]);

    Creature* unit = GetPlayer()->GetMap()->GetCreature(guid);
    if (!unit)
        return;

    if (!unit->IsSpiritService())                            // it's not spirit service
        return;

    if (bg)
        bg->AddPlayerToResurrectQueue(guid, _player->GetGUID());

    if (Battlefield* bf = sBattlefieldMgr->GetBattlefieldToZoneId(_player->GetZoneId()))
        bf->AddPlayerToResurrectQueue(guid, _player->GetGUID());
}

void WorldSession::HandleHearthAndResurrect(WorldPacket& /*recvData*/)
{
    if (_player->IsInFlight())
        return;

    if (/*Battlefield* bf = */sBattlefieldMgr->GetBattlefieldToZoneId(_player->GetZoneId()))
    {
        // bf->PlayerAskToLeave(_player); FIXME
        return;
    }

    AreaTableEntry const* atEntry = GetAreaEntryByAreaID(_player->GetAreaId());
    if (!atEntry || !(atEntry->flags & AREA_FLAG_CAN_HEARTH_AND_RESURRECT))
        return;

    _player->BuildPlayerRepop();
    _player->ResurrectPlayer(100);
    _player->TeleportTo(_player->m_homebindMapId, _player->m_homebindX, _player->m_homebindY, _player->m_homebindZ, _player->GetOrientation());
}

void WorldSession::HandleInstanceLockResponse(WorldPacket& recvPacket)
{
    uint8 accept;
    recvPacket >> accept;

    if (!_player->HasPendingBind())
    {
        TC_LOG_INFO("network", "InstanceLockResponse: Player %s (guid %u) tried to bind himself/teleport to graveyard without a pending bind!",
            _player->GetName().c_str(), _player->GetGUIDLow());
        return;
    }

    if (accept)
        _player->BindToInstance();
    else
        _player->RepopAtGraveyard();

    _player->SetPendingBind(0, 0);
}

void WorldSession::HandleRequestHotfix(WorldPacket& recvPacket)
{
    uint32 type, count;
    recvPacket >> type;

    DB2StorageBase const* store = GetDB2Storage(type);
    if (!store)
    {
        //TC_LOG_DEBUG("network", "CMSG_REQUEST_HOTFIX: Received unknown hotfix type: %u", type);
        recvPacket.rfinish();
        return;
    }

    count = recvPacket.ReadBits(21);

    ObjectGuid* guids = new ObjectGuid[count];
    for (uint32 i = 0; i < count; ++i)
    {
        recvPacket.ReadGuidMask(guids[i], 6, 3, 0, 1, 4, 5, 7, 2);
    }

    uint32 entry;
    for (uint32 i = 0; i < count; ++i)
    {
        recvPacket.ReadByteSeq(guids[i][1]);
        recvPacket >> entry;
        recvPacket.ReadGuidBytes(guids[i], 0, 5, 6, 4, 7, 2, 3);

        // Temporary: This should be moved once broadcast text is properly implemented
        if (type == DB2_REPLY_BroadcastText)
        {
            SendBroadcastText(entry);
            continue;
        }

        if (!store->HasRecord(entry))
            continue;

        ByteBuffer record;
        store->WriteRecord(entry, (uint32)GetSessionDbcLocale(), record);

        WorldPacket data(SMSG_DB_REPLY);
        data << uint32(entry);
        data << uint32(time(NULL));
        data << uint32(type);
        data << uint32(record.size());
        data.append(record);

        SendPacket(&data);

        //TC_LOG_DEBUG("network", "SMSG_DB_REPLY: Sent hotfix entry: %u type: %u", entry, type);
    }

    delete [] guids;
}

void WorldSession::SendBroadcastText(uint32 entry)
{
    /*  This is a hack fix! Still uses Gossip ID's instead of Broadcast Text ID's.
     *  Major database changed required at some point. */

    ByteBuffer buffer;
    std::string defaultText = "Greetings, $n";

    GossipText const* pGossip = sObjectMgr->GetGossipText(entry);

    uint16 nrmTextLength = pGossip ? pGossip->Options[0].Text_0.length() : defaultText.length();
    uint16 altTextLength = pGossip ? pGossip->Options[0].Text_1.length() : defaultText.length();

    buffer << uint32(entry);
    buffer << uint32(pGossip ? pGossip->Options[0].Language : 0);
    buffer << uint16(nrmTextLength);

    if (nrmTextLength)
        buffer << std::string(pGossip ? pGossip->Options[0].Text_0 : defaultText);

    buffer << uint16(altTextLength);

    if (altTextLength)
        buffer << std::string(pGossip ? pGossip->Options[0].Text_1 : defaultText);

    for (int i = 0; i < MAX_GOSSIP_TEXT_OPTIONS; i++)
        buffer << uint32(0);

    buffer << uint32(1);

    WorldPacket data(SMSG_DB_REPLY);
    data << uint32(entry);
    data << uint32(time(NULL));
    data << uint32(DB2_REPLY_BroadcastText);
    data << uint32(buffer.size());
    data.append(buffer);

    SendPacket(&data);
}

void WorldSession::HandleUpdateMissileTrajectory(WorldPacket& recvPacket)
{
    //TC_LOG_DEBUG("network", "WORLD: CMSG_UPDATE_MISSILE_TRAJECTORY");

    uint64 guid;
    uint32 spellId;
    float elevation, speed;
    float curX, curY, curZ;
    float targetX, targetY, targetZ;
    uint8 moveStop;

    recvPacket >> guid >> spellId >> elevation >> speed;
    recvPacket >> curX >> curY >> curZ;
    recvPacket >> targetX >> targetY >> targetZ;
    recvPacket >> moveStop;

    Unit* caster = ObjectAccessor::GetUnit(*_player, guid);
    Spell* spell = caster ? caster->GetCurrentSpell(CURRENT_GENERIC_SPELL) : NULL;
    if (!spell || spell->m_spellInfo->Id != spellId || !spell->m_targets.HasDst() || !spell->m_targets.HasSrc())
    {
        recvPacket.rfinish();
        return;
    }

    Position pos = *spell->m_targets.GetSrcPos();
    pos.Relocate(curX, curY, curZ);
    spell->m_targets.ModSrc(pos);

    pos = *spell->m_targets.GetDstPos();
    pos.Relocate(targetX, targetY, targetZ);
    spell->m_targets.ModDst(pos);

    spell->m_targets.SetElevation(elevation);
    spell->m_targets.SetSpeed(speed);

    if (moveStop)
    {
        uint32 opcode;
        recvPacket >> opcode;
        recvPacket.SetOpcode(MSG_MOVE_STOP); // always set to MSG_MOVE_STOP in client SetOpcode
        HandleMovementOpcodes(recvPacket);
    }
}

void WorldSession::HandleViolenceLevel(WorldPacket& recvPacket)
{
    uint8 violenceLevel;
    recvPacket >> violenceLevel;
}

void WorldSession::HandleObjectUpdateFailedOpcode(WorldPacket& recvPacket)
{
    ObjectGuid guid;

    recvPacket.ReadGuidMask(guid, 3, 5, 6, 0, 1, 2, 7, 4);

    recvPacket.ReadGuidBytes(guid, 0, 6, 5, 7, 2, 1, 3, 4);

    WorldObject* obj = ObjectAccessor::GetWorldObject(*GetPlayer(), guid);

    TC_LOG_ERROR("network", "Object update failed for object " UI64FMTD " (%s) for player %s (%u)", uint64(guid), obj ? obj->GetName().c_str() : "object-not-found", GetPlayerName().c_str(), GetGuidLow());
}

void WorldSession::HandleSaveCUFProfiles(WorldPacket& recvPacket)
{
    //TC_LOG_DEBUG("network", "WORLD: CMSG_SAVE_CUF_PROFILES");

    uint8 count = (uint8)recvPacket.ReadBits(19);

    if (count > MAX_CUF_PROFILES)
    {
        recvPacket.rfinish();
        return;
    }

    CUFProfile* profiles[MAX_CUF_PROFILES];
    uint8 strlens[MAX_CUF_PROFILES];

    for (uint8 i = 0; i < count; ++i)
    {
        profiles[i] = new CUFProfile;
        profiles[i]->BoolOptions.set(CUF_AUTO_ACTIVATE_SPEC_2,              recvPacket.ReadBit());
        profiles[i]->BoolOptions.set(CUF_DISPLAY_MAIN_TANK_AND_ASSIST,      recvPacket.ReadBit());
        profiles[i]->BoolOptions.set(CUF_DISPLAY_POWER_BAR,                 recvPacket.ReadBit());
        profiles[i]->BoolOptions.set(CUF_AUTO_ACTIVATE_10_PLAYERS,          recvPacket.ReadBit());
        profiles[i]->BoolOptions.set(CUF_AUTO_ACTIVATE_3_PLAYERS,           recvPacket.ReadBit());
        profiles[i]->BoolOptions.set(CUF_UNK_156,                           recvPacket.ReadBit());
        profiles[i]->BoolOptions.set(CUF_AUTO_ACTIVATE_40_PLAYERS,          recvPacket.ReadBit());
        profiles[i]->BoolOptions.set(CUF_AUTO_ACTIVATE_2_PLAYERS,           recvPacket.ReadBit());
        profiles[i]->BoolOptions.set(CUF_KEEP_GROUPS_TOGETHER,              recvPacket.ReadBit());
        profiles[i]->BoolOptions.set(CUF_USE_CLASS_COLORS,                  recvPacket.ReadBit());
        profiles[i]->BoolOptions.set(CUF_AUTO_ACTIVATE_25_PLAYERS,          recvPacket.ReadBit());
        profiles[i]->BoolOptions.set(CUF_UNK_145,                           recvPacket.ReadBit());
        strlens[i] = (uint8)recvPacket.ReadBits(7);
        profiles[i]->BoolOptions.set(CUF_DISPLAY_PETS,                      recvPacket.ReadBit());
        profiles[i]->BoolOptions.set(CUF_AUTO_ACTIVATE_PVP,                 recvPacket.ReadBit());
        profiles[i]->BoolOptions.set(CUF_DISPLAY_ONLY_DISPELLABLE_DEBUFFS,  recvPacket.ReadBit());
        profiles[i]->BoolOptions.set(CUF_DISPLAY_NON_BOSS_DEBUFFS,          recvPacket.ReadBit());
        profiles[i]->BoolOptions.set(CUF_AUTO_ACTIVATE_15_PLAYERS,          recvPacket.ReadBit());
        profiles[i]->BoolOptions.set(CUF_UNK_157,                           recvPacket.ReadBit());
        profiles[i]->BoolOptions.set(CUF_DISPLAY_BORDER,                    recvPacket.ReadBit());
        profiles[i]->BoolOptions.set(CUF_DISPLAY_HORIZONTAL_GROUPS,         recvPacket.ReadBit());
        profiles[i]->BoolOptions.set(CUF_AUTO_ACTIVATE_SPEC_1,              recvPacket.ReadBit());
        profiles[i]->BoolOptions.set(CUF_AUTO_ACTIVATE_5_PLAYERS,           recvPacket.ReadBit());
        profiles[i]->BoolOptions.set(CUF_AUTO_ACTIVATE_PVE,                 recvPacket.ReadBit());
        profiles[i]->BoolOptions.set(CUF_DISPLAY_HEAL_PREDICTION,           recvPacket.ReadBit());
        profiles[i]->BoolOptions.set(CUF_DISPLAY_AGGRO_HIGHLIGHT,           recvPacket.ReadBit());
    }

    for (uint8 i = 0; i < count; ++i)
    {
        recvPacket >> profiles[i]->FrameHeight;
        recvPacket >> profiles[i]->Unk146;
        recvPacket >> profiles[i]->HealthText;
        recvPacket >> profiles[i]->FrameWidth;
        recvPacket >> profiles[i]->Unk148;
        recvPacket >> profiles[i]->SortBy;
        recvPacket >> profiles[i]->Unk150;
        profiles[i]->ProfileName = recvPacket.ReadString(strlens[i]);
        recvPacket >> profiles[i]->Unk147;
        recvPacket >> profiles[i]->Unk152;
        recvPacket >> profiles[i]->Unk154;

        GetPlayer()->SaveCUFProfile(i, profiles[i]);
    }

    for (uint8 i = count; i < MAX_CUF_PROFILES; ++i)
        GetPlayer()->SaveCUFProfile(i, NULL);
}

void WorldSession::SendLoadCUFProfiles()
{
    Player* player = GetPlayer();

    uint8 count = player->GetCUFProfilesCount();

    ByteBuffer byteBuffer(25 * count);
    WorldPacket data(SMSG_LOAD_CUF_PROFILES, 5 * count + 25 * count);

    data.WriteBits(count, 19);
    for (uint8 i = 0; i < MAX_CUF_PROFILES; ++i)
    {
        CUFProfile* profile = player->GetCUFProfile(i);
        if (!profile)
            continue;

        data.WriteBit(profile->BoolOptions[CUF_AUTO_ACTIVATE_SPEC_1]);
        data.WriteBit(profile->BoolOptions[CUF_AUTO_ACTIVATE_3_PLAYERS]);
        data.WriteBit(profile->BoolOptions[CUF_UNK_157]);
        data.WriteBit(profile->BoolOptions[CUF_AUTO_ACTIVATE_10_PLAYERS]);
        data.WriteBit(profile->BoolOptions[CUF_AUTO_ACTIVATE_40_PLAYERS]);
        data.WriteBit(profile->BoolOptions[CUF_DISPLAY_BORDER]);
        data.WriteBit(profile->BoolOptions[CUF_USE_CLASS_COLORS]);
        data.WriteBit(profile->BoolOptions[CUF_KEEP_GROUPS_TOGETHER]);
        data.WriteBit(profile->BoolOptions[CUF_DISPLAY_POWER_BAR]);
        data.WriteBits(profile->ProfileName.size(), 7);
        data.WriteBit(profile->BoolOptions[CUF_DISPLAY_PETS]);
        data.WriteBit(profile->BoolOptions[CUF_DISPLAY_AGGRO_HIGHLIGHT]); 
        data.WriteBit(profile->BoolOptions[CUF_UNK_145]);
        data.WriteBit(profile->BoolOptions[CUF_AUTO_ACTIVATE_PVP]);
        data.WriteBit(profile->BoolOptions[CUF_UNK_156]);
        data.WriteBit(profile->BoolOptions[CUF_DISPLAY_MAIN_TANK_AND_ASSIST]);
        data.WriteBit(profile->BoolOptions[CUF_DISPLAY_ONLY_DISPELLABLE_DEBUFFS]);
        data.WriteBit(profile->BoolOptions[CUF_DISPLAY_HORIZONTAL_GROUPS]);
        data.WriteBit(profile->BoolOptions[CUF_AUTO_ACTIVATE_SPEC_2]);
        data.WriteBit(profile->BoolOptions[CUF_DISPLAY_HEAL_PREDICTION]);
        data.WriteBit(profile->BoolOptions[CUF_DISPLAY_NON_BOSS_DEBUFFS]);
        data.WriteBit(profile->BoolOptions[CUF_AUTO_ACTIVATE_25_PLAYERS]);
        data.WriteBit(profile->BoolOptions[CUF_AUTO_ACTIVATE_PVE]);
        data.WriteBit(profile->BoolOptions[CUF_AUTO_ACTIVATE_5_PLAYERS]);
        data.WriteBit(profile->BoolOptions[CUF_AUTO_ACTIVATE_15_PLAYERS]);
        data.WriteBit(profile->BoolOptions[CUF_AUTO_ACTIVATE_2_PLAYERS]);
        
        byteBuffer << uint16(profile->Unk152);
        byteBuffer << uint16(profile->Unk154);
        byteBuffer << uint8(profile->HealthText);
        byteBuffer.WriteString(profile->ProfileName);
        byteBuffer << uint8(profile->Unk147);
        byteBuffer << uint8(profile->Unk146);
        byteBuffer << uint16(profile->FrameHeight);
        byteBuffer << uint8(profile->Unk148);
        byteBuffer << uint8(profile->SortBy);
        byteBuffer << uint16(profile->FrameWidth);
        byteBuffer << uint16(profile->Unk150);
    }

    data.FlushBits();
    data.append(byteBuffer);
    SendPacket(&data);
}

void WorldSession::HandleRequestResearchHistory(WorldPacket& recvPacket)
{
    //TC_LOG_DEBUG("network", "WORLD: CMSG_REQUEST_RESEARCH_HISTORY");

    _player->SendResearchHistory();
}

void WorldSession::HandleSetPreferedCemetery(WorldPacket& recvData)
{
    //TC_LOG_DEBUG("network", "WORLD: Received CMSG_SET_PREFERED_CEMETERY");

    uint32 CemeteryId;
    recvData >> CemeteryId;
}

void WorldSession::HandleCemeteryListRequest(WorldPacket& recvPacket)
{
    GetPlayer()->SendCemeteryList(false);
}

void WorldSession::HandleChangeCurrencyFlags(WorldPacket& recvPacket)
{
    //TC_LOG_DEBUG("network", "WORLD: Received CMSG_SET_CURRENCY_FLAGS");

    uint32 currencyId, flags;
    recvPacket >> flags >> currencyId;

    if (GetPlayer())
        GetPlayer()->ModifyCurrencyFlag(currencyId, uint8(flags));
}

// Dynamic Difficulty system.
void WorldSession::HandleChangePlayerDifficulty(WorldPacket& recvData)
{
    TC_LOG_DEBUG("network", "WORLD: Received CMSG_PLAYER_DIFFICULTY_CHANGE"); // Opcode NYI

    uint32 difficulty = RAID_DIFFICULTY_10MAN_NORMAL;

    Player* player = GetPlayer();
    Map* map = _player->FindMap();
    Group* group = _player->GetGroup();

    Difficulty oldDifficulty = map->GetDifficulty();

    switch (oldDifficulty)
    {
        case RAID_DIFFICULTY_10MAN_NORMAL:
            difficulty = RAID_DIFFICULTY_10MAN_HEROIC;
            break;

        case RAID_DIFFICULTY_10MAN_HEROIC:
            difficulty = RAID_DIFFICULTY_10MAN_NORMAL;
            break;

        case RAID_DIFFICULTY_25MAN_NORMAL:
            difficulty = RAID_DIFFICULTY_25MAN_HEROIC;
            break;

        case RAID_DIFFICULTY_25MAN_HEROIC:
            difficulty = RAID_DIFFICULTY_25MAN_NORMAL;
            break;
        default:
            break;
    }

    // You must be in a raid group to enter a raid, unless the raid is from an older expansion.
    if (!_player->IsGameMaster() && uint32(player->GetExpByLevel()) <= map->Expansion() && (!group || !group->isRaidGroup()))
        return;

    // You must satisfy special requirements to be able to use this. Ex: defeat LK on Heroic to unlock that difficulty and the possibility of changing to it.
    if (!_player->IsGameMaster() && !_player->Satisfy(sObjectMgr->GetAccessRequirement(map->GetId(), Difficulty(difficulty)), map->GetId(), true))
        return;

    uint32 result = DIFF_CHANGE_START; // Default result: Success, start swap. ! DIFF_CHANGE_START causes the sending of CMSG_LOADING_SCREEN. Ends with DIFF_CHANGE_SUCCESS.

    /*** Now do the checks here for every other possible result. ***/

    time_t now = time(NULL);

    // If recently changed difficulty using this system, there is a 5 min cooldown.
    uint32 lastDifficultyChangeTimePassed = timeLastDifficultyChange ? uint32(now - timeLastDifficultyChange) : 0;
    if (lastDifficultyChangeTimePassed && lastDifficultyChangeTimePassed <= 5 * MINUTE)
       result = DIFF_CHANGE_FAIL_COOLDOWN;

    // Here should test for any of the players bound to the instance going as a separate group on heroic. ToDo.
    if (Difficulty(difficulty) == map->GetDifficulty() && (map->GetDifficulty() == RAID_DIFFICULTY_10MAN_HEROIC || map->GetDifficulty() == RAID_DIFFICULTY_25MAN_HEROIC))
        result = DIFF_CHANGE_FAIL_ALREADY_HEROIC;

    // Check the per-player conditions.
    bool groupCombatCooldown = false;
    bool groupInCombat = false;
    bool groupBusy = false;
    bool inEncounter = false;
    bool diffLocked = false;
    ObjectGuid lockedGuid = 0;
    uint32 lastCombatTime = 0;

    // If all passed so far, we test, in order: if an encounter is in progress -> if a player is in combat -> if a player has been in combat -> if anyone is locked on the difficulty -> if anyone is busy.
    if (result != DIFF_CHANGE_FAIL_COOLDOWN && result != DIFF_CHANGE_FAIL_ALREADY_HEROIC)
    {
        if (group)
        {
            if (group->IsLeader(_player->GetGUID()))
            {
                for (GroupReference* itr = group->GetFirstMember(); itr != NULL; itr = itr->next())
                {
                    if (Player* groupGuy = itr->GetSource())
                    {
                        if (groupGuy->GetMap()->IsRaid() && groupGuy->GetInstanceScript() && groupGuy->GetInstanceScript()->IsEncounterInProgress())
                            inEncounter = true;               // A Boss encounter is in progress.
                        else if (groupGuy->GetMap()->IsRaid() && groupGuy->GetInstanceScript() && !groupGuy->GetInstanceScript()->IsEncounterInProgress() && groupGuy->IsInCombat())
                            groupInCombat = true;             // Someone is in combat with mobs or something.
                        else if (groupGuy->GetMap()->IsRaid() && groupGuy->GetInstanceScript() && !groupGuy->GetInstanceScript()->IsEncounterInProgress() && (now - groupGuy->GetLastCombatTime()) < 5 * MINUTE)
                        {
                            groupCombatCooldown = true;       // Someone was in combat recently.
                            lastCombatTime = uint32(now - groupGuy->GetLastCombatTime());
                            break;
                        }
                        else if (groupGuy->GetMap()->IsRaid() && (difficulty == RAID_DIFFICULTY_10MAN_HEROIC || difficulty == RAID_DIFFICULTY_25MAN_HEROIC) && 
                        groupGuy->GetBoundInstance(groupGuy->GetMapId(), Difficulty(difficulty)) && _player->GetBoundInstance(groupGuy->GetMapId(), Difficulty(difficulty)) && 
                        groupGuy->GetBoundInstance(groupGuy->GetMapId(), Difficulty(difficulty))->save != _player->GetBoundInstance(groupGuy->GetMapId(), Difficulty(difficulty))->save)
                        {
                            diffLocked = true;                // Someone is locked.
                            lockedGuid = groupGuy->GetGUID(); // Get first player found with the different instance bind on the Heroic difficulty and use his guid.
                            break;
                        }
                        else if (groupGuy->GetSession()->isLogingOut() || groupGuy->IsNonMeleeSpellCasted(false))
                            groupBusy = true;                 // Someone is busy.
                    }
                }

                if (groupCombatCooldown)
                    result = DIFF_CHANGE_FAIL_COOLDOWN;
                else if (groupInCombat)
                    result = DIFF_CHANGE_FAIL_SOMEONE_IN_COMBAT;
                else if (inEncounter)
                    result = DIFF_CHANGE_FAIL_ENCOUNTER_IN_PROGRESS;
                else if (groupBusy)
                    result = DIFF_CHANGE_FAIL_SOMEONE_BUSY;
                else if (diffLocked)
                    result = DIFF_CHANGE_FAIL_SOMEONE_LOCKED;
            }
        }
        else // Solo, older expansion raids.
        {
            if (_player->GetMap()->IsRaid() && _player->GetInstanceScript() && _player->GetInstanceScript()->IsEncounterInProgress())
                result = DIFF_CHANGE_FAIL_ENCOUNTER_IN_PROGRESS;             // A Boss encounter is in progress.
            else if (_player->GetMap()->IsRaid() && _player->GetInstanceScript() && !_player->GetInstanceScript()->IsEncounterInProgress() && _player->IsInCombat())
                result = DIFF_CHANGE_FAIL_SOMEONE_IN_COMBAT;                 // Is in combat with mobs or something.
            else if (_player->GetMap()->IsRaid() && _player->GetInstanceScript() && !_player->GetInstanceScript()->IsEncounterInProgress() && (now - _player->GetLastCombatTime()) < 5 * MINUTE)
            {
                groupCombatCooldown = true;
                lastCombatTime = uint32(now - _player->GetLastCombatTime());
                result = DIFF_CHANGE_FAIL_COOLDOWN;                          // Was in combat recently.
            }
            else if (_player->GetSession()->isLogingOut() || _player->IsNonMeleeSpellCasted(false))
                result = DIFF_CHANGE_FAIL_SOMEONE_BUSY;                      // Is busy.
            // DIFF_CHANGE_FAIL_SOMEONE_LOCKED; // Player has both Normal and Heroic binds and on the target Heroic difficulty he killed more bosses so he can't switch. ToDo.
        }
    }

    // Send the result and the corresponding values.
    switch(result)
    {
        case DIFF_CHANGE_FAIL_SOMEONE_IN_COMBAT:
        case DIFF_CHANGE_FAIL_EVENT_IN_PROGRESS:
        case DIFF_CHANGE_FAIL_IN_LFR:
        case DIFF_CHANGE_FAIL_ENCOUNTER_IN_PROGRESS:
        case DIFF_CHANGE_FAIL_ALREADY_HEROIC:
        case DIFF_CHANGE_FAIL_CHANGE_STARTED:
        case DIFF_CHANGE_FAIL_SOMEONE_BUSY:
        {
            WorldPacket data(SMSG_PLAYER_DIFFICULTY_CHANGE_RESULT, 1);
            data.WriteBits(result, 4);
            SendPacket(&data);
            break;
        }
        case DIFF_CHANGE_START: // A change started.
        {
            if (group)
            {
                if (group->IsLeader(_player->GetGUID()))
                {
                    for (GroupReference* itr = group->GetFirstMember(); itr != NULL; itr = itr->next()) // Send the loading screen and update the zone for all players here.
                    {
                        if (Player* groupGuy = itr->GetSource())
                        {
                            WorldPacket data(SMSG_PLAYER_DIFFICULTY_CHANGE_RESULT, 1 + 4);
                            data.WriteBits(result, 4);

                            data.WriteBit(groupCombatCooldown); // Should be false here.
                            data.FlushBits();
                            data << uint32(lastDifficultyChangeTimePassed);
                            groupGuy->GetSession()->SendPacket(&data);
                        }
                    }
                }
            }
            else // Solo, older expansion raids.
            {
                WorldPacket data(SMSG_PLAYER_DIFFICULTY_CHANGE_RESULT, 1 + 4);
                data.WriteBits(result, 4);

                data.WriteBit(groupCombatCooldown); // Should be false here.
                data.FlushBits();
                data << uint32(lastDifficultyChangeTimePassed);
                SendPacket(&data);
            }
            result = DIFF_CHANGE_SUCCESS; // It finally worked!
            break;
        }
        case DIFF_CHANGE_FAIL_SOMEONE_LOCKED:
        {
            WorldPacket data(SMSG_PLAYER_DIFFICULTY_CHANGE_RESULT, 1 + 8 + 1);
            data.WriteBits(result, 4);

            uint8 bitOrder[8] = { 6, 0, 4, 5, 2, 1, 3, 7 };
            uint8 byteOrder[8] = { 1, 6, 5, 0, 7, 2, 4, 3 };

            data.WriteBitInOrder(lockedGuid, bitOrder);
            data.FlushBits();
            data.WriteBytesSeq(lockedGuid, byteOrder);
            SendPacket(&data);
            break;
        }
        case DIFF_CHANGE_FAIL_COOLDOWN:
        {
            WorldPacket data(SMSG_PLAYER_DIFFICULTY_CHANGE_RESULT, 1 + 4);
            data.WriteBits(result, 4);

            data.WriteBit(groupCombatCooldown); // Bit controls difficulty changed recently / someone was in combat.
            data.FlushBits();
            data << uint32((5 * MINUTE) - (groupCombatCooldown ? lastCombatTime : lastDifficultyChangeTimePassed));
            SendPacket(&data);
            break;
        }
        // TODO: Complete this and make the checks using bool Player::Satisfy(AccessRequirement const* ar, uint32 target_map, bool report).
        case DIFF_CHANGE_SHOW_AREA_TRIGGER_CANNOT_ENTER_TEXT:
        {
            uint32 messageDifficultyId = 1866; // "Heroic Difficulty requires you to be level 90." - Temp. MapDifficulty.dbc, Id.
            WorldPacket data(SMSG_PLAYER_DIFFICULTY_CHANGE_RESULT, 1 + 4);
            data.WriteBits(result, 4);
            data << uint32(messageDifficultyId);
            SendPacket(&data);
            break;
        }
        default:
        {
            WorldPacket data(SMSG_PLAYER_DIFFICULTY_CHANGE_RESULT, 1);
            data.WriteBits(result, 4);
            SendPacket(&data);
            break;
        }
    }

    if (result == DIFF_CHANGE_SUCCESS)  // It finally worked! Update everything.
    {
        timeLastDifficultyChange = now;

        if (group)
        {
            if (group->IsLeader(_player->GetGUID()))
            {
                // Set raid difficulty.
                group->SetRaidDifficulty(Difficulty(difficulty));

                // Add the leader the DynamicDifficultyMap if he doesn't have it.
                if (!_player->HasDynamicDifficultyMap(map->GetId()))
                    _player->AddDynamicDifficultyMap(map->GetId());

                // Set the leader as being on a DynamicDifficultyMap if he is not.
                if (!_player->IsOnDynamicDifficultyMap())
                    _player->SetOnDynamicDifficultyMap(true);

                // Update only the leader's map. All group members will be added to it and on the same instance map.
                Map* map2 = sMapMgr->CreateMap(map->GetId(), _player);

                for (GroupReference* itr = group->GetFirstMember(); itr != NULL; itr = itr->next()) // Remove the loading screen and update the zone for all players here.
                {
                    if (Player* groupGuy = itr->GetSource())
                    {
                        // Add the players the DynamicDifficultyMap if they don't have it.
                        if (!groupGuy->HasDynamicDifficultyMap(map->GetId()))
                            groupGuy->AddDynamicDifficultyMap(map->GetId());

                        // Set the players as being on a DynamicDifficultyMap if they are not.
                        if (!groupGuy->IsOnDynamicDifficultyMap())
                            groupGuy->SetOnDynamicDifficultyMap(true);

                        // Update everything.
                        if (groupGuy->IsInWorld())
                        {
                            map->RemovePlayerFromMap(groupGuy, false);
                            sScriptMgr->OnPlayerLeaveMap(map, groupGuy);
                        }

                        groupGuy->ResetMap();
                        groupGuy->SetMap(map2);

                        groupGuy->SendInitialPacketsBeforeAddToMap();

                        if (!groupGuy->GetMap()->AddPlayerToMap(groupGuy))
                            TC_LOG_ERROR("network", "Failed to change dynamic difficulty for player %s (%d) (G) to map %s because of unknown reason!", groupGuy->GetName().c_str(), groupGuy->GetGUIDLow(), map->GetMapName());

                        groupGuy->SendInitialPacketsAfterAddToMap();

                        if (time_t timeReset = uint64(sWorld->getWorldState(WS_WEEKLY_QUEST_RESET_TIME)))
                        {
                            uint32 timeleft = uint32(timeReset - time(NULL));
                            groupGuy->SendInstanceResetWarning(map->GetId(), Difficulty(difficulty), timeleft, true);
                        }

                        // Send the difficulty change result to all players to remove their loading screen.
                        WorldPacket data(SMSG_PLAYER_DIFFICULTY_CHANGE_RESULT, 1 + 4 + 4);
                        data.WriteBits(result, 4);

                        data << uint32(difficulty);
                        data << uint32(map->GetId());
                        groupGuy->GetSession()->SendPacket(&data);
                    }
                }
            }
        }
        else // Solo, older expansion raids.
        {
            // Set raid difficulty.
            _player->SetRaidDifficulty(Difficulty(difficulty));

            // Add the player the DynamicDifficultyMap if he doesn't have it.
            if (!_player->HasDynamicDifficultyMap(map->GetId()))
                _player->AddDynamicDifficultyMap(map->GetId());

            // Set the player as being on a DynamicDifficultyMap if he is not.
            if (!_player->IsOnDynamicDifficultyMap())
                _player->SetOnDynamicDifficultyMap(true);

            // Update everything.
            if (_player->IsInWorld())
            {
                map->RemovePlayerFromMap(_player, false);
                sScriptMgr->OnPlayerLeaveMap(map, player);
            }

            Map* map2 = sMapMgr->CreateMap(map->GetId(), _player);

            _player->ResetMap();
            _player->SetMap(map2);

            _player->SendInitialPacketsBeforeAddToMap();

            if (!_player->GetMap()->AddPlayerToMap(_player))
                TC_LOG_ERROR("network", "Failed to change dynamic difficulty for player %s (%d) to map %s because of unknown reason!", _player->GetName().c_str(), _player->GetGUIDLow(), map->GetMapName());

            _player->SendInitialPacketsAfterAddToMap();

            if (time_t timeReset = uint64(sWorld->getWorldState(WS_WEEKLY_QUEST_RESET_TIME)))
            {
                uint32 timeleft = uint32(timeReset - time(NULL));
                _player->SendInstanceResetWarning(map->GetId(), Difficulty(difficulty), timeleft, true);
            }

            // Send the difficulty change result to the player to remove his loading screen.
            WorldPacket data(SMSG_PLAYER_DIFFICULTY_CHANGE_RESULT, 1 + 4 + 4);
            data.WriteBits(result, 4);

            data << uint32(difficulty);
            data << uint32(map2->GetId());
            SendPacket(&data);
        }
    }
}
