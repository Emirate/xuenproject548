/*
 * Copyright (C) 2008-2016 TrinityCore <http://www.trinitycore.org/>
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 3 of the License, or (at your
 * option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program. If not, see <http://www.gnu.org/licenses/>.
 */

/*
 * Ordered alphabetically using scriptname.
 * Scriptnames of files in this file should be prefixed with "spell_racial_".
 */

#include "ScriptMgr.h"
#include "SpellScript.h"
#include "SpellAuraEffects.h"
#include "Player.h"

enum RacialSpells
{
    SPELL_RUNNING_WILD_MODEL_MALE                              = 29422,
    SPELL_RUNNING_WILD_MODEL_FEMALE                            = 29423,
    SPELL_ALTERED_FORM                                         = 97709,
    SPELL_PVP_TRINKET_NEUTRAL                                  = 97979,
    SPELL_PVP_TRINKET_ALLIANCE                                 = 97403,
    SPELL_PVP_TRINKET_HORDE                                    = 97404,
    SPELL_WILL_OF_THE_FORSAKEN_COOLDOWN_TRIGGER                = 72752,
    SPELL_WILL_OF_THE_FORSAKEN_COOLDOWN_TRIGGER_PVP_TRINKET    = 72757,
    SPELL_REPLENISHMENT                                        = 57669,
    SPELL_CANNIBALIZE_TRIGGERED                                = 20578
};

// Gift of the Naaru - 59548 or 59547 or 59545 or 59544 or 59543 or 59542 or 121093
class spell_racial_gift_of_the_naaru : public SpellScriptLoader
{
    public:
        spell_racial_gift_of_the_naaru() : SpellScriptLoader("spell_racial_gift_of_the_naaru") { }

        class spell_racial_gift_of_the_naaru_AuraScript : public AuraScript
        {
            PrepareAuraScript(spell_racial_gift_of_the_naaru_AuraScript);

            void CalculateAmount(constAuraEffectPtr aurEff, int32& amount, bool& /*canBeRecalculated*/)
            {
                if (!GetCaster())
                    return;

                amount = GetCaster()->CountPctFromMaxHealth(4);
            }

            void Register()
            {
                DoEffectCalcAmount += AuraEffectCalcAmountFn(spell_racial_gift_of_the_naaru_AuraScript::CalculateAmount, EFFECT_0, SPELL_AURA_PERIODIC_HEAL);
            }
        };

        AuraScript* GetAuraScript() const
        {
            return new spell_racial_gift_of_the_naaru_AuraScript();
        }
};

// Blood Fury - 20572 or Blood Fury - 33702
class spell_racial_blood_fury : public SpellScriptLoader
{
    public:
        spell_racial_blood_fury() : SpellScriptLoader("spell_racial_blood_fury") { }

        class spell_racial_blood_fury_SpellScript : public SpellScript
        {
            PrepareSpellScript(spell_racial_blood_fury_SpellScript);

            void HandleOnHit()
            {
                if (GetCaster()->HasAura(GetSpellInfo()->Id) && GetCaster()->getClass() != CLASS_HUNTER)
                {
                    GetCaster()->RemoveAura(GetSpellInfo()->Id);
                    GetCaster()->CastSpell(GetCaster(), 33697, true);
                }
            }

            void Register()
            {
                OnHit += SpellHitFn(spell_racial_blood_fury_SpellScript::HandleOnHit);
            }
        };

        SpellScript* GetSpellScript() const
        {
            return new spell_racial_blood_fury_SpellScript();
        }
};

class spell_racial_pvp_trinket_wotf_shared_cd : public SpellScriptLoader
{
    public:
        spell_racial_pvp_trinket_wotf_shared_cd() : SpellScriptLoader("spell_racial_pvp_trinket_wotf_shared_cd") { }

        class spell_racial_pvp_trinket_wotf_shared_cd_SpellScript : public SpellScript
        {
            PrepareSpellScript(spell_racial_pvp_trinket_wotf_shared_cd_SpellScript);

            bool Load()
            {
                return GetCaster()->GetTypeId() == TYPEID_PLAYER;
            }

            bool Validate(SpellInfo const* /*spellInfo*/)
            {
                if (!sSpellMgr->GetSpellInfo(SPELL_WILL_OF_THE_FORSAKEN_COOLDOWN_TRIGGER) ||
                    !sSpellMgr->GetSpellInfo(SPELL_WILL_OF_THE_FORSAKEN_COOLDOWN_TRIGGER_PVP_TRINKET))
                    return false;
                return true;
            }

            void HandleCooldownTrigger()
            {
                if (GetSpellInfo()->Id == 7744) // Will of the Forsaken
                    GetCaster()->CastSpell(GetCaster(), SPELL_WILL_OF_THE_FORSAKEN_COOLDOWN_TRIGGER_PVP_TRINKET, false);

                if (GetSpellInfo()->Id == 42292) // PvP Trinket
                    GetCaster()->CastSpell(GetCaster(), SPELL_WILL_OF_THE_FORSAKEN_COOLDOWN_TRIGGER, false);
            }

            void Register()
            {
                AfterCast += SpellCastFn(spell_racial_pvp_trinket_wotf_shared_cd_SpellScript::HandleCooldownTrigger);
            }
        };

        SpellScript* GetSpellScript() const
        {
            return new spell_racial_pvp_trinket_wotf_shared_cd_SpellScript();
        }
};

// Running Wild - 87840
class spell_racial_running_wild : public SpellScriptLoader
{
    public:
        spell_racial_running_wild() : SpellScriptLoader("spell_racial_running_wild") { }

        class spell_racial_running_wild_AuraScript : public AuraScript
        {
            PrepareAuraScript(spell_racial_running_wild_AuraScript);

            bool Validate(SpellInfo const* /*spell*/)
            {
                if (!sCreatureDisplayInfoStore.LookupEntry(SPELL_RUNNING_WILD_MODEL_MALE))
                    return false;
                if (!sCreatureDisplayInfoStore.LookupEntry(SPELL_RUNNING_WILD_MODEL_FEMALE))
                    return false;
                return true;
            }

            void HandleMount(constAuraEffectPtr aurEff, AuraEffectHandleModes /*mode*/)
            {
                Unit* target = GetTarget();
                PreventDefaultAction();

                target->Mount(target->getGender() == GENDER_FEMALE ? SPELL_RUNNING_WILD_MODEL_FEMALE : SPELL_RUNNING_WILD_MODEL_MALE, 0, 0);

                // Cast speed aura
                if (MountCapabilityEntry const* mountCapability = sMountCapabilityStore.LookupEntry(aurEff->GetAmount()))
                    target->CastSpell(target, mountCapability->SpeedModSpell, TRIGGERED_FULL_MASK);
            }

            void Register()
            {
                OnEffectApply += AuraEffectApplyFn(spell_racial_running_wild_AuraScript::HandleMount, EFFECT_1, SPELL_AURA_MOUNTED, AURA_EFFECT_HANDLE_REAL);
            }
        };

        class spell_racial_running_wild_SpellScript : public SpellScript
        {
            PrepareSpellScript(spell_racial_running_wild_SpellScript);

            bool Validate(SpellInfo const* /*spell*/)
            {
                if (!sSpellMgr->GetSpellInfo(SPELL_ALTERED_FORM))
                    return false;
                return true;
            }

            bool Load()
            {
                // Definitely not a good thing, but currently the only way to do something at cast start.
                // Should be replaced as soon as possible with a new hook: BeforeCastStart.
                GetCaster()->CastSpell(GetCaster(), SPELL_ALTERED_FORM, TRIGGERED_FULL_MASK);
                return false;
            }

            void Register()
            {
            }
        };

        AuraScript* GetAuraScript() const
        {
            return new spell_racial_running_wild_AuraScript();
        }

        SpellScript* GetSpellScript() const
        {
            return new spell_racial_running_wild_SpellScript();
        }
};

// Two Forms - 68996
class spell_racial_two_forms : public SpellScriptLoader
{
    public:
        spell_racial_two_forms() : SpellScriptLoader("spell_racial_two_forms") { }

        class spell_racial_two_forms_SpellScript : public SpellScript
        {
            PrepareSpellScript(spell_racial_two_forms_SpellScript);

            SpellCastResult CheckCast()
            {
                if (GetCaster()->IsInCombat())
                {
                    SetCustomCastResultMessage(SPELL_CUSTOM_ERROR_CANT_TRANSFORM);
                    return SPELL_FAILED_CUSTOM_ERROR;
                }

                // Player cannot transform to human form if he is forced to be worgen for some reason (Darkflight)
                if (GetCaster()->GetAuraEffectsByType(SPELL_AURA_WORGEN_ALTERED_FORM).size() > 1)
                {
                    SetCustomCastResultMessage(SPELL_CUSTOM_ERROR_CANT_TRANSFORM);
                    return SPELL_FAILED_CUSTOM_ERROR;
                }

                return SPELL_CAST_OK;
            }

            void HandleTransform(SpellEffIndex effIndex)
            {
                Unit* target = GetHitUnit();
                PreventHitDefaultEffect(effIndex);
                if (target->HasAuraType(SPELL_AURA_WORGEN_ALTERED_FORM))
                    target->RemoveAurasByType(SPELL_AURA_WORGEN_ALTERED_FORM);
                else    // Basepoints 1 for this aura control whether to trigger transform transition animation or not.
                    target->CastCustomSpell(SPELL_ALTERED_FORM, SPELLVALUE_BASE_POINT0, 1, target, true);
            }

            void Register()
            {
                OnCheckCast += SpellCheckCastFn(spell_racial_two_forms_SpellScript::CheckCast);
                OnEffectHitTarget += SpellEffectFn(spell_racial_two_forms_SpellScript::HandleTransform, EFFECT_0, SPELL_EFFECT_DUMMY);
            }
        };

        SpellScript* GetSpellScript() const
        {
            return new spell_racial_two_forms_SpellScript();
        }
};

// Darkflight - 68992
class spell_racial_darkflight : public SpellScriptLoader
{
    public:
        spell_racial_darkflight() : SpellScriptLoader("spell_racial_darkflight") { }

        class spell_racial_darkflight_SpellScript : public SpellScript
        {
            PrepareSpellScript(spell_racial_darkflight_SpellScript);

            void TriggerTransform()
            {
                GetCaster()->CastSpell(GetCaster(), SPELL_ALTERED_FORM, TRIGGERED_FULL_MASK);
            }

            void Register()
            {
                AfterCast += SpellCastFn(spell_racial_darkflight_SpellScript::TriggerTransform);
            }
        };

        SpellScript* GetSpellScript() const
        {
            return new spell_racial_darkflight_SpellScript();
        }
};

class spell_racial_replenishment : public SpellScriptLoader
{
    public:
        spell_racial_replenishment() : SpellScriptLoader("spell_racial_replenishment") { }

        class spell_racial_replenishment_AuraScript : public AuraScript
        {
            PrepareAuraScript(spell_racial_replenishment_AuraScript);

            bool Validate(SpellInfo const* /*spellInfo*/)
            {
                if (!sSpellMgr->GetSpellInfo(SPELL_REPLENISHMENT))
                    return false;
                return true;
            }

            bool Load()
            {
                return GetUnitOwner()->GetPower(POWER_MANA);
            }

            void CalculateAmount(constAuraEffectPtr /*aurEff*/, int32& amount, bool& /*canBeRecalculated*/)
            {
                switch (GetSpellInfo()->Id)
                {
                    case SPELL_REPLENISHMENT:
                        amount = GetUnitOwner()->GetMaxPower(POWER_MANA) * 0.002f;
                        break;
                    default:
                        break;
                }
            }

            void Register()
            {
                DoEffectCalcAmount += AuraEffectCalcAmountFn(spell_racial_replenishment_AuraScript::CalculateAmount, EFFECT_0, SPELL_AURA_PERIODIC_ENERGIZE);
            }
        };

        AuraScript* GetAuraScript() const
        {
            return new spell_racial_replenishment_AuraScript();
        }
};

// Cannibalize - 20577
class spell_racial_cannibalize : public SpellScriptLoader
{
    public:
        spell_racial_cannibalize() : SpellScriptLoader("spell_racial_cannibalize") { }

        class spell_racial_cannibalize_SpellScript : public SpellScript
        {
            PrepareSpellScript(spell_racial_cannibalize_SpellScript);

            bool Validate(SpellInfo const* /*spellInfo*/)
            {
                if (!sSpellMgr->GetSpellInfo(SPELL_CANNIBALIZE_TRIGGERED))
                    return false;
                return true;
            }

            SpellCastResult CheckIfCorpseNear()
            {
                Unit* caster = GetCaster();
                float max_range = GetSpellInfo()->GetMaxRange(false);
                WorldObject* result = NULL;
                // Search for nearby enemy corpse in range
                Trinity::AnyDeadUnitSpellTargetInRangeCheck check(caster, max_range, GetSpellInfo(), TARGET_CHECK_ENEMY);
                Trinity::WorldObjectSearcher<Trinity::AnyDeadUnitSpellTargetInRangeCheck> searcher(caster, result, check);
                caster->GetMap()->VisitFirstFound(caster->m_positionX, caster->m_positionY, max_range, searcher);
                if (!result)
                    return SPELL_FAILED_NO_EDIBLE_CORPSES;
                return SPELL_CAST_OK;
            }

            void HandleDummy(SpellEffIndex /*effIndex*/)
            {
                GetCaster()->CastSpell(GetCaster(), SPELL_CANNIBALIZE_TRIGGERED, false);
            }

            void Register()
            {
                OnEffectHit += SpellEffectFn(spell_racial_cannibalize_SpellScript::HandleDummy, EFFECT_0, SPELL_EFFECT_DUMMY);
                OnCheckCast += SpellCheckCastFn(spell_racial_cannibalize_SpellScript::CheckIfCorpseNear);
            }
        };

        SpellScript* GetSpellScript() const
        {
            return new spell_racial_cannibalize_SpellScript();
        }
};

void AddSC_racial_spell_scripts()
{
    new spell_racial_gift_of_the_naaru();
    new spell_racial_blood_fury();
    new spell_racial_pvp_trinket_wotf_shared_cd();
    new spell_racial_running_wild();
    new spell_racial_two_forms();
    new spell_racial_darkflight();
    new spell_racial_replenishment();
    new spell_racial_cannibalize();
}