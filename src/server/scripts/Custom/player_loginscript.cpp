/*
* Copyright (C) 2008-2016 TrinityCore <http://www.trinitycore.org/>
*
* This program is free software; you can redistribute it and/or modify it
* under the terms of the GNU General Public License as published by the
* Free Software Foundation; either version 3 of the License, or (at your
* option) any later version.
*
* This program is distributed in the hope that it will be useful, but WITHOUT
* ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
* FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
* more details.
*
* You should have received a copy of the GNU General Public License along
* with this program. If not, see <http://www.gnu.org/licenses/>.
*/

#include "ScriptPCH.h"

enum AutoLearnSkills
{
    AUTO_LEARN_FIRST_AID,
    AUTO_LEARN_COOKING,

    AUTO_LEARN_MAX
};

struct AutoLearnSkillInfo
{
    const uint32 spellId;
    const uint32 skillId;
    const uint32 trainerId;

    AutoLearnSkillInfo(const uint32 _spellId, const uint32 _skillId, const uint32 _trainerId) :
        spellId(_spellId), skillId(_skillId), trainerId(_trainerId) { }
};

const AutoLearnSkillInfo AutoLearnSkillList[AUTO_LEARN_MAX] =
{
    // First Aid
    AutoLearnSkillInfo(110406, SKILL_FIRST_AID, 59077),

    // Cooking
    AutoLearnSkillInfo(104381, SKILL_COOKING, 59077)

    // Fishing TODO
};

class player_loginscript : public PlayerScript
{
private:
    std::set<uint32> m_glyphIds;

public:
    player_loginscript() : PlayerScript("player_loginscript")
    {
        // Extract all the glyphs from the ItemTemplate store.
        ExtractAllGlyphs();
    }

    void OnLogin(Player* player)
    {
        if (!player)
            return;

        // Learn Dual Specialization
        LearnDualSpec(player);

        // Learn all glyphs
        LearnAllGlyphs(player);

        // Auto learn skills
        for (uint8 i = AUTO_LEARN_FIRST_AID; i < AUTO_LEARN_MAX; ++i)
        {
            AutoLearnSkillInfo const* info = &AutoLearnSkillList[i];
            LearnSkill(player, info->spellId, info->skillId, info->trainerId);
        }
    }

    void LearnDualSpec(Player* player)
    {
        if (player->GetSpecsCount() == 1 && player->getLevel() >= sWorld->getIntConfig(CONFIG_MIN_DUALSPEC_LEVEL))
        {
            player->CastSpell(player, 63680, true, NULL, NULL, player->GetGUID());
            player->CastSpell(player, 63624, true, NULL, NULL, player->GetGUID());
        }
    }

    void LearnAllGlyphs(Player* player)
    {
        for (std::set<uint32>::const_iterator itr = m_glyphIds.begin(); itr != m_glyphIds.end(); ++itr)
            if (ItemTemplate const* itemTemplate = sObjectMgr->GetItemTemplate(*itr))
                if (itemTemplate->AllowableClass & player->getClassMask())
                    for (uint8 i = 0; i < MAX_ITEM_PROTO_SPELLS; ++i)
                    {
                        uint32 spellId = itemTemplate->Spells[i].SpellId;
                        if (IsValidSpellForPlayer(player, spellId))
                            player->learnSpell(spellId, false);
                    }
    }

    bool IsValidSpellForPlayer(Player* player, uint32 spellId)
    {
        if (SpellInfo const* spellInfo = sSpellMgr->GetSpellInfo(spellId))
            return SpellMgr::IsSpellValid(spellInfo, player, false) && !player->HasSpell(spellId);

        return false;
    }

    void ExtractAllGlyphs()
    {
        for (ItemTemplateContainer::const_iterator itr = sObjectMgr->GetItemTemplateStore()->begin();
             itr!=sObjectMgr->GetItemTemplateStore()->end(); ++itr)
            if (ItemTemplate const* itemTemplate = &itr->second)
                if (itemTemplate->Class == ITEM_CLASS_GLYPH)
                    m_glyphIds.insert(itemTemplate->ItemId);
    }

    void LearnSkill(Player* player, uint32 spellId, uint32 skillId, uint32 trainerId)
    {
        player->learnSpellHighRank(spellId);

        uint16 maxLevel = player->GetPureMaxSkillValue(skillId);
        player->SetSkill(skillId, player->GetSkillStep(skillId), maxLevel, maxLevel);

        LearnProfessionRecipes(player);
    }

    void LearnProfessionRecipes(Player* player)
    {
        // Learn recipes for max First Aid
        player->learnSpell(3275, false);
        player->learnSpell(3276, false);
        player->learnSpell(7934, false);
        player->learnSpell(3277, false);
        player->learnSpell(3278, false);
        player->learnSpell(7935, false);
        player->learnSpell(7928, false);
        player->learnSpell(7929, false);
        player->learnSpell(10840, false);
        player->learnSpell(10841, false);
        player->learnSpell(18629, false);
        player->learnSpell(18630, false);
        player->learnSpell(23787, false);
        player->learnSpell(27032, false);
        player->learnSpell(27033, false);
        player->learnSpell(45545, false);
        player->learnSpell(45546, false);
        player->learnSpell(74556, false);
        player->learnSpell(74557, false);
        player->learnSpell(88893, false);
        player->learnSpell(74558, false);
        player->learnSpell(102698, false);
        player->learnSpell(102699, false);

        // Learn recipes for max Cooking
        player->learnSpell(65454, false);
        player->learnSpell(37836, false);
        player->learnSpell(7751, false);
        player->learnSpell(15935, false);
        player->learnSpell(21143, false);
        player->learnSpell(33276, false);
        player->learnSpell(33277, false);
        player->learnSpell(7752, false);
        player->learnSpell(43779, false);
        player->learnSpell(66038, false);
        player->learnSpell(62050, false);
        player->learnSpell(6417, false);
        player->learnSpell(3397, false);
        player->learnSpell(6412, false);
        player->learnSpell(2539, false);
        player->learnSpell(2795, false);
        player->learnSpell(6413, false);
        player->learnSpell(6414, false);
        player->learnSpell(21144, false);
        player->learnSpell(8607, false);
        player->learnSpell(93741, false);
        player->learnSpell(33278, false);
        player->learnSpell(6499, false);
        player->learnSpell(2541, false);
        player->learnSpell(6415, false);
        player->learnSpell(2542, false);
        player->learnSpell(7754, false);
        player->learnSpell(7753, false);
        player->learnSpell(7827, false);
        player->learnSpell(6416, false);
        player->learnSpell(2543, false);
        player->learnSpell(3371, false);
        player->learnSpell(28267, false);
        player->learnSpell(9513, false);
        player->learnSpell(2544, false);
        player->learnSpell(3370, false);
        player->learnSpell(2546, false);
        player->learnSpell(25704, false);
        player->learnSpell(2545, false);
        player->learnSpell(8238, false);
        player->learnSpell(6501, false);
        player->learnSpell(3372, false);
        player->learnSpell(45695, false);
        player->learnSpell(2547, false);
        player->learnSpell(7755, false);
        player->learnSpell(6418, false);
        player->learnSpell(2549, false);
        player->learnSpell(66036, false);
        player->learnSpell(62044, false);
        player->learnSpell(2548, false);
        player->learnSpell(3377, false);
        player->learnSpell(6419, false);
        player->learnSpell(3373, false);
        player->learnSpell(6500, false);
        player->learnSpell(15853, false);
        player->learnSpell(3398, false);
        player->learnSpell(3376, false);
        player->learnSpell(24418, false);
        player->learnSpell(3399, false);
        player->learnSpell(66035, false);
        player->learnSpell(62049, false);
        player->learnSpell(13028, false);
        player->learnSpell(7828, false);
        player->learnSpell(4094, false);
        player->learnSpell(15863, false);
        player->learnSpell(7213, false);
        player->learnSpell(15856, false);
        player->learnSpell(15861, false);
        player->learnSpell(20916, false);
        player->learnSpell(15865, false);
        player->learnSpell(15855, false);
        player->learnSpell(25954, false);
        player->learnSpell(3400, false);
        player->learnSpell(15906, false);
        player->learnSpell(15910, false);
        player->learnSpell(21175, false);
        player->learnSpell(62051, false);
        player->learnSpell(66034, false);
        player->learnSpell(18239, false);
        player->learnSpell(18241, false);
        player->learnSpell(15933, false);
        player->learnSpell(15915, false);
        player->learnSpell(18238, false);
        player->learnSpell(22480, false);
        player->learnSpell(20626, false);
        player->learnSpell(18240, false);
        player->learnSpell(18242, false);
        player->learnSpell(64054, false);
        player->learnSpell(46684, false);
        player->learnSpell(46688, false);
        player->learnSpell(18243, false);
        player->learnSpell(18244, false);
        player->learnSpell(18247, false);
        player->learnSpell(18245, false);
        player->learnSpell(18246, false);
        player->learnSpell(22761, false);
        player->learnSpell(66037, false);
        player->learnSpell(62045, false);
        player->learnSpell(24801, false);
        player->learnSpell(33290, false);
        player->learnSpell(43761, false);
        player->learnSpell(33279, false);
        player->learnSpell(36210, false);
        player->learnSpell(33291, false);
        player->learnSpell(33284, false);
        player->learnSpell(43758, false);
        player->learnSpell(25659, false);
        player->learnSpell(43772, false);
        player->learnSpell(33292, false);
        player->learnSpell(33286, false);
        player->learnSpell(42296, false);
        player->learnSpell(33293, false);
        player->learnSpell(33294, false);
        player->learnSpell(45022, false);
        player->learnSpell(43707, false);
        player->learnSpell(43765, false);
        player->learnSpell(33295, false);
        player->learnSpell(33287, false);
        player->learnSpell(33289, false);
        player->learnSpell(33288, false);
        player->learnSpell(38868, false);
        player->learnSpell(38867, false);
        player->learnSpell(57421, false);
        player->learnSpell(58523, false);
        player->learnSpell(58525, false);
        player->learnSpell(58521, false);
        player->learnSpell(58512, false);
        player->learnSpell(45561, false);
        player->learnSpell(45562, false);
        player->learnSpell(45560, false);
        player->learnSpell(45569, false);
        player->learnSpell(58065, false);
        player->learnSpell(45563, false);
        player->learnSpell(45549, false);
        player->learnSpell(45566, false);
        player->learnSpell(45565, false);
        player->learnSpell(45553, false);
        player->learnSpell(45552, false);
        player->learnSpell(45550, false);
        player->learnSpell(45564, false);
        player->learnSpell(45551, false);
        player->learnSpell(33296, false);
        player->learnSpell(42302, false);
        player->learnSpell(42305, false);
        player->learnSpell(53056, false);
        player->learnSpell(45554, false);
        player->learnSpell(62350, false);
        player->learnSpell(64358, false);
        player->learnSpell(57441, false);
        player->learnSpell(57438, false);
        player->learnSpell(57435, false);
        player->learnSpell(57439, false);
        player->learnSpell(57442, false);
        player->learnSpell(45568, false);
        player->learnSpell(57436, false);
        player->learnSpell(45570, false);
        player->learnSpell(45555, false);
        player->learnSpell(45559, false);
        player->learnSpell(45567, false);
        player->learnSpell(57434, false);
        player->learnSpell(57437, false);
        player->learnSpell(57440, false);
        player->learnSpell(45557, false);
        player->learnSpell(45571, false);
        player->learnSpell(57433, false);
        player->learnSpell(45556, false);
        player->learnSpell(57443, false);
        player->learnSpell(45558, false);
        player->learnSpell(88015, false);
        player->learnSpell(58527, false);
        player->learnSpell(58528, false);
        player->learnSpell(88006, false);
        player->learnSpell(88017, false);
        player->learnSpell(57423, false);
        player->learnSpell(88022, false);
        player->learnSpell(88045, false);
        player->learnSpell(88012, false);
        player->learnSpell(88024, false);
        player->learnSpell(88028, false);
        player->learnSpell(88030, false);
        player->learnSpell(88035, false);
        player->learnSpell(88037, false);
        player->learnSpell(88047, false);
        player->learnSpell(88021, false);
        player->learnSpell(88033, false);
        player->learnSpell(88046, false);
        player->learnSpell(88018, false);
        player->learnSpell(96133, false);
        player->learnSpell(88003, false);
        player->learnSpell(88004, false);
        player->learnSpell(88005, false);
        player->learnSpell(88034, false);
        player->learnSpell(88014, false);
        player->learnSpell(88016, false);
        player->learnSpell(88020, false);
        player->learnSpell(88025, false);
        player->learnSpell(88031, false);
        player->learnSpell(88039, false);
        player->learnSpell(88042, false);
        player->learnSpell(88011, false);
        player->learnSpell(88013, false);
        player->learnSpell(88019, false);
        player->learnSpell(88036, false);
        player->learnSpell(88044, false);
        player->learnSpell(105190, false);
        player->learnSpell(105194, false);
        player->learnSpell(124029, false);
        player->learnSpell(124032, false);
        player->learnSpell(125122, false);
        player->learnSpell(125121, false);
        player->learnSpell(125123, false);
        player->learnSpell(125120, false);
        player->learnSpell(125080, false);
        player->learnSpell(124230, false);
        player->learnSpell(124232, false);
        player->learnSpell(124231, false);
        player->learnSpell(124234, false);
        player->learnSpell(124229, false);
        player->learnSpell(124233, false);
        player->learnSpell(104297, false);
        player->learnSpell(125078, false);
        player->learnSpell(124228, false);
        player->learnSpell(124226, false);
        player->learnSpell(124224, false);
        player->learnSpell(124223, false);
        player->learnSpell(124227, false);
        player->learnSpell(124225, false);
        player->learnSpell(125067, false);
        player->learnSpell(125117, false);
    }
};

void AddSC_player_loginscript()
{
    new player_loginscript;
}