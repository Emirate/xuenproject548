/*
 * Copyright (C) 2008-2016 TrinityCore <http://www.trinitycore.org/>
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 3 of the License, or (at your
 * option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program. If not, see <http://www.gnu.org/licenses/>.
 */

 /*
 * Scriptnames of files in this file should be prefixed with "npc_pet_monk_".
 */

#include "ScriptMgr.h"
#include "ScriptedCreature.h"
#include "Pet.h"

enum xuenSpells
{
    CRACKLING_TIGER_LIGHTNING   = 123996,
    PROVOKE                     = 130793,
    TIGER_LEAP                  = 124007,
    TIGER_LUST                  = 124009
};

enum xuenEvents
{
    EVENT_LIGHTNING = 1,
    EVENT_PROVOKE   = 2,
    EVENT_LEAP      = 3
};

class npc_pet_monk_xuen_the_white_tiger : public CreatureScript
{
    public:
        npc_pet_monk_xuen_the_white_tiger() : CreatureScript("npc_pet_monk_xuen_the_white_tiger") { }

        struct npc_pet_monk_xuen_the_white_tigerAI : public ScriptedAI
        {
            EventMap events;

            npc_pet_monk_xuen_the_white_tigerAI(Creature* creature) : ScriptedAI(creature)
            {
                me->SetReactState(REACT_DEFENSIVE);
            }

            void Reset()
            {
                events.Reset();

                events.ScheduleEvent(EVENT_LIGHTNING, 500);
                events.ScheduleEvent(EVENT_LEAP,      200);
                events.ScheduleEvent(EVENT_PROVOKE,   200);
            }

            void UpdateAI(uint32 diff)
            {
                if (!UpdateVictim())
                {
                    if (me->GetOwner())
                        if (Unit* victim = me->GetOwner()->getAttackerForHelper())
                            AttackStart(victim);

                    return;
                }

                if (me->HasUnitState(UNIT_STATE_CASTING))
                    return;

                events.Update(diff);

                while (uint32 eventId = events.ExecuteEvent())
                {
                    switch (eventId)
                    {
                        case EVENT_LIGHTNING:
                        {
                            if (Unit* target = me->GetVictim())
                            {
                                me->CastSpell(target, CRACKLING_TIGER_LIGHTNING, false);
                            }
                            else if (me->GetOwner())
                            {
                                if (Unit* target = me->GetOwner()->getAttackerForHelper())
                                    me->CastSpell(target, CRACKLING_TIGER_LIGHTNING, false);
                            }

                            events.ScheduleEvent(EVENT_LIGHTNING, 6000);
                            break;
                        }
                        case EVENT_LEAP:
                        {
                            if (Unit* target = me->GetVictim())
                            {
                                me->CastSpell(target, TIGER_LEAP, false);
                            }
                            else if (me->GetOwner())
                            {
                                if (Unit* target = me->GetOwner()->getAttackerForHelper())
                                    me->CastSpell(target, TIGER_LEAP, false);
                            }

                            me->CastSpell(me, TIGER_LUST, false);
                            events.ScheduleEvent(EVENT_LEAP, 15000);

                            break;
                        }
                        case EVENT_PROVOKE:
                        {
                            Unit* m_owner = me->GetOwner();
                            if (!m_owner || m_owner->GetTypeId() != TYPEID_PLAYER)
                                break;

                            Player* p_owner = m_owner->ToPlayer();
                            if (p_owner->GetSpecializationId(p_owner->GetActiveSpec()) != CHAR_SPECIALIZATION_MONK_BREWMASTER)
                                break;

                            if (Unit* target = me->GetVictim())
                            {
                                if (target->GetTypeId() == TYPEID_UNIT)
                                    if (target->ToCreature()->IsDungeonBoss() || target->ToCreature()->isWorldBoss())
                                        break;

                                me->CastSpell(target, PROVOKE, false);
                            }
                            else if (me->GetOwner())
                            {
                                if (Unit* target = me->GetOwner()->getAttackerForHelper())
                                {
                                    if (target->GetTypeId() == TYPEID_UNIT)
                                        if (target->ToCreature()->IsDungeonBoss() || target->ToCreature()->isWorldBoss())
                                            break;

                                    me->CastSpell(target, PROVOKE, false);
                                }
                            }

                            events.ScheduleEvent(EVENT_PROVOKE, 15000);
                            break;
                        }
                        default:
                            break;
                    }
                }

                DoMeleeAttackIfReady();
            }
        };

        CreatureAI* GetAI(Creature* creature) const
        {
            return new npc_pet_monk_xuen_the_white_tigerAI(creature);
        }
};

enum eSpiritEntries
{
    NPC_STORM_SPIRIT    = 69680,
    NPC_EARTH_SPIRIT    = 69792,
    NPC_FIRE_SPIRIT     = 69791
};

enum eSpiritEquips
{
    EQUIP_STORM_TWO_HANDS   = 25197,
    EQUIP_EARTH_STAFF       = 86218,
    EQUIP_FIRE_TWO_HANDS    = 82224
};

enum eSpiritActions
{
    ACTION_DESPAWN_SPIRIT
};

enum eSpiritMoves
{
    MOVE_DESPAWN    = 1
};

const uint32 visualMorph[3] = { 138080, 138083, 138081 }; // Storm, Earth and Fire

class npc_pet_monk_spirit : public CreatureScript
{
    public:
        npc_pet_monk_spirit() : CreatureScript("npc_pet_monk_spirit") { }

        struct npc_pet_monk_spiritAI : public ScriptedAI
        {
            npc_pet_monk_spiritAI(Creature* creature) : ScriptedAI(creature)
            {
                targetGuid = 0;
            }

            uint64 targetGuid;

            void Reset()
            {
                Unit* owner = me->GetOwner();
                if (!owner)
                    return;

                owner->CastSpell(me, 45204, true); // Clone Me!

                switch (me->GetEntry())
                {
                    case NPC_STORM_SPIRIT:
                        me->CastSpell(me, visualMorph[0], true);
                        SetEquipmentSlots(false, EQUIP_STORM_TWO_HANDS, EQUIP_STORM_TWO_HANDS, EQUIP_NO_CHANGE);
                        break;
                    case NPC_EARTH_SPIRIT:
                        me->CastSpell(me, visualMorph[1], true);
                        SetEquipmentSlots(false, EQUIP_EARTH_STAFF, EQUIP_NO_CHANGE, EQUIP_NO_CHANGE);
                        break;
                    case NPC_FIRE_SPIRIT:
                        me->CastSpell(me, visualMorph[2], true);
                        SetEquipmentSlots(false, EQUIP_FIRE_TWO_HANDS, EQUIP_FIRE_TWO_HANDS, EQUIP_NO_CHANGE);
                        break;
                }
            }

            void SetGUID(uint64 guid, int32 data /*= 0*/)
            {
                targetGuid = guid;

                if (Unit* victim = Unit::GetUnit(*me, targetGuid))
                {
                    me->CastSpell(victim, 138104, true);    // Jump
                    me->CastSpell(me, 138130, true);
                    AttackStart(victim);
                }
            }

            virtual uint64 GetGUID(int32 data /*= 0*/)
            {
                return targetGuid;
            }

            void IsSummonedBy(Unit* summoner)
            {
                if (!summoner || summoner->GetTypeId() != TYPEID_PLAYER)
                    return;

                summoner->CastSpell(me, 119051, true);
            }

            void DoAction(int32 const action)
            {
                switch (action)
                {
                    case ACTION_DESPAWN_SPIRIT:
                    {
                        if (Unit* owner = me->GetOwner())
                        {
                            AuraPtr spirit = owner->GetAura(137639);

                            me->GetMotionMaster()->Clear();
                            me->GetMotionMaster()->MoveJump(owner->GetPositionX(), owner->GetPositionY(), owner->GetPositionZ(), 15.0f, 10.0f, me->GetOrientation(), MOVE_DESPAWN);
                            me->DespawnOrUnsummon();
                            owner->RemoveAuraFromStack(1);
                        }
                        break;
                    }
                    default:
                        break;
                }
            }

            void KilledUnit(Unit* victim)
            {
                if (victim->GetGUID() == targetGuid)
                    DoAction(ACTION_DESPAWN_SPIRIT);
            }

            void MovementInform(uint32 type, uint32 id)
            {
                if (id == MOVE_DESPAWN)
                    me->DespawnOrUnsummon(500);
            }

            void UpdateAI(uint32 diff)
            {
                if (targetGuid)
                {
                    if (me->GetVictim() && me->GetVictim()->GetGUID() != targetGuid)
                        DoAction(0);
                    else if (!me->GetVictim())
                        DoAction(0);
                }

                DoMeleeAttackIfReady();
            }
        };

        CreatureAI* GetAI(Creature* creature) const
        {
            return new npc_pet_monk_spiritAI(creature);
        }
};

/*######
## npc_pet_monk_transcendence -- 54569
######*/

enum TranscendenceSpiritSpells
{
    SPELL_CLONE_CASTER          = 119051,
    SPELL_CLONE_WEAPONS_1       = 41054,
    SPELL_CLONE_WEAPONS_2       = 45205,
    SPELL_VISUAL_SPIRIT         = 119053,
    SPELL_MEDITATE              = 124416,
    SPELL_ROOT_FOR_EVER         = 31366
};

enum transcendenceActions
{
    ACTION_TELEPORT     = 1
};

class npc_pet_monk_transcendence : public CreatureScript
{
    public:
        npc_pet_monk_transcendence() : CreatureScript("npc_pet_monk_transcendence") { }

        struct npc_pet_monk_transcendenceAI : public ScriptedAI
        {
            npc_pet_monk_transcendenceAI(Creature* c) : ScriptedAI(c)
            {
                me->ApplySpellImmune(0, IMMUNITY_MECHANIC, MECHANIC_GRIP, true);
                me->ApplySpellImmune(0, IMMUNITY_MECHANIC, MECHANIC_STUN, true);
                me->ApplySpellImmune(0, IMMUNITY_MECHANIC, MECHANIC_FEAR, true);
                me->ApplySpellImmune(0, IMMUNITY_MECHANIC, MECHANIC_ROOT, true);
                me->ApplySpellImmune(0, IMMUNITY_MECHANIC, MECHANIC_FREEZE, true);
                me->ApplySpellImmune(0, IMMUNITY_MECHANIC, MECHANIC_POLYMORPH, true);
                me->ApplySpellImmune(0, IMMUNITY_MECHANIC, MECHANIC_HORROR, true);
                me->ApplySpellImmune(0, IMMUNITY_MECHANIC, MECHANIC_SAPPED, true);
                me->ApplySpellImmune(0, IMMUNITY_MECHANIC, MECHANIC_CHARM, true);
                me->ApplySpellImmune(0, IMMUNITY_MECHANIC, MECHANIC_DISORIENTED, true);
                me->ApplySpellImmune(0, IMMUNITY_STATE, SPELL_AURA_MOD_CONFUSE, true);
                me->ApplySpellImmune(0, IMMUNITY_STATE, SPELL_AURA_MOD_TAUNT, true);
                me->ApplySpellImmune(0, IMMUNITY_EFFECT, SPELL_EFFECT_ATTACK_ME, true);
                me->ApplySpellImmune(0, IMMUNITY_EFFECT, SPELL_EFFECT_KNOCK_BACK, true);
                me->SetReactState(REACT_PASSIVE);
            }

            void IsSummonedBy(Unit* /*owner*/)
            {
                Unit* owner = me->GetOwner();
                if (!owner)
                    return;

                if (owner && owner->GetTypeId() == TYPEID_PLAYER)
                {
                    me->SetMaxHealth(owner->ToPlayer()->GetMaxHealth());
                    me->SetHealth(owner->ToPlayer()->GetHealth());

                    me->CastSpell(me, SPELL_VISUAL_SPIRIT, true);
                    owner->CastSpell(me, SPELL_CLONE_CASTER, true);
                    me->AddAura(SPELL_ROOT_FOR_EVER, me);
                }
                else
                    me->DespawnOrUnsummon();
            }

            void MovementInform(uint32 type, uint32 id)
            {
                if (type != POINT_MOTION_TYPE)
                    return;

                if (id == 0)
                    me->SetSpeed(MOVE_RUN, 0.0f);
            }

            void DoAction(int32 const action)
            {
                switch (action)
                {
                    case ACTION_TELEPORT:
                        if (!me->GetOwner())
                        {
                            me->DespawnOrUnsummon(600);
                            break;
                        }

                        me->SetSpeed(MOVE_RUN, 10.0f);
                        me->GetMotionMaster()->MovePoint(0, me->GetOwner()->GetPositionX(), me->GetOwner()->GetPositionY(), me->GetOwner()->GetPositionZ());
                        me->SetOrientation(me->GetOwner()->GetOrientation());
                        break;
                    default:
                        break;
                }
            }
        };

        CreatureAI* GetAI(Creature *creature) const
        {
            return new npc_pet_monk_transcendenceAI(creature);
        }
};

void AddSC_monk_pet_scripts()
{
    new npc_pet_monk_xuen_the_white_tiger();
    new npc_pet_monk_spirit();
    new npc_pet_monk_transcendence();
}