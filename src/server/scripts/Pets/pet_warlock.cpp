/*
 * Copyright (C) 2008-2016 TrinityCore <http://www.trinitycore.org/>
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 3 of the License, or (at your
 * option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program. If not, see <http://www.gnu.org/licenses/>.
 */

/*
 * Ordered alphabetically using scriptname.
 * Scriptnames of files in this file should be prefixed with "npc_pet_sha_".
 */

#include "Pet.h"
#include "ScriptMgr.h"
#include "SpellAuras.h"
#include "ScriptedCreature.h"
#include "ScriptedEscortAI.h"
#include "Player.h"
#include "Group.h"

enum ShamanSpells
{
    SPELL_WARLOCK_FIREBOLT              = 104318,
    SPELL_WARLOCK_MOLTEN_CORE_AURA      = 122351,
    SPELL_WARLOCK_MOLTEN_CORE           = 122355,
    SPELL_PET_DOOM_BOLT                 = 85692
};

class npc_wild_imp : public CreatureScript
{
public:
    npc_wild_imp() : CreatureScript("npc_wild_imp") { }

    struct npc_wild_impAI : public ScriptedAI
    {
        uint32 charges;

        npc_wild_impAI(Creature* creature) : ScriptedAI(creature)
        {
            charges = 10;
            me->SetReactState(REACT_ASSIST);
        }

        void Reset()
        {
            me->SetReactState(REACT_ASSIST);

            if (me->GetOwner())
                if (me->GetOwner()->GetVictim())
                    AttackStart(me->GetOwner()->GetVictim());
        }

        void UpdateAI(uint32 diff)
        {
            if (me->GetReactState() != REACT_ASSIST)
                me->SetReactState(REACT_ASSIST);

            if (!me->GetOwner())
                return;

            if (!me->GetOwner()->ToPlayer())
                return;

            if (me->HasUnitState(UNIT_STATE_CASTING))
                return;

            if (!charges)
            {
                me->DespawnOrUnsummon();
                return;
            }

            if ((me->GetVictim() || me->GetOwner()->getAttackerForHelper()))
            {
                me->CastSpell(me->GetVictim() ? me->GetVictim() : me->GetOwner()->getAttackerForHelper(), SPELL_WARLOCK_FIREBOLT, false);
                me->GetOwner()->EnergizeBySpell(me->GetOwner(), SPELL_WARLOCK_FIREBOLT, 5, POWER_DEMONIC_FURY);
                charges--;

                if (me->GetOwner()->HasAura(SPELL_WARLOCK_MOLTEN_CORE_AURA))
                    if (roll_chance_i(8))
                        me->GetOwner()->CastSpell(me->GetOwner(), SPELL_WARLOCK_MOLTEN_CORE, true);
            }
            else if (Pet* pet = me->GetOwner()->ToPlayer()->GetPet())
            {
                if (pet->getAttackerForHelper())
                {
                    me->CastSpell(me->GetVictim() ? me->GetVictim() : pet->getAttackerForHelper(), SPELL_WARLOCK_FIREBOLT, false);
                    me->GetOwner()->EnergizeBySpell(me->GetOwner(), SPELL_WARLOCK_FIREBOLT, 5, POWER_DEMONIC_FURY);
                    charges--;

                    if (me->GetOwner()->HasAura(SPELL_WARLOCK_MOLTEN_CORE_AURA))
                        if (roll_chance_i(8))
                            me->GetOwner()->CastSpell(me->GetOwner(), SPELL_WARLOCK_MOLTEN_CORE, true);
                }
            }
        }
    };

    CreatureAI* GetAI(Creature* creature) const
    {
        return new npc_wild_impAI(creature);
    }
};

const int32 greenAuras[6] = { 113930, 113903, 113911, 113912, 113913, 113914 };
const int32 purpleAuras[6] = { 113931, 113915, 113916, 113917, 113918, 113919 };

/*######
# npc_demonic_gateway_purple - 59271
######*/

class npc_demonic_gateway_purple : public CreatureScript
{
    public:
        npc_demonic_gateway_purple() : CreatureScript("npc_demonic_gateway_purple") { }

        struct npc_demonic_gateway_purpleAI : public ScriptedAI
        {
            npc_demonic_gateway_purpleAI(Creature* creature) : ScriptedAI(creature) { }

            void Reset()
            {
                me->CastSpell(me, 113901, true); // Periodic add charge
                me->CastSpell(me, 113900, true); // Portal Visual
                me->CastSpell(me, 113931, true); // 0 Purple Charge
                me->SetFlag(UNIT_FIELD_INTERACT_SPELL_ID, 113902);
                me->SetFlag(UNIT_FIELD_NPC_FLAGS, UNIT_NPC_FLAG_SPELLCLICK);
                me->setFaction(35);
            }

            void OnSpellClick(Unit* clicker)
            {
                if (clicker->GetTypeId() != TYPEID_PLAYER)
                    return;

                // Demonic Gateway cooldown marker
                if (clicker->HasAura(113942))
                    return;

                Unit* owner = me->GetOwner();
                if (!owner || !owner->ToPlayer())
                    return;

                if (Group* group = clicker->ToPlayer()->GetGroup())
                {
                    if (group != owner->ToPlayer()->GetGroup())
                        return;
                }
                else if (owner != clicker)
                    return;

                AuraEffectPtr charges = me->GetAuraEffect(113901, EFFECT_0);
                if (!charges)
                    return;

                if (charges->GetAmount() == 0)
                    return;

                std::list<Creature*> greenGates;
                me->GetCreatureListWithEntryInGrid(greenGates, 59262, 75.0f);

                if (greenGates.empty())
                    return;

                greenGates.sort(Trinity::DistanceCompareOrderPred(me));
                for (auto itr : greenGates)
                {
                    clicker->CastSpell(clicker, 113942, true);

                    // Init dest coordinates
                    float x, y, z;
                    itr->GetPosition(x, y, z);

                    float speedXY;
                    float speedZ = 5;

                    speedXY = clicker->GetExactDist2d(x, y) * 10.0f / speedZ;
                    clicker->GetMotionMaster()->MoveJump(x, y, z, speedXY, speedZ);
                    break;
                }

                charges->SetAmount(charges->GetAmount() - 1);

                for (int i = 0; i < 6; ++i)
                {
                    if (i <= charges->GetAmount())
                        me->CastSpell(me, purpleAuras[i], true);
                    else
                        me->RemoveAura(purpleAuras[i]);
                }
            }
        };

        CreatureAI* GetAI(Creature* creature) const
        {
            return new npc_demonic_gateway_purpleAI(creature);
        }
};

/*######
# new npc_demonic_gateway_green - 59262
######*/

class npc_demonic_gateway_green : public CreatureScript
{
    public:
        npc_demonic_gateway_green() : CreatureScript("npc_demonic_gateway_green") { }

        struct npc_demonic_gateway_greenAI : public ScriptedAI
        {
            npc_demonic_gateway_greenAI(Creature* creature) : ScriptedAI(creature) { }

            void Reset()
            {
                me->CastSpell(me, 113901, true); // Periodic add charges
                me->CastSpell(me, 113900, true); // Portal Visual
                me->SetFlag(UNIT_FIELD_INTERACT_SPELL_ID, 113902);
                me->SetFlag(UNIT_FIELD_NPC_FLAGS, UNIT_NPC_FLAG_SPELLCLICK);
                me->setFaction(35);
            }

            void OnSpellClick(Unit* clicker)
            {
                if (clicker->GetTypeId() != TYPEID_PLAYER)
                    return;

                // Demonic Gateway cooldown marker
                if (clicker->HasAura(113942))
                    return;

                Unit* owner = me->GetOwner();
                if (!owner || !owner->ToPlayer())
                    return;

                if (Group* group = clicker->ToPlayer()->GetGroup())
                {
                    if (group != owner->ToPlayer()->GetGroup())
                        return;
                }
                else if (owner != clicker)
                    return;

                AuraEffectPtr charges = me->GetAuraEffect(113901, EFFECT_0);
                if (!charges)
                    return;

                if (charges->GetAmount() == 0)
                    return;

                std::list<Creature*> purpleGates;
                me->GetCreatureListWithEntryInGrid(purpleGates, 59271, 75.0f);

                if (purpleGates.empty())
                    return;

                purpleGates.sort(Trinity::DistanceCompareOrderPred(me));
                for (auto itr : purpleGates)
                {
                    clicker->CastSpell(clicker, 113942, true);

                    // Init dest coordinates
                    float x, y, z;
                    itr->GetPosition(x, y, z);

                    float speedXY;
                    float speedZ = 5;

                    speedXY = clicker->GetExactDist2d(x, y) * 10.0f / speedZ;
                    clicker->GetMotionMaster()->MoveJump(x, y, z, speedXY, speedZ);
                    break;
                }

                charges->SetAmount(charges->GetAmount() - 1);

                for (int i = 0; i < 6; ++i)
                {
                    if (i <= charges->GetAmount())
                        me->CastSpell(me, greenAuras[i], true);
                    else
                        me->RemoveAura(greenAuras[i]);
                }
            }
        };

        CreatureAI* GetAI(Creature* creature) const
        {
            return new npc_demonic_gateway_greenAI(creature);
        }
};

// Grimoire of Service
class npc_grimoire_minion : public CreatureScript
{
    public:
        npc_grimoire_minion() : CreatureScript("npc_grimoire_minion") { }

        struct npc_grimoire_minionAI : public ScriptedAI
        {
            npc_grimoire_minionAI(Creature* c) : ScriptedAI(c)
            {
                if (c->IsPet())
                    NeedScript = false;
                else
                    NeedScript = true;

                firstCast = false;
                fireBoltCastTime = 3000;
            }

            bool NeedScript, firstCast;
            uint32 fireBoltCastTime;

            void UpdateAI(uint32 diff)
            {
                if (!NeedScript || me->HasUnitState(UNIT_STATE_CASTING))
                    return;

                if (Unit* owner = me->GetOwner())
                {
                    if (!me->GetVictim())
                    {
                        Unit * target = NULL;
                        if (owner->GetTypeId() == TYPEID_PLAYER)
                            target = owner->ToPlayer()->GetSelectedUnit();
                        else
                            target = owner->GetVictim();

                        if (target)
                            me->AI()->AttackStart(target);
                        else
                            me->GetMotionMaster()->MoveFollow(owner, PET_FOLLOW_DIST, owner->GetFollowAngle());
                    }

                    me->SetReactState(REACT_ASSIST);

                    if (!firstCast && me->GetVictim())
                    {
                        switch (me->GetEntry())
                        {
                           case 416:
                               me->CastSpell(owner, 89808, true);
                               break;
                           case 417:
                               me->CastSpell(me->GetVictim(), 19647, true);
                               break;
                           case 1860:
                               me->CastSpell(me->GetVictim(), 17735, true);
                               break;
                           case 1863:
                               me->CastSpell(me->GetVictim(), 6358, true);
                               break;
                           case 17252:
                               me->CastSpell(me->GetVictim(), 89766, true);
                               break;
                        }
                        firstCast = true;
                    }

                    if (me->GetEntry() == 416 && me->GetVictim())
                    {
                        if (fireBoltCastTime <= diff)
                        {
                            fireBoltCastTime = 3000;
                            me->CastSpell(me->GetVictim(),3110,true);
                        }
                        else fireBoltCastTime -= diff;
                    }
                }

                if (me->GetEntry() != 416)
                    DoMeleeAttackIfReady();
            }
        };

        CreatureAI* GetAI(Creature *creature) const
        {
            return new npc_grimoire_minionAI(creature);
        }
};

#define SPELL_PET_DOOM_BOLT 85692

class npc_terror_and_doom_guard : public CreatureScript
{
public:
    npc_terror_and_doom_guard() : CreatureScript("npc_terror_and_doom_guard") { }

    struct npc_terror_and_doom_guardAI : CasterAI
    {
        npc_terror_and_doom_guardAI(Creature* creature) : CasterAI(creature) {}

        uint32 spell_id;

        void InitializeAI()
        {
            spell_id = SPELL_PET_DOOM_BOLT;

            CasterAI::InitializeAI();
            Unit* owner = me->GetOwner();
            if (!owner)
                return;
        }

        void UpdateAI(uint32 diff)
        {
            if (!UpdateVictim() || me->HasUnitState(UNIT_STATE_CASTING))
                return;

            DoCastVictim(spell_id);
        }

        void EnterEvadeMode()
        {
            if (me->IsInEvadeMode() || !me->IsAlive())
                return;

            Unit* owner = me->GetCharmerOrOwner();

            me->CombatStop(true);
            if (owner && !me->HasUnitState(UNIT_STATE_FOLLOW))
            {
                me->GetMotionMaster()->Clear(false);
                me->GetMotionMaster()->MoveFollow(owner, PET_FOLLOW_DIST, me->GetFollowAngle(), MOTION_SLOT_ACTIVE);
            }
        }
    };

    CreatureAI* GetAI(Creature* creature) const
    {
        return new npc_terror_and_doom_guardAI(creature);
    }
};

void AddSC_warlock_pet_scripts()
{
    new npc_wild_imp();
    new npc_demonic_gateway_green();
    new npc_demonic_gateway_purple();
    new npc_grimoire_minion();
    new npc_terror_and_doom_guard();
}
