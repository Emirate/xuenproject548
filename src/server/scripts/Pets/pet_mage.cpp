/*
 * Copyright (C) 2008-2016 TrinityCore <http://www.trinitycore.org/>
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 3 of the License, or (at your
 * option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program. If not, see <http://www.gnu.org/licenses/>.
 */

/*
 * Ordered alphabetically using scriptname.
 * Scriptnames of files in this file should be prefixed with "npc_pet_mag_".
 */

#include "ScriptMgr.h"
#include "ScriptedCreature.h"
#include "CombatAI.h"
#include "Pet.h"
#include "PetAI.h"
#include "Cell.h"
#include "CellImpl.h"
#include "GridNotifiers.h"
#include "GridNotifiersImpl.h"

#define SPELL_ARCANE_SPEC 78832
#define SPELL_FIRE_SPEC 99062
#define SPELL_FROST_SPEC 59638

enum MageSpells
{
    SPELL_MAGE_CLONE_ME                 = 45204,
    SPELL_CLONE_WEAPONS_1               = 41054,
    SPELL_CLONE_WEAPONS_2               = 45205,
    SPELL_MAGE_MASTERS_THREAT_LIST      = 58838
};

/*######
## npc_pet_mage_mirror_image
######*/

class npc_pet_mage_mirror_image : public CreatureScript
{
public:
    npc_pet_mage_mirror_image() : CreatureScript("npc_pet_mage_mirror_image") { }

    struct npc_pet_mage_mirror_imageAI : CasterAI
    {
        npc_pet_mage_mirror_imageAI(Creature* creature) : CasterAI(creature) {}

        uint32 damagespellid;
        uint32 istantspellid;
        uint32 checkTimer;
        uint32 blastcd;
        float followdist;

        void InitializeAI()
        {
            CasterAI::InitializeAI();
            Unit* owner = me->GetOwner();
            Player* caster = me->GetOwner()->ToPlayer();

            int32 mana;
            mana = owner->GetPower(POWER_MANA);
            me->SetPower(POWER_MANA, mana);
            me->SetHealth(owner->GetMaxHealth());

            if (!owner)
                return;

            owner->CastSpell((Unit*)NULL, SPELL_MAGE_MASTERS_THREAT_LIST, true); // Inherit Master's Threat List (Not yet implemented)
            owner->CastSpell(me, SPELL_MAGE_CLONE_ME, true);                     // Clone Me!
            owner->CastSpell(me, SPELL_CLONE_WEAPONS_1, true);                   // Clone Main-Hand
            owner->CastSpell(me, SPELL_CLONE_WEAPONS_2, true);                   // Clone Off-Hand

            damagespellid = 59638;
            istantspellid = 59637;
            blastcd = 0;
            followdist = PET_FOLLOW_DIST * 2;

            // Check on Glyph of Mirror Image and spec
            owner = me->ToTempSummon()->GetSummoner();
                if (caster->GetSpecializationId(caster->GetActiveSpec()) == CHAR_SPECIALIZATION_MAGE_ARCANE)
                    damagespellid = 88084;
                else if (caster->GetSpecializationId(caster->GetActiveSpec()) == CHAR_SPECIALIZATION_MAGE_FIRE)
                    damagespellid = 88082;
            checkTimer = 250;
        }

        void UpdateAI(uint32 diff)
        {
            if (checkTimer <= diff)
            {
                if (!me->GetOwner() || !me->GetOwner()->ToPlayer())
                    return;

                Player* owner = me->GetOwner()->ToPlayer();
                // Actions by Mirror Images
                if(owner->IsInCombat())
                {
                    if (Unit* target = owner->GetSelectedUnit())
                    {
                        if (target->HasAuraTypeWithFamilyFlags(SPELL_AURA_MOD_CONFUSE, SPELLFAMILY_MAGE, 0x01000000))
                        {
                            AuraPtr poly = NULL;
                            if (target->HasAura(118))            // Polymorph: Sheep
                                poly = target->GetAura(118);
                            else if (target->HasAura(28272))     // Polymorph: Pig
                                poly = target->GetAura(28272);
                            else if (target->HasAura(28271))     // Polymorph: Turtle
                                poly = target->GetAura(28271);
                            else if (target->HasAura(61305))     // Polymorph: Black cat
                                poly = target->GetAura(61305);
                            else if (target->HasAura(61721))     // Polymorph: Rabbit
                                poly = target->GetAura(61721);
                            else if (target->HasAura(61780))     // Polymorph: Turkey
                                poly = target->GetAura(61780);

                            if (poly != NULL && poly->GetCasterGUID() == owner->GetGUID()){
                                // Will get back to the caster if his/her target is polymorphed
                                me->GetMotionMaster()->Clear(false);
                                me->GetMotionMaster()->MoveFollow(owner, followdist, me->GetFollowAngle(), MOTION_SLOT_ACTIVE);
                                checkTimer = 200;
                            }
                        }
                        else
                        {
                            if (owner->isMoving())
                            {
                                if (blastcd <= diff)
                                {
                                    me->CastSpell(target, istantspellid);
                                    blastcd = 6000;
                                }
                                me->GetMotionMaster()->Clear(false);
                                me->GetMotionMaster()->MoveFollow(owner, followdist, me->GetFollowAngle(), MOTION_SLOT_ACTIVE);
                                if (blastcd == 6000)
                                    checkTimer = 1500;
                                else
                                    checkTimer = 200;
                            }
                            else 
                            {
                                if (blastcd <= diff && damagespellid == 59638 && roll_chance_i(30))
                                {
                                    me->CastSpell(target, istantspellid);
                                    blastcd = 6000;
                                    checkTimer = 1500;
                                }
                                else
                                {
                                    me->CastSpell(target, damagespellid);
                                    checkTimer = 2500;
                                }
                            }
                        }
                    }
                }
                else
                {
                    me->GetMotionMaster()->Clear(false);
                    me->GetMotionMaster()->MoveFollow(owner, followdist, me->GetFollowAngle(), MOTION_SLOT_ACTIVE);
                    checkTimer = 200;
                }
            }
            else
                checkTimer -= diff;
                if (blastcd >= diff)
                    blastcd -= diff;
        }

        // Do not reload Creature templates on evade mode enter - prevent visual lost
        void EnterEvadeMode()
        {
            if (me->IsInEvadeMode() || !me->IsAlive())
                return;

            Unit* owner = me->GetCharmerOrOwner();

            me->CombatStop(true);
            if (owner && !me->HasUnitState(UNIT_STATE_FOLLOW))
            {
                me->GetMotionMaster()->Clear(false);
                me->GetMotionMaster()->MoveFollow(owner, followdist, me->GetFollowAngle(), MOTION_SLOT_ACTIVE);
            }
        }
    };

    CreatureAI* GetAI(Creature* creature) const
    {
        return new npc_pet_mage_mirror_imageAI(creature);
    }
};

/*######
## npc_pet_mage_ring_of_frost
######*/

class npc_pet_mage_ring_of_frost : public CreatureScript
{
    public:
        npc_pet_mage_ring_of_frost() : CreatureScript("npc_pet_mage_ring_of_frost") { }

        struct npc_pet_mage_ring_of_frostAI : public Scripted_NoMovementAI
        {
            npc_pet_mage_ring_of_frostAI(Creature *c) : Scripted_NoMovementAI(c)
            {
                Reset();
            }

            void Reset()
            {
                me->SetReactState(REACT_PASSIVE);
                me->SetFlag(UNIT_FIELD_FLAGS, UNIT_FLAG_NON_ATTACKABLE | UNIT_FLAG_NOT_SELECTABLE | UNIT_FLAG_DISABLE_MOVE);
            }
        };

        CreatureAI* GetAI(Creature* pCreature) const
        {
            return new npc_pet_mage_ring_of_frostAI(pCreature);
        }
};

/*######
# npc_pet_mage_frozen_orb
######

enum frozenOrbSpells
{
    SPELL_MAGE_FROZEN_ORB_SELF_SNARE    = 82736,
    SPELL_MAGE_FROZEN_ORB_VISUAL        = 123605,
    SPELL_MAGE_FROZEN_ORB_PERIODIC_AURA = 84717,
    SPELL_MAGE_FROZEN_ORB_DMG           = 84721,
    SPELL_MAGE_FROZEN_ORB_VISUAL_DMG    = 113162,
    SPELL_FINGERS_OF_FROST_VISUAL       = 44544,
    SPELL_FINGERS_OF_FROST              = 126084
};

class npc_pet_mage_frozen_orb : public CreatureScript
{
    public:
        npc_pet_mage_frozen_orb() : CreatureScript("npc_pet_mage_frozen_orb") { }

        struct npc_pet_mage_frozen_orbAI : public ScriptedAI
        {
            npc_pet_mage_frozen_orbAI(Creature* creature) : ScriptedAI(creature)
            {
                frozenOrbTimer = 0;
            }

            uint32 frozenOrbTimer;
            bool CombatCheck;
            float angle;

            void IsSummonedBy(Unit* owner)
            {
                if (owner && owner->GetTypeId() == TYPEID_PLAYER)    
                {
                    angle = me->GetOwner()->GetOrientation();
                    me->SetReactState(REACT_PASSIVE);
                    me->SetSpeed(MOVE_RUN, 1.0f);
                    me->SetSpeed(MOVE_WALK, 1.0f);
                    me->SetWalk(true);

                    frozenOrbTimer = 2000;
                }
                else
                    me->DespawnOrUnsummon();
            }

            void EnterCombat(Unit*)
            {
                if (me->IsInCombat())
                {
                    me->GetOwner()->CastSpell(me->GetOwner(), SPELL_FINGERS_OF_FROST_VISUAL, true);
                    me->GetOwner()->CastSpell(me->GetOwner(), SPELL_FINGERS_OF_FROST, true);
                    CombatCheck = true;
                }
            }

            void UpdateAI(uint32 diff)
            {
                Unit* owner = me->GetOwner();

                if (!owner)
                    return;

                me->CastSpell(me, SPELL_MAGE_FROZEN_ORB_VISUAL, true);
                me->CastSpell(me, SPELL_MAGE_FROZEN_ORB_PERIODIC_AURA, true);

                if (me->IsInCombat())
                    me->CastSpell(me, SPELL_MAGE_FROZEN_ORB_SELF_SNARE, true);

                if (frozenOrbTimer <= diff)
                {
                    if (owner && owner->ToPlayer())
                        if (owner->ToPlayer()->HasSpellCooldown(SPELL_MAGE_FROZEN_ORB_DMG))
                            owner->ToPlayer()->RemoveSpellCooldown(SPELL_MAGE_FROZEN_ORB_DMG);

                    owner->CastSpell(me, SPELL_MAGE_FROZEN_ORB_DMG, true);

                    if (CombatCheck)
                        me->CastSpell(me, SPELL_MAGE_FROZEN_ORB_VISUAL_DMG, true);

                    frozenOrbTimer = 2000;
                }
                else 
                    frozenOrbTimer -= diff;

                me->GetMotionMaster()->Clear(false);
                me->GetMotionMaster()->MovePoint(0, me->GetPositionX() + 60 * cos(angle), me->GetPositionY() + 60 * sin(angle), me->GetPositionZ());
            }
        };

        CreatureAI* GetAI(Creature* creature) const
        {
            return new npc_pet_mage_frozen_orbAI(creature);
        }
};*/

/*######
# npc_pet_mage_frozen_orb
######

enum frozenOrbSpells
{
    SPELL_SELF_SNARE_90             = 82736,
    SPELL_SNARE_DAMAGE              = 84721,
    SPELL_FINGERS_OF_FROST          = 44544,
	SPELL_MAGE_FROZEN_ORB_VISUAL = 123605,
	SPELL_MAGE_FROZEN_ORB_VISUAL_DMG = 113162,
};

class npc_pet_mage_frozen_orb : public CreatureScript
{
    public:
        npc_pet_mage_frozen_orb() : CreatureScript("npc_pet_mage_frozen_orb") { }

        struct npc_pet_mage_frozen_orbAI : public ScriptedAI
        {
			float newx, newy, newz;
			bool engagedInCombat;
			bool activemovement;
			std::list<Unit*> targetList;

            npc_pet_mage_frozen_orbAI(Creature* creature) : ScriptedAI(creature)
            {
				newz = me->GetPositionZ() + 2.0f;
				float angle = me->GetOwner()->GetAngle(me);
				newx = me->GetPositionX() + 60 * cos(angle);
				newy = me->GetPositionY() + 60 * sin(angle);
				engagedInCombat = false;
				activemovement = false;
            }

            uint32 frozenOrbTimer;

            void Reset() override
            {
                    me->GetOwner()->CastSpell(me, SPELL_SNARE_DAMAGE, true);
                    me->AddAura(SPELL_SELF_SNARE_90, me);

					if (!activemovement)
						me->SetSpeed(MOVE_RUN, 1.0f, false);

                    frozenOrbTimer = 1000;
					me->SetFlag(UNIT_FIELD_FLAGS, UNIT_FLAG_NOT_SELECTABLE | UNIT_FLAG_NON_ATTACKABLE);
					me->SetReactState(REACT_PASSIVE);
					me->GetMotionMaster()->MovePoint(0, newx, newy, newz);
					me->AddUnitMovementFlag(MOVEMENTFLAG_SPLINE_ELEVATION);
					if (!targetList.empty())
						targetList.empty(); // clear the data inside the vector to prevent a possible issue
             }

            void UpdateAI(const uint32 diff) override
            {
                Unit* owner = me->GetOwner();

                if (!owner)
                    return;

				// We have to force the visaul as the client doesn't trigger the frozen orb visual

				me->CastSpell(me, SPELL_MAGE_FROZEN_ORB_VISUAL, true);

				Position pos;
				float angle = me->GetRelativeAngle(me);
				me->GetNearPosition(pos, me->GetObjectSize(), angle);
				float radius = 8.00f;

				Trinity::NearestAttackableUnitInObjectRangeCheck u_check(me, me, radius);
				Trinity::UnitListSearcher<Trinity::NearestAttackableUnitInObjectRangeCheck> searcher(me, targetList, u_check);

				me->VisitNearbyObject(radius, searcher);

				if (!targetList.empty())
				{
					targetList.sort(Trinity::ObjectDistanceOrderPred(me));
					targetList.resize(1);

					for (auto itr : targetList)

						if (!engagedInCombat)
						{
							owner->CastSpell(owner, SPELL_FINGERS_OF_FROST, true);
							me->SetSpeed(MOVE_RUN, 0.10f, false);
							me->CastSpell(me, SPELL_MAGE_FROZEN_ORB_VISUAL_DMG, true);
							engagedInCombat = true;
							activemovement = true;
						}
				}

                if (frozenOrbTimer <= diff)
                {
                    if (owner && owner->ToPlayer())
                        if (owner->ToPlayer()->HasSpellCooldown(SPELL_SNARE_DAMAGE))
                            owner->ToPlayer()->RemoveSpellCooldown(SPELL_SNARE_DAMAGE);

                    owner->CastSpell(me, SPELL_SNARE_DAMAGE, true);
                    frozenOrbTimer = 1000;
                }
                else
                    frozenOrbTimer -= diff;
            }
        };

        CreatureAI* GetAI(Creature* creature) const override
        {
			if (!creature->ToTempSummon())
				return NULL;
            return new npc_pet_mage_frozen_orbAI(creature);
        }
};*/

/*###########################
## Orbe de fuego BoT WoW ##
#############################*/

enum frozenOrbSpells
{
    SPELL_MAGE_FROZEN_ORB_VISUAL        = 123605,
    SPELL_MAGE_FROZEN_ORB_PERIODIC_AURA = 84717,
    SPELL_MAGE_FROZEN_ORB_VISUAL_DMG    = 113162,
    SPELL_MAGE_FROZEN_ORB_DMG           = 84721,
    SPELL_MAGE_FROZEN_ORB_SELF_SNARE    = 82736,
    SPELL_MAGE_FINGERS_OF_FROST         = 44544
};

class npc_pet_mage_frozen_orb : public CreatureScript
{
public:
    npc_pet_mage_frozen_orb() : CreatureScript("npc_pet_mage_frozen_orb") { }

    struct npc_pet_mage_frozen_orbAI : public ScriptedAI
    {
        npc_pet_mage_frozen_orbAI(Creature* creature) : ScriptedAI(creature)
        {
			summoner = me->ToTempSummon()->GetSummoner();
			summoner->GetPosition(&pos);
			me->NearTeleportTo(pos.GetPositionX(), pos.GetPositionY(), pos.GetPositionZ(), pos.GetOrientation(), true);

			pos.m_positionX = pos.GetPositionX() + (15 * cos(pos.GetOrientation()));
			pos.m_positionY = pos.GetPositionY() + (15 * sin(pos.GetOrientation()));
			DamageTimer = 1000;
			CombatCheck = false;
        }

		Position pos, temp;
		Unit* summoner;
		uint32 DamageTimer;
		bool CombatCheck;

        void EnterCombat(Unit* /*target*/) override
        {
			summoner->CastSpell(summoner, SPELL_MAGE_FINGERS_OF_FROST, true);
			me->CastSpell(me, SPELL_MAGE_FROZEN_ORB_VISUAL_DMG, true);

			CombatCheck = true;
        }

        void Reset() override
        {
			me->AddUnitMovementFlag(MOVEMENTFLAG_SPLINE_ELEVATION);
			me->CastSpell(me, SPELL_MAGE_FROZEN_ORB_VISUAL, true);
			me->CastSpell(me, SPELL_MAGE_FROZEN_ORB_PERIODIC_AURA, true);
			summoner->MovePositionToFirstCollision(pos, 200, 0.0f);
			me->GetMotionMaster()->MovePoint(0, pos.GetPositionX(), pos.GetPositionY(), pos.GetPositionZ(), false);
        }

        void MoveInLineOfSight(Unit* /*who*/) override { }

        void EnterEvadeMode() override { }

		void UpdateAI(uint32 diff) override
        {
			if (DamageTimer <= diff)
			{
				if (summoner)
					me->CastSpell(me, SPELL_MAGE_FROZEN_ORB_DMG, true, 0, 0, summoner->GetGUID());

				if (CombatCheck)
					me->CastSpell(me, SPELL_MAGE_FROZEN_ORB_VISUAL_DMG, true);

				DamageTimer = 1000;
			}
			else
				DamageTimer -= diff;

			if (!CombatCheck)
			{
				if (Unit* target = me->SelectNearestTarget(10))
					AttackStart(target);
			}
        }
    };

    CreatureAI* GetAI(Creature* creature) const override
    {
        if (!creature->ToTempSummon())
            return NULL;
        return new npc_pet_mage_frozen_orbAI(creature);
    }
};

/*######
## npc_past_self
######*/

enum PastSelfSpells
{
    SPELL_FADING                    = 107550,
    SPELL_ALTER_TIME                = 110909,
    SPELL_ENCHANTED_REFLECTION      = 102284,
    SPELL_ENCHANTED_REFLECTION_2    = 102288
};

struct auraData
{
    uint32 m_id;
    int32 m_duration;
    int32 m_maxDuration;
    uint8 m_stacks;
    int32 m_damage[MAX_SPELL_EFFECTS];
    int32 m_fixedAmount[MAX_SPELL_EFFECTS];
    uint8 charges;
};

#define ACTION_ALTER_TIME   1

class npc_past_self : public CreatureScript
{
    public:
        npc_past_self() : CreatureScript("npc_past_self") { }

        struct npc_past_selfAI : public ScriptedAI
        {
            npc_past_selfAI(Creature* c) : ScriptedAI(c)
            {
                me->SetFlag(UNIT_FIELD_FLAGS, UNIT_FLAG_NOT_SELECTABLE | UNIT_FLAG_NON_ATTACKABLE);
                me->SetReactState(REACT_PASSIVE);
                me->SetMaxHealth(500);
                me->SetHealth(me->GetMaxHealth());
                mana = 0;
                health = 0;
                auras.clear();
            }

            int32 mana;
            int32 health;
            std::list<auraData> auras;

            void Reset()
            {
                if (!me->HasAura(SPELL_FADING))
                    me->AddAura(SPELL_FADING, me);
            }

            void IsSummonedBy(Unit* owner)
            {
                if (owner && owner->GetTypeId() == TYPEID_PLAYER)
                {
                    Unit::AuraApplicationMap const& appliedAuras = owner->GetAppliedAuras();
                    for (Unit::AuraApplicationMap::const_iterator itr = appliedAuras.begin(); itr != appliedAuras.end(); ++itr)
                    {
                        if (AuraPtr aura = itr->second->GetBase())
                        {
                            SpellInfo const* auraInfo = aura->GetSpellInfo();
                            if (!auraInfo)
                                continue;

                            if (auraInfo->Id == SPELL_ALTER_TIME)
                                continue;

                            if (auraInfo->IsPassive())
                                continue;

                            auraData aData;
                            aData.charges = aura->GetCharges();
                            aData.m_stacks = aura->GetStackAmount();
                            aData.m_maxDuration = aura->GetMaxDuration();
                            aData.m_duration = aura->GetDuration();
                            aData.m_id = auraInfo->Id;

                            for (uint8 i = 0; i < auraInfo->Effects[i].BasePoints; ++i)
                            {
                                if (AuraEffectPtr effect = aura->GetEffect(i))
                                {
                                    aData.m_damage[i] = effect->GetAmount();
                                    aData.m_fixedAmount[i] = effect->GetFixedDamageInfo().GetFixedDamage();
                                }
                                else
                                {
                                    aData.m_damage[i] = 0;
                                    aData.m_fixedAmount[i] = 0;
                                }
                            }
   
                            auras.push_back(aData);
                        }
                    }

                    mana = owner->GetPower(POWER_MANA);
                    health = owner->GetHealth();

                    owner->AddAura(SPELL_ENCHANTED_REFLECTION, me);
                    owner->AddAura(SPELL_ENCHANTED_REFLECTION_2, me);
                }
                else
                    me->DespawnOrUnsummon();
            }

            void DoAction(int32 const action)
            {
                if (action == ACTION_ALTER_TIME)
                {
                    if (TempSummon* pastSelf = me->ToTempSummon())
                    {
                        if (Unit* m_owner = pastSelf->GetSummoner())
                        {
                            if (m_owner->ToPlayer())
                            {
                                if (!m_owner->IsAlive())
                                    return;

                                m_owner->RemoveNonPassivesAuras();

                                for (std::list<auraData>::iterator itr = auras.begin(); itr != auras.end(); ++itr)
                                {
                                    AuraPtr aura = !m_owner->HasAura((itr)->m_id) ? m_owner->AddAura((itr)->m_id, m_owner) : m_owner->GetAura((itr)->m_id);

                                    if (aura)
                                    {
                                        aura->SetStackAmount((itr)->m_stacks);
                                        aura->SetMaxDuration((itr)->m_maxDuration);
                                        aura->SetDuration((itr)->m_duration);
                                        aura->SetNeedClientUpdateForTargets();
                                        aura->SetCharges((itr)->charges);

                                        for (uint8 i = 0; i < aura->GetSpellInfo()->Effects[i].BasePoints; ++i)
                                            if (AuraEffectPtr effect = aura->GetEffect(i))
                                            {
                                                effect->ChangeAmount((itr)->m_damage[i]);

                                                if ((itr)->m_fixedAmount[i])
                                                    effect->GetFixedDamageInfo().SetFixedDamage((itr)->m_fixedAmount[i]);
                                            }
                                    }
                                }

                                auras.clear();

                                m_owner->SetPower(POWER_MANA, mana);
                                m_owner->SetHealth(health);

                                m_owner->NearTeleportTo(me->GetPositionX(), me->GetPositionY(), me->GetPositionZ(), me->GetOrientation(), true);
                                me->DespawnOrUnsummon(100);
                            }
                        }
                    }
                }
            }
        };

        CreatureAI* GetAI(Creature *creature) const
        {
            return new npc_past_selfAI(creature);
        }
};

void AddSC_mage_pet_scripts()
{
    new npc_pet_mage_mirror_image();
    new npc_pet_mage_ring_of_frost();
    new npc_pet_mage_frozen_orb();
    new npc_past_self();
}
