/*
 * Copyright (C) 2008-2016 TrinityCore <http://www.trinitycore.org/>
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 3 of the License, or (at your
 * option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program. If not, see <http://www.gnu.org/licenses/>.
 */

/*
 * Ordered alphabetically using scriptname.
 * Scriptnames of files in this file should be prefixed with "npc_pet_sha_".
 */

#include "ScriptMgr.h"
#include "ScriptedCreature.h"

enum ShamanSpells
{
    SPELL_SHAMAN_ANGEREDEARTH   = 36213,
    SPELL_SHAMAN_FIREBLAST      = 57984,
    SPELL_SHAMAN_FIRENOVA       = 12470,
    SPELL_SHAMAN_FIRESHIELD     = 13376
};

enum ShamanEvents
{
    // Earth Elemental
    EVENT_SHAMAN_ANGEREDEARTH   = 1,
    // Fire Elemental
    EVENT_SHAMAN_FIRENOVA       = 1,
    EVENT_SHAMAN_FIRESHIELD     = 2,
    EVENT_SHAMAN_FIREBLAST      = 3
};

class npc_pet_shaman_earth_elemental : public CreatureScript
{
    public:
        npc_pet_shaman_earth_elemental() : CreatureScript("npc_pet_shaman_earth_elemental") { }

        struct npc_pet_shaman_earth_elementalAI : public ScriptedAI
        {
            npc_pet_shaman_earth_elementalAI(Creature* creature) : ScriptedAI(creature) { }


            void Reset()
            {
                _events.Reset();
                _events.ScheduleEvent(EVENT_SHAMAN_ANGEREDEARTH, 0);
                me->ApplySpellImmune(0, IMMUNITY_SCHOOL, SPELL_SCHOOL_MASK_NATURE, true);
            }

            void UpdateAI(uint32 diff)
            {
                if (!UpdateVictim())
                    return;

                _events.Update(diff);

                if (_events.ExecuteEvent() == EVENT_SHAMAN_ANGEREDEARTH)
                {
                    DoCastVictim(SPELL_SHAMAN_ANGEREDEARTH);
                    _events.ScheduleEvent(EVENT_SHAMAN_ANGEREDEARTH, urand(5000, 20000));
                }

                DoMeleeAttackIfReady();
            }

        private:
            EventMap _events;
        };

        CreatureAI* GetAI(Creature* creature) const
        {
            return new npc_pet_shaman_earth_elementalAI(creature);
        }
};

class npc_pet_shaman_fire_elemental : public CreatureScript
{
    public:
        npc_pet_shaman_fire_elemental() : CreatureScript("npc_pet_shaman_fire_elemental") { }

        struct npc_pet_shaman_fire_elementalAI : public ScriptedAI
        {
            npc_pet_shaman_fire_elementalAI(Creature* creature) : ScriptedAI(creature) { }

            void Reset()
            {
                _events.Reset();
                _events.ScheduleEvent(EVENT_SHAMAN_FIRENOVA, urand(5000, 20000));
                _events.ScheduleEvent(EVENT_SHAMAN_FIREBLAST, urand(5000, 20000));
                _events.ScheduleEvent(EVENT_SHAMAN_FIRESHIELD, 0);
                me->ApplySpellImmune(0, IMMUNITY_SCHOOL, SPELL_SCHOOL_MASK_FIRE, true);
            }

            void UpdateAI(uint32 diff)
            {
                if (!UpdateVictim())
                    return;

                if (me->HasUnitState(UNIT_STATE_CASTING))
                    return;

                _events.Update(diff);

                while (uint32 eventId = _events.ExecuteEvent())
                {
                    switch (eventId)
                    {
                        case EVENT_SHAMAN_FIRENOVA:
                            DoCastVictim(SPELL_SHAMAN_FIRENOVA);
                            _events.ScheduleEvent(EVENT_SHAMAN_FIRENOVA, urand(5000, 20000));
                            break;
                        case EVENT_SHAMAN_FIRESHIELD:
                            DoCastVictim(SPELL_SHAMAN_FIRESHIELD);
                            _events.ScheduleEvent(EVENT_SHAMAN_FIRESHIELD, 2000);
                            break;
                        case EVENT_SHAMAN_FIREBLAST:
                            DoCastVictim(SPELL_SHAMAN_FIREBLAST);
                            _events.ScheduleEvent(EVENT_SHAMAN_FIREBLAST, urand(5000, 20000));
                            break;
                        default:
                            break;
                    }
                }

                DoMeleeAttackIfReady();
            }

        private:
            EventMap _events;
        };

        CreatureAI* GetAI(Creature* creature) const
        {
            return new npc_pet_shaman_fire_elementalAI(creature);
        }
};

class npc_pet_shaman_feral_spirit : public CreatureScript
{
    public:
        npc_pet_shaman_feral_spirit() : CreatureScript("npc_pet_shaman_feral_spirit") { }

        struct npc_pet_shaman_feral_spiritAI : public ScriptedAI
        {
            npc_pet_shaman_feral_spiritAI(Creature* creature) : ScriptedAI(creature) {}

            uint32 SpiritBiteTimer;

            void Reset()
            {
                SpiritBiteTimer = 6000;

                Unit* owner = me->GetOwner();

                // Glyph of Spirit Raptors
                if (owner->HasAura(147783))
                    me->CastSpell(me, 147908, true);
            }

            void UpdateAI(uint32 diff)
            {
                if (!UpdateVictim())
                    return;

                if (me->HasUnitState(UNIT_STATE_CASTING))
                    return;

                if (SpiritBiteTimer <= diff)
                {
                    me->CastSpell(me->GetVictim(), 58859, true);
                    SpiritBiteTimer = 6000;
                }
                else
                    SpiritBiteTimer -= diff;

                DoMeleeAttackIfReady();
            }
        };

        CreatureAI* GetAI(Creature* creature) const
        {
            return new npc_pet_shaman_feral_spiritAI(creature);
        }
};

/*######
## npc_healing_tide_totem
######*/

#define HEALING_TIDE     114941

class npc_healing_tide_totem : public CreatureScript
{
    public:
        npc_healing_tide_totem() : CreatureScript("npc_healing_tide_totem") { }

    struct npc_healing_tide_totemAI : public ScriptedAI
    {
        npc_healing_tide_totemAI(Creature* creature) : ScriptedAI(creature)
        {
            creature->CastSpell(creature, HEALING_TIDE, true);
        }

        void UpdateAI(uint32 diff)
        {
            if (!me->HasAura(HEALING_TIDE))
                me->CastSpell(me, HEALING_TIDE, true);
        }
    };

    CreatureAI* GetAI(Creature* creature) const
    {
        return new npc_healing_tide_totemAI(creature);
    }
};

/*######
## npc_stone_bulwark_totem
######*/

#define STONE_BULWARK_TOTEM_ABSORB      114889

class npc_stone_bulwark_totem : public CreatureScript
{
    public:
        npc_stone_bulwark_totem() : CreatureScript("npc_stone_bulwark_totem") { }

    struct npc_stone_bulwark_totemAI : public ScriptedAI
    {
        npc_stone_bulwark_totemAI(Creature* creature) : ScriptedAI(creature)
        {
            creature->CastSpell(creature, STONE_BULWARK_TOTEM_ABSORB, true);
        }

        void UpdateAI(uint32 diff)
        {
            if (!me->HasAura(STONE_BULWARK_TOTEM_ABSORB))
                me->CastSpell(me, STONE_BULWARK_TOTEM_ABSORB, true);
        }
    };

    CreatureAI* GetAI(Creature* creature) const
    {
        return new npc_stone_bulwark_totemAI(creature);
    }
};

/*######
## npc_earthgrab_totem
######*/

#define EARTHGRAB       116943

class npc_earthgrab_totem : public CreatureScript
{
    public:
        npc_earthgrab_totem() : CreatureScript("npc_earthgrab_totem") { }

    struct npc_earthgrab_totemAI : public ScriptedAI
    {
        npc_earthgrab_totemAI(Creature* creature) : ScriptedAI(creature)
        {
            creature->CastSpell(creature, EARTHGRAB, true);
        }

        void UpdateAI(uint32 diff)
        {
            if (!me->HasAura(EARTHGRAB))
                me->CastSpell(me, EARTHGRAB, true);
        }
    };

    CreatureAI* GetAI(Creature* creature) const
    {
        return new npc_earthgrab_totemAI(creature);
    }
};

/*######
## npc_windwalk_totem
######*/

#define WINDWALK     114896

class npc_windwalk_totem : public CreatureScript
{
    public:
        npc_windwalk_totem() : CreatureScript("npc_windwalk_totem") { }

    struct npc_windwalk_totemAI : public ScriptedAI
    {
        npc_windwalk_totemAI(Creature* creature) : ScriptedAI(creature)
        {
            creature->CastSpell(creature, WINDWALK, true);
        }

        void UpdateAI(uint32 diff)
        {
            if (!me->HasAura(WINDWALK))
                me->CastSpell(me, WINDWALK, true);
        }
    };

    CreatureAI* GetAI(Creature* creature) const
    {
        return new npc_windwalk_totemAI(creature);
    }
};

/*######
## npc_capacitor_totem
######*/

class npc_capacitor_totem : public CreatureScript
{
    public:
        npc_capacitor_totem() : CreatureScript("npc_capacitor_totem") { }

    struct npc_capacitor_totemAI : public ScriptedAI
    {
        uint32 CastTimer;

        npc_capacitor_totemAI(Creature* creature) : ScriptedAI(creature) { }

        void UpdateAI(uint32 diff)
        {
            if (me->HasUnitState(UNIT_STATE_CASTING))
                return;

            me->CastSpell(me, 118905, false);
        }
    };

    CreatureAI* GetAI(Creature* creature) const
    {
        return new npc_capacitor_totemAI(creature);
    }
};

/*######
## npc_spirit_link_totem
######*/

class npc_spirit_link_totem : public CreatureScript
{
    public:
        npc_spirit_link_totem() : CreatureScript("npc_spirit_link_totem") { }

    struct npc_spirit_link_totemAI : public ScriptedAI
    {
        uint32 CastTimer;

        npc_spirit_link_totemAI(Creature* creature) : ScriptedAI(creature)
        {
            CastTimer = 1000;

            if (creature->GetOwner() && creature->GetOwner()->GetTypeId() == TYPEID_PLAYER)
            {
                if (creature->GetEntry() == 53006)
                {
                    creature->CastSpell(creature, 98007, false);
                    creature->CastSpell(creature, 98017, true);
                }
            }
        }

        void UpdateAI(uint32 diff)
        {
            if (CastTimer >= diff)
            {
                if (me->GetOwner() && me->GetOwner()->GetTypeId() == TYPEID_PLAYER)
                {
                    if (me->GetEntry() == 53006)
                    {
                        me->CastSpell(me, 98007, false);
                        me->CastSpell(me, 98017, true);
                    }
                }
            }

            CastTimer = 0;
        }
    };

    CreatureAI* GetAI(Creature* creature) const
    {
        return new npc_spirit_link_totemAI(creature);
    }
};

void AddSC_shaman_pet_scripts()
{
    new npc_pet_shaman_earth_elemental();
    new npc_pet_shaman_fire_elemental();
    new npc_pet_shaman_feral_spirit();
    new npc_stone_bulwark_totem();
    new npc_earthgrab_totem();
    new npc_windwalk_totem();
    new npc_healing_tide_totem();
    new npc_capacitor_totem();
    new npc_spirit_link_totem();
}
