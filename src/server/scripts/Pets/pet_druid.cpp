/*
 * Copyright (C) 2008-2016 TrinityCore <http://www.trinitycore.org/>
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 3 of the License, or (at your
 * option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program. If not, see <http://www.gnu.org/licenses/>.
 */

/*
 * Ordered alphabetically using scriptname.
 * Scriptnames of files in this file should be prefixed with "npc_pet_druid_".
 */

#include "ScriptMgr.h"
#include "ScriptedCreature.h"
#include "CombatAI.h"
#include "Cell.h"
#include "CellImpl.h"
#include "GridNotifiers.h"
#include "GridNotifiersImpl.h"

/*class npc_pet_druid_force_of_nature : public CreatureScript
{
    public:
        npc_pet_druid_force_of_nature() : CreatureScript("npc_pet_druid_force_of_nature") { }

        struct npc_pet_druid_force_of_natureAI : public ScriptedAI
        {
            uint64 own;
            uint64 tar;
            bool init;
            bool rakeCast;

        npc_pet_druid_force_of_natureAI(Creature* pCreature) : ScriptedAI(pCreature)
        {
            init = false;
            own = 0;
            tar = 0;
            rakeCast = false;
        }

        void InitializeAI()
        {
            init = true;
            Unit* owner;

            if (TempSummon* temp = me->ToTempSummon())
                owner = temp->GetSummoner();

            if (owner == NULL)
                return;

            Player* player = owner->ToPlayer();

            if (player == NULL)
                return;

            own = player->GetGUID();

            if (Unit* target = player->GetSelectedUnit())
            {
                tar = target->GetGUID();
                CastStartSpell(owner, target);
            }
        }

        void CastStartSpell(Unit* o, Unit* t)
        {
            if (me->GetEntry() == PET_ENTRY_TREANT_RESTO)
            {
                me->CastSpell(t->IsFriendlyTo(o) ? t : o, 142423, true);
                return;
            }
            else if (t->IsFriendlyTo(o))
                return;

            switch (me->GetEntry())
            {
                case PET_ENTRY_TREANT_GUARDIAN:
                    me->CastSpell(t, 113830, true); // Taunt
                    break;
                case PET_ENTRY_TREANT_FERAL:
                case PET_ENTRY_TREANT_BALANCE:
                    me->CastSpell(t, 113770, true); // Root
                    // on creatures 30seconds
                    if (t->GetTypeId() !=TYPEID_PLAYER)
                        if (AuraPtr root = t->GetAura(113770, me->GetGUID()))
                            root->SetDuration(30000);
                    break;
                default:
                    break;
            }

            AttackStart(t);
        }

        void Reset() {}

        bool UpdateTarget()
        {
            if (!(me->GetVictim()) && me->GetEntry() != PET_ENTRY_TREANT_RESTO)
            {
                Player* owner = Unit::GetPlayer((*me), own);
                if (owner)
                {
                    if (Unit* target = owner->getAttackerForHelper())
                    {
                        tar = target->GetGUID();
                        me->SetTarget(tar);
                        me->SetInCombatWith(target);
                        AttackStart(target);
                    }
                    else
                        return false;
                }
                else
                {
                    own = 0;
                    return false;
                }
            }

            return true;
        }

        void UpdateAI(uint32 diff)
        {
            if (!init)
                InitializeAI();

            if (!own)
                return;

            if (me->IsNonMeleeSpellCasted(false))
                return;

            if (!UpdateTarget())
                return;

            switch (me->GetEntry())
            {         
                case PET_ENTRY_TREANT_RESTO:
                    me->CastSpell(me, 113828);
                    return;
                case PET_ENTRY_TREANT_BALANCE:
                    if (Unit* target = Unit::GetUnit((*me), tar))
                        me->CastSpell(target, 113769, false);
                    break;
                case PET_ENTRY_TREANT_FERAL:
                    // Special case
                    if (!rakeCast)
                    {
                        if (Unit* target = Unit::GetUnit((*me), tar))
                        {
                            if (me->IsWithinDistInMap(target, 5.0f, false))
                            {
                                rakeCast = true;
                                me->CastSpell(target, 150017, true);
                            }
                        }
                    }
                default:
                    break;
                }

                DoMeleeAttackIfReady();
            }
        };

        CreatureAI* GetAI(Creature* pCreature) const
        {
            return new npc_pet_druid_force_of_natureAI(pCreature);
        }
};*/

/*######
## npc_pet_druid_force_of_nature
######*/

class npc_pet_druid_force_of_nature : public CreatureScript
{
    public:
        npc_pet_druid_force_of_nature() : CreatureScript("npc_pet_druid_force_of_nature") { }

        struct npc_pet_druid_force_of_natureAI : public ScriptedAI
        {
            bool m_canCastRake;

            npc_pet_druid_force_of_natureAI(Creature* pCreature) : ScriptedAI(pCreature)
            {
                m_canCastRake = true;
            }

            void IsSummonedBy(Unit* p_Owner) override
            {
                if (!p_Owner && !p_Owner->GetTypeId() == TYPEID_PLAYER)
                    return;

                if (Unit* l_Target = p_Owner->ToPlayer()->GetSelectedUnit())
                    CastStartSpell(p_Owner, l_Target);
            }

            void CastStartSpell(Unit* p_Owner, Unit* p_Target)
            {
				if (me->GetEntry() == PET_ENTRY_TREANT_RESTO)
                {
                    me->CastSpell(p_Target->IsFriendlyTo(p_Owner) ? p_Target : p_Owner, 142423, true);
                    return;
                }
            
                if (p_Target->IsFriendlyTo(p_Owner))
                    return;

                switch (me->GetEntry())
                {
					case PET_ENTRY_TREANT_GUARDIAN:
						me->CastSpell(p_Target, 113830, true); // Taunt
                        break;

					case PET_ENTRY_TREANT_FERAL:
						m_canCastRake = true;
						me->CastSpell(p_Target, 113770, true); // Root
                        // on creatures 30seconds
						if (!p_Target->GetTypeId() == TYPEID_PLAYER)
                        {
                            if (AuraPtr root = p_Target->GetAura(113770, me->GetGUID()))
                                root->SetDuration(30000);
                        }
                        break;
					case PET_ENTRY_TREANT_BALANCE:
                        me->CastSpell(p_Target, 113770, true); // Root
                        // on creatures 30seconds
						if (!p_Target->GetTypeId() == TYPEID_PLAYER)
                        {
                            if (AuraPtr root = p_Target->GetAura(113770, me->GetGUID()))
                                root->SetDuration(30000);
                        }
                        break;

                    default:
                        break;
                }
            }

            bool UpdateTarget()
            {
				TempSummon* meTemp = me->ToTempSummon();

				if (!meTemp)
					return NULL;
				
				Unit* caster = meTemp->GetSummoner();
				
				if (!caster)
					return NULL;
				
                if (caster->ToPlayer()->GetSelectedUnit())
                    return true;

				if (me->GetEntry() != PET_ENTRY_TREANT_RESTO)
                {
                    /// Let's choose a new target
                    Unit* l_Target = caster->ToPlayer()->GetSelectedUnit();
                    if (!l_Target)
                    {
                        /// No target? Let's see if our owner has a better target for us
                        if (Unit* l_Owner = me->GetOwner())
                        {
                            Unit* l_OwnerVictim = caster->ToPlayer()->GetSelectedUnit();
                            if (l_OwnerVictim && me->CanCreatureAttack(l_OwnerVictim))
                                l_Target = l_OwnerVictim;
                        }
                    }

                    if (l_Target)
                    {
                        AttackStart(l_Target);
                        return true;
                    }
                }

                return false;
            }

            void UpdateAI(const uint32 diff)
            {
				TempSummon* meTemp = me->ToTempSummon();

				if (!meTemp)
					return;
				
				
				Unit* caster = meTemp->GetSummoner();
				
				if (!caster)
					return;
				
                if (me->IsNonMeleeSpellCasted(false))
                    return;

                if (!UpdateTarget())
                    return;

                switch (me->GetEntry())
                {         
				case PET_ENTRY_TREANT_RESTO:
                        me->CastSpell(me, 113828);
						if (Unit* target = caster->ToPlayer()->GetSelectedUnit())
							AttackStart(target);
                        return;

				case PET_ENTRY_TREANT_BALANCE:
                        if (Unit* target = caster->ToPlayer()->GetSelectedUnit())
                            me->CastSpell(target, 113769, false);
                        break;

				case PET_ENTRY_TREANT_FERAL:
                        // Special case
                        if (m_canCastRake)
                        {
                            if (Unit* target = caster->ToPlayer()->GetSelectedUnit())
                            {
                                if (me->IsWithinDistInMap(target, 5.0f, false))
                                {
                                    m_canCastRake = false;
                                    me->CastSpell(target, 1822, true);
									AttackStart(target);
                                }
                            }
                        }
                        break;

                    default:
                        break;
                }
				//AttackStart(target);
                DoMeleeAttackIfReady();
            }
        };

        CreatureAI* GetAI(Creature* pCreature) const
        {
            return new npc_pet_druid_force_of_natureAI(pCreature);
        }
};

/*######
# npc_wild_mushroom
######*/

#define WILD_MUSHROOM_INVISIBILITY   92661

class npc_wild_mushroom : public CreatureScript
{
    public:
        npc_wild_mushroom() : CreatureScript("npc_wild_mushroom") { }

        struct npc_wild_mushroomAI : public ScriptedAI
        {
            uint32 CastTimer;
            bool stealthed;

            npc_wild_mushroomAI(Creature *creature) : ScriptedAI(creature)
            {
                me->SetFlag(UNIT_FIELD_FLAGS, UNIT_FLAG_DISABLE_MOVE);
                CastTimer = 6000;
                stealthed = false;
                me->SetReactState(REACT_PASSIVE);
            }

            void UpdateAI(uint32 diff)
            {
                if (CastTimer <= diff && !stealthed)
                {
                    DoCast(me, WILD_MUSHROOM_INVISIBILITY, true);
                    stealthed = true;
                }
                else
                {
                    CastTimer -= diff;

                    if (!stealthed)
                        me->RemoveAura(WILD_MUSHROOM_INVISIBILITY);
                }
            }
        };

        CreatureAI* GetAI(Creature* creature) const
        {
            return new npc_wild_mushroomAI(creature);
        }
};

/*######
## npc_fungal_growth
######*/

#define FUNGAL_GROWTH_PERIODIC  81282
#define FUNGAL_GROWTH_AREA      94339

class npc_fungal_growth : public CreatureScript
{
    public:
        npc_fungal_growth() : CreatureScript("npc_fungal_growth") { }

        struct npc_fungal_growthAI : public ScriptedAI
        {
            npc_fungal_growthAI(Creature *c) : ScriptedAI(c)
            {
                me->SetReactState(REACT_PASSIVE);
                me->SetFlag(UNIT_FIELD_FLAGS, UNIT_FLAG_DISABLE_MOVE);
                me->SetFlag(UNIT_FIELD_FLAGS, UNIT_FLAG_NON_ATTACKABLE);
                me->SetFlag(UNIT_FIELD_FLAGS, UNIT_FLAG_NOT_SELECTABLE);
            }

            void Reset()
            {
                me->SetReactState(REACT_PASSIVE);
                me->SetFlag(UNIT_FIELD_FLAGS, UNIT_FLAG_DISABLE_MOVE);
                me->SetFlag(UNIT_FIELD_FLAGS, UNIT_FLAG_NON_ATTACKABLE);
                me->SetFlag(UNIT_FIELD_FLAGS, UNIT_FLAG_NOT_SELECTABLE);
            }

            void InitializeAI()
            {
                ScriptedAI::InitializeAI();
                Unit * owner = me->GetOwner();
                if (!owner || owner->GetTypeId() != TYPEID_PLAYER)
                    return;

                me->SetReactState(REACT_PASSIVE);
                me->SetFlag(UNIT_FIELD_FLAGS, UNIT_FLAG_DISABLE_MOVE);
                me->SetFlag(UNIT_FIELD_FLAGS, UNIT_FLAG_NON_ATTACKABLE);
                me->SetFlag(UNIT_FIELD_FLAGS, UNIT_FLAG_NOT_SELECTABLE);

                me->CastSpell(me, FUNGAL_GROWTH_PERIODIC, true);    // Periodic Trigger spell : decrease speed
                me->CastSpell(me, FUNGAL_GROWTH_AREA, true);        // Persistent Area
            }

            void UpdateAI(uint32 diff)
            {
                if (!me->HasAura(FUNGAL_GROWTH_PERIODIC))
                    me->CastSpell(me, FUNGAL_GROWTH_PERIODIC, true);
            }
        };

        CreatureAI* GetAI(Creature* pCreature) const
        {
            return new npc_fungal_growthAI(pCreature);
        }
};

void AddSC_druid_pet_scripts()
{
    new npc_pet_druid_force_of_nature();
    new npc_wild_mushroom();
    new npc_fungal_growth();
}
