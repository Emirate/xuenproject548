/*
 * Copyright (C) 2008-2016 TrinityCore <http://www.trinitycore.org/>
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 3 of the License, or (at your
 * option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program. If not, see <http://www.gnu.org/licenses/>.
 */

/*
 * Ordered alphabetically using scriptname.
 * Scriptnames of files in this file should be prefixed with "npc_pet_pri_".
 */

#include "ScriptMgr.h"
#include "ScriptedCreature.h"
#include "PassiveAI.h"
#include "PetAI.h"

enum PriestSpells
{
    SPELL_PRIEST_GLYPH_OF_SHADOWFIEND       = 58228,
    SPELL_PRIEST_GLYPH_OF_SHADOWFIEND_MANA  = 58227,
    SPELL_PRIEST_LIGHTWELL_CHARGES          = 59907
};

// Lightwell - 64571
class npc_new_lightwell : public CreatureScript
{
    public:
        npc_new_lightwell() : CreatureScript("npc_new_lightwell") { }

        struct npc_new_lightwellAI : public PassiveAI
        {
            npc_new_lightwellAI(Creature* creature) : PassiveAI(creature)
            {
                me->CastSpell(me, 126150, true);
                renewTimer = 1000;
                stacks = false;
				me->ApplySpellImmune(0, IMMUNITY_STATE, SPELL_AURA_OBS_MOD_HEALTH, true);
				me->ApplySpellImmune(0, IMMUNITY_EFFECT, SPELL_EFFECT_HEAL_PCT, true);
				me->ApplySpellImmune(0, IMMUNITY_EFFECT, SPELL_EFFECT_HEAL, true);
				me->ApplySpellImmune(0, IMMUNITY_EFFECT, SPELL_EFFECT_HEALTH_LEECH, true);
				
				me->ApplySpellImmune(0, IMMUNITY_EFFECT, SPELL_EFFECT_KNOCK_BACK, true);
				me->ApplySpellImmune(0, IMMUNITY_EFFECT, SPELL_EFFECT_KNOCK_BACK_DEST, true);
				me->SetFlag(UNIT_FIELD_NPC_FLAGS, UNIT_FLAG_DISABLE_MOVE);
            }

            uint32 renewTimer;
            bool stacks;
			
            void EnterEvadeMode()
            {
                if (!me->IsAlive())
                    return;

                me->DeleteThreatList();
                me->CombatStop(true);
                me->ResetPlayerDamageReq();
            }

            void UpdateAI(const uint32 diff)
            {
                if (!stacks)
                {
                    if (AuraPtr charges = me->GetAura(126150))
                    {
                        if (me->ToTempSummon())
                        {
                            if (Unit* owner = me->ToTempSummon()->GetSummoner())
                            {
                                stacks = true;
                                charges->ModStackAmount(15);
                                // Glyph of Deep Well
                                if (owner->HasAura(55673))
                                    charges->ModStackAmount(17);
                           }
                        }
                    }
                }

                if (renewTimer)
                {
                    if (renewTimer <= diff)
                    {
                        if (me->GetOwner())
                        {
                            if (Player* plr = me->GetOwner()->ToPlayer())
                            {
                                std::list<Unit*> party;
                                std::list<Unit*> tempList;
                                plr->GetPartyMembers(party);

                                for (auto itr : party)
                                {
                                    if (itr->GetHealthPct() >= 50.0f ||
                                        itr->GetDistance(me) >= 40.0f ||
                                        itr->HasAura(126154))
                                        continue;

                                    tempList.push_back(itr);
                                }

                                for (auto itr : tempList)
                                    me->CastSpell(itr, 60123, true);
                            }
                        }

                        renewTimer = 1000;
                    }
                    else
                        renewTimer -= diff;
                }
            }
        };

        CreatureAI* GetAI(Creature* creature) const
        {
            return new npc_new_lightwellAI(creature);
        }
};

class npc_pet_pri_shadowfiend : public CreatureScript
{
    public:
        npc_pet_pri_shadowfiend() : CreatureScript("npc_pet_pri_shadowfiend") { }

        struct npc_pet_pri_shadowfiendAI : public PetAI
        {
            npc_pet_pri_shadowfiendAI(Creature* creature) : PetAI(creature) { }

            void JustDied(Unit* /*killer*/)
            {
                if (me->IsSummon())
                    if (Unit* owner = me->ToTempSummon()->GetSummoner())
                        if (owner->HasAura(SPELL_PRIEST_GLYPH_OF_SHADOWFIEND))
                            owner->CastSpell(owner, SPELL_PRIEST_GLYPH_OF_SHADOWFIEND_MANA, true);
            }
        };

        CreatureAI* GetAI(Creature* creature) const
        {
            return new npc_pet_pri_shadowfiendAI(creature);
        }
};

enum PsyfiendSpells
{
    SPELL_PSYCHIC_TERROR    = 113792,
    SPELL_ROOT_FOR_EVER     = 31366
};

class npc_pet_pri_psyfiend : public CreatureScript
{
    public:
        npc_pet_pri_psyfiend() : CreatureScript("npc_pet_pri_psyfiend") { }

        struct npc_pet_pri_psyfiendAI : public ScriptedAI
        {
            npc_pet_pri_psyfiendAI(Creature* c) : ScriptedAI(c)
            {
                me->SetReactState(REACT_DEFENSIVE);
                targetGUID = 0;
            }

            uint64 targetGUID;
            uint32 psychicHorrorTimer;

            void Reset()
            {
                if (!me->HasAura(SPELL_ROOT_FOR_EVER))
                    me->AddAura(SPELL_ROOT_FOR_EVER, me);

                psychicHorrorTimer = 1500;
            }

            void SetGUID(uint64 guid, int32)
            {
                targetGUID = guid;
            }

            void IsSummonedBy(Unit* owner)
            {
                if (owner && owner->GetTypeId() == TYPEID_PLAYER)
                {
                    me->SetLevel(owner->getLevel());
                    me->SetBaseWeaponDamage(BASE_ATTACK, MINDAMAGE, 0.0f);
                    me->SetBaseWeaponDamage(BASE_ATTACK, MAXDAMAGE, 0.0f);
                    me->AddAura(SPELL_ROOT_FOR_EVER, me);
                }
                else
                    me->DespawnOrUnsummon();
            }

            void UpdateAI(uint32 diff)
            {
                if (psychicHorrorTimer)
                {
                    if (psychicHorrorTimer <= diff)
                    {
                            std::list<Unit*> targets;
                            Trinity::AnyUnfriendlyUnitInObjectRangeCheck u_check(me, me, 20);
                            Trinity::UnitListSearcher<Trinity::AnyUnfriendlyUnitInObjectRangeCheck> searcher(me, targets, u_check);
                            me->VisitNearbyObject(20, searcher);
                            for (std::list<Unit*>::const_iterator iter = targets.begin(); iter != targets.end(); ++iter)
                            {
                                if (!me->HasAuraWithMechanic(MECHANIC_FEAR) &&
                                !me->HasAuraWithMechanic(MECHANIC_STUN) &&
                                !me->HasAuraWithMechanic(MECHANIC_SILENCE) &&
                                !(*iter)->IsTotem() &&
                                !(*iter)->HasAura(SPELL_PSYCHIC_TERROR) &&
                                !(*iter)->HasAuraType(SPELL_AURA_MOD_STEALTH) &&
                                !(*iter)->HasAura(76577) &&
                                (*iter)->IsWithinLOSInMap(me))
                                me->CastSpell((*iter), SPELL_PSYCHIC_TERROR, true);
                                me->Attack((*iter), false);
                            }

                        psychicHorrorTimer = 1500;
                    }
                    else
                        psychicHorrorTimer -= diff;
                }
            }
        };

        CreatureAI* GetAI(Creature *creature) const
        {
            return new npc_pet_pri_psyfiendAI(creature);
        }
};

/*######
## npc_spectral_guise -- 59607
######*/

enum spectralGuiseSpells
{
    SPELL_SPECTRAL_GUISE_CLONE      = 119012,
    SPELL_SPECTRAL_GUISE_CHARGES    = 119030,
    SPELL_SPECTRAL_GUISE_STEALTH    = 119032,
	SPELL_INITIALIZE_IMAGES			= 102284
};

class npc_spectral_guise : public CreatureScript
{
    public:
        npc_spectral_guise() : CreatureScript("npc_spectral_guise") { }

        struct npc_spectral_guiseAI : public Scripted_NoMovementAI
        {
            npc_spectral_guiseAI(Creature* c) : Scripted_NoMovementAI(c)
            {
                me->SetReactState(REACT_PASSIVE);
            }

            void Reset()
            {
                if (!me->HasAura(SPELL_ROOT_FOR_EVER))
                    me->AddAura(SPELL_ROOT_FOR_EVER, me);
            }

            void IsSummonedBy(Unit* owner)
            {
                if (owner && owner->GetTypeId() == TYPEID_PLAYER)
                {
                    me->SetLevel(owner->getLevel());
                    me->SetMaxHealth(owner->GetMaxHealth() / 2);
                    me->SetHealth(me->GetMaxHealth());

                    owner->AddAura(SPELL_INITIALIZE_IMAGES, me);
                    owner->AddAura(SPELL_SPECTRAL_GUISE_CLONE, me);

                    me->CastSpell(me, SPELL_SPECTRAL_GUISE_CHARGES, true);
					const SpellInfo* spellInfo = sSpellMgr->GetSpellInfo(SPELL_SPECTRAL_GUISE_STEALTH);
                    Aura::TryRefreshStackOrCreate(spellInfo, MAX_EFFECT_MASK, owner, owner, spellInfo->spellPower);

                    std::list<HostileReference*> threatList = owner->getThreatManager().getThreatList();
                    for (std::list<HostileReference*>::const_iterator itr = threatList.begin(); itr != threatList.end(); ++itr)
                        if (Unit* unit = (*itr)->getTarget())
                            if (unit->GetTypeId() == TYPEID_UNIT)
                                if (Creature* creature = unit->ToCreature())
                                    if (creature->CanStartAttack(me, false))
                                        creature->Attack(me, true);

                    // Set no damage
                    me->SetBaseWeaponDamage(BASE_ATTACK, MINDAMAGE, 0.0f);
                    me->SetBaseWeaponDamage(BASE_ATTACK, MAXDAMAGE, 0.0f);

                    me->AddAura(SPELL_ROOT_FOR_EVER, me);
                }
                else
                    me->DespawnOrUnsummon();
            }
        };

        CreatureAI* GetAI(Creature *creature) const
        {
            return new npc_spectral_guiseAI(creature);
        }
};

/*######
## npc_void_tendrils -- 65282
######*/

enum voidTendrilsSpells
{
    SPELL_VOID_TENDRILS_ROOT = 108920
};

class npc_void_tendrils : public CreatureScript
{
    public:
        npc_void_tendrils() : CreatureScript("npc_void_tendrils") { }

        struct npc_void_tendrilsAI : public ScriptedAI
        {
            npc_void_tendrilsAI(Creature* c) : ScriptedAI(c)
            {
                me->SetReactState(REACT_AGGRESSIVE);
                targetGUID = 0;
            }

            uint64 targetGUID;

            void Reset()
            {
                if (!me->HasAura(SPELL_ROOT_FOR_EVER))
                    me->AddAura(SPELL_ROOT_FOR_EVER, me);
            }

            void SetGUID(uint64 guid, int32)
            {
                targetGUID = guid;

                me->setFaction(14);
            }

            void JustDied(Unit* killer)
            {
                if (Unit* m_target = ObjectAccessor::FindUnit(targetGUID))
                    m_target->RemoveAura(SPELL_VOID_TENDRILS_ROOT);
            }

            void IsSummonedBy(Unit* owner)
            {
                if (owner && owner->GetTypeId() == TYPEID_PLAYER)
                {
                    me->SetLevel(owner->getLevel());
                    me->SetMaxHealth(owner->CountPctFromMaxHealth(20));
                    me->SetHealth(me->GetMaxHealth());
                    // Set no damage
                    me->SetBaseWeaponDamage(BASE_ATTACK, MINDAMAGE, 0.0f);
                    me->SetBaseWeaponDamage(BASE_ATTACK, MAXDAMAGE, 0.0f);

                    me->AddAura(SPELL_ROOT_FOR_EVER, me);
                }
                else
                    me->DespawnOrUnsummon();
            }

            void UpdateAI(uint32 diff)
            {
                if (!(ObjectAccessor::FindUnit(targetGUID)))
                    me->DespawnOrUnsummon();
            }
        };

        CreatureAI* GetAI(Creature *creature) const
        {
            return new npc_void_tendrilsAI(creature);
        }
};

/*######
# npc_power_word_barrier
######*/

class npc_power_word_barrier : public CreatureScript
{
    public:
        npc_power_word_barrier() : CreatureScript("npc_power_word_barrier") { }

        struct npc_power_word_barrierAI : public ScriptedAI
        {
            npc_power_word_barrierAI(Creature* creature) : ScriptedAI(creature)
            {
                Unit* owner = creature->GetOwner();

                if (owner)
                {
                    creature->CastSpell(creature, 115725, true); // Barrier visual
                    creature->CastSpell(creature, 81781, true);  // Periodic Trigger Spell
                }
            }

            void UpdateAI(uint32 diff)
            {
                Unit* owner = me->GetOwner();

                if (!owner)
                    return;

                if (!me->HasAura(115725))
                    me->CastSpell(me, 115725, true);
                if (!me->HasAura(81781))
                    me->CastSpell(me, 81781, true);
            }
        };

        CreatureAI* GetAI(Creature* creature) const
        {
            return new npc_power_word_barrierAI(creature);
        }
};

void AddSC_priest_pet_scripts()
{
    new npc_new_lightwell();
    new npc_pet_pri_shadowfiend();
    new npc_pet_pri_psyfiend();
    new npc_spectral_guise();
    new npc_void_tendrils();
    new npc_power_word_barrier();
}
