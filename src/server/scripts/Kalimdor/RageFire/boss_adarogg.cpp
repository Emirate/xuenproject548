/*
 * Copyright (C) 2008-2016 TrinityCore <http://www.trinitycore.org/>
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 3 of the License, or (at your
 * option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#include "ScriptMgr.h"
#include "ScriptedCreature.h"

enum Spells
{
    SPELL_INFERNO_CHARGE           = 119405,
    SPELL_INFERNO_CHARGE_TRIGGERED = 119299,
    SPELL_FLAME_BREATH             = 119420
};

enum Events
{
    EVENT_FLAME_BREATH,
    EVENT_INFERNO
};

class boss_adarogg : public CreatureScript
{
    public:
        boss_adarogg() : CreatureScript("boss_adarogg") { }

        struct boss_adaroggAI : public ScriptedAI
        {
            boss_adaroggAI(Creature* creature) : ScriptedAI(creature) { }

            void Reset() { }

            void EnterCombat(Unit* /*who*/)
            {
                events.ScheduleEvent(EVENT_FLAME_BREATH, 5000);
                events.ScheduleEvent(EVENT_INFERNO, 15000);
            }

            void JustDied(Unit* /*killer*/) { }

            void UpdateAI(uint32 diff)
            {
                if(!UpdateVictim())
                    return;

                events.Update(diff);

                if(me->HasUnitState(UNIT_STATE_CASTING))
                    return;

                if(uint32 eventId = events.ExecuteEvent())
                {
                    switch(eventId)
                    {
                        case EVENT_FLAME_BREATH:
                            DoCastVictim(SPELL_FLAME_BREATH);
                            events.ScheduleEvent(EVENT_FLAME_BREATH, 10*IN_MILLISECONDS);
                            break;
                        case EVENT_INFERNO:
                            DoCastRandom(SPELL_INFERNO_CHARGE, 100);
                            events.ScheduleEvent(EVENT_INFERNO, 18*IN_MILLISECONDS);
                            break;
                        default:
                            break;
                    }
                }
                DoMeleeAttackIfReady();
            }
        };

        CreatureAI* GetAI(Creature* creature) const
        {
            return new boss_adaroggAI(creature);
        }
};

class spell_inferno_charge : public SpellScriptLoader
{
    public:
        spell_inferno_charge() : SpellScriptLoader("spell_inferno_charge") { }

        class spell_inferno_charge_AuraScript : public AuraScript
        {
            PrepareAuraScript(spell_inferno_charge_AuraScript);

            void HandleTriggerSpell(constAuraEffectPtr aurEff)
            {
                PreventDefaultAction();
                Unit* caster = GetCaster();
                caster->CastSpell(caster, SPELL_INFERNO_CHARGE_TRIGGERED, false);
            }

            void Register()
            {
                OnEffectPeriodic += AuraEffectPeriodicFn(spell_inferno_charge_AuraScript::HandleTriggerSpell, EFFECT_0, SPELL_AURA_PERIODIC_TRIGGER_SPELL);
            }
        };

        AuraScript* GetAuraScript() const
        {
            return new spell_inferno_charge_AuraScript();
        }
};


void AddSC_boss_adarogg()
{
    new boss_adarogg();
    new spell_inferno_charge();
}
