/*
 * Copyright (C) 2008-2016 TrinityCore <http://www.trinitycore.org/>
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 3 of the License, or (at your
 * option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#include "ScriptMgr.h"
#include "ScriptedCreature.h"

enum Spells
{
    SPELL_LAVA_SPIT = 119434,
    SPELL_SUBMERGE  = 120384
};

enum Events
{
    EVENT_LAVA_SPIT,
    EVENT_SUBMERGE
};

class boss_slagmaw : public CreatureScript
{
    public:
        boss_slagmaw() : CreatureScript("boss_slagmaw") { }

        struct boss_slagmawAI : public ScriptedAI
        {
            boss_slagmawAI(Creature* creature) : ScriptedAI(creature)
            {
                me->SetFlag(UNIT_FIELD_FLAGS, UNIT_FLAG_NON_ATTACKABLE | UNIT_FLAG_NOT_SELECTABLE);
            }

            void Reset() { }

            void EnterCombat(Unit* /*who*/) { }

            void JustDied(Unit* /*who*/)
            {
                me->RemoveFlag(UNIT_FIELD_FLAGS, UNIT_FLAG_NON_ATTACKABLE | UNIT_FLAG_NOT_SELECTABLE);
                me->SetFlag(OBJECT_FIELD_DYNAMIC_FLAGS, UNIT_DYNFLAG_LOOTABLE);
            }

            void AttackStart(Unit* target)
            {
                if (me->Attack(target, true))
                    DoStartNoMovement(target);
            }

            void UpdateAI(uint32 diff)
            {

                if(me->HasUnitState(UNIT_STATE_CASTING))
                    return;
            }
        };

        CreatureAI* GetAI(Creature* creature) const
        {
            return new boss_slagmawAI(creature);
        }
};

class boss_slagmaw_head : public CreatureScript
{
    public:
        boss_slagmaw_head() : CreatureScript("boss_slagmaw_head") { }

        struct boss_slagmaw_headAI : public ScriptedAI
        {
            boss_slagmaw_headAI(Creature* creature) : ScriptedAI(creature) { }

            void Reset() { }

            void EnterCombat(Unit* /*who*/)
            {
                events.ScheduleEvent(EVENT_LAVA_SPIT, 8000);
            }

            void JustDied(Unit* /*killer*/)
            {
                if (Creature* SlagmawBody = me->FindNearestCreature(61463, 50.0f))
                    me->Kill(SlagmawBody);

                me->DespawnOrUnsummon(500);
            }

            void UpdateAI(uint32 diff)
            {
                if(!UpdateVictim())
                    return;

                events.Update(diff);

                if(me->HasUnitState(UNIT_STATE_CASTING))
                    return;

                if(uint32 eventId = events.ExecuteEvent())
                {
                    switch(eventId)
                    {
                        case EVENT_LAVA_SPIT:
                            DoCastVictim(SPELL_LAVA_SPIT);
                            events.ScheduleEvent(EVENT_LAVA_SPIT, 5*IN_MILLISECONDS);
                            break;
                        default:
                            break;
                    }
                }
            }
        };

        CreatureAI* GetAI(Creature* creature) const
        {
            return new boss_slagmaw_headAI(creature);
        }
};

void AddSC_boss_slagmaw()
{
    new boss_slagmaw();
    new boss_slagmaw_head();
}
