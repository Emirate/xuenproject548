/*
 * Copyright (C) 2008-2016 TrinityCore <http://www.trinitycore.org/>
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 3 of the License, or (at your
 * option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#include "ScriptMgr.h"
#include "ScriptedCreature.h"

enum Spells
{
    SPELL_SHADOW_STORM     = 119971,
    SPELL_TWISTED_ELEMENTS = 119300
};

enum Events
{
    EVENT_STORM,
    EVENT_ELEMENTS
};

class boss_koranthal : public CreatureScript
{
    public:
        boss_koranthal() : CreatureScript("boss_koranthal") { }

        struct boss_koranthalAI : public ScriptedAI
        {
            boss_koranthalAI(Creature* creature) : ScriptedAI(creature) { }

            void Reset() { }

            void EnterCombat(Unit* /*who*/)
            {
                events.ScheduleEvent(EVENT_STORM, 10000);
                events.ScheduleEvent(EVENT_ELEMENTS, 8000);
            }

            void JustDied(Unit* /*killer*/) { }

            void UpdateAI(uint32 diff)
            {
                if(!UpdateVictim())
                    return;

                events.Update(diff);

                if(me->HasUnitState(UNIT_STATE_CASTING))
                    return;

                if(uint32 eventId = events.ExecuteEvent())
                {
                    switch(eventId)
                    {
                        case EVENT_STORM:
                            DoCast(me, SPELL_SHADOW_STORM, false);
                            events.ScheduleEvent(EVENT_STORM, 15*IN_MILLISECONDS);
                            break;
                        case EVENT_ELEMENTS:
                            DoCastVictim(SPELL_TWISTED_ELEMENTS);
                            events.ScheduleEvent(EVENT_ELEMENTS, 8*IN_MILLISECONDS);
                            break;
                        default:
                            break;
                    }
                }
                DoMeleeAttackIfReady();
            }
        };

        CreatureAI* GetAI(Creature* creature) const
        {
            return new boss_koranthalAI(creature);
        }
};

void AddSC_boss_koranthal()
{
    new boss_koranthal();
}
