/*
 * Copyright (C) 2008-2016 TrinityCore <http://www.trinitycore.org/>
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 3 of the License, or (at your
 * option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#include "ScriptMgr.h"
#include "ScriptedCreature.h"
#include "wailing_caverns.h"

enum Spells
{
    SPELL_DRUIDS_SLUMBER =  8040,
    SPELL_HEALING_TOUCH  = 11431,
    SPELL_LIGHTNING_BOLT = 20805,
    SPELL_THUNDERCLAP    =  8147
};

enum Events
{
    EVENT_DRUIDS_SLUMBER = 0,
    EVENT_HEALING_TOUCH  = 1,
    EVENT_LIGHTNING_BOLT = 2,
    EVENT_THUNDERCLAP    = 3
};

enum Texts
{
    SAY_AGGRO    = 0
};

class boss_lord_pythas : public CreatureScript
{
    public:
        boss_lord_pythas() : CreatureScript("boss_lord_pythas") { }

        struct boss_lord_pythasAI : public BossAI
        {
            boss_lord_pythasAI(Creature* creature) : BossAI(creature, TYPE_LORD_PYTHAS) { }

            void Reset()
            {
                _Reset();
                thunder = false;
                heal   = false;
            }

            void EnterCombat(Unit* /*who*/)
            {
                _EnterCombat();
                Talk(SAY_AGGRO);
                events.ScheduleEvent(EVENT_LIGHTNING_BOLT, 500);
                events.ScheduleEvent(EVENT_DRUIDS_SLUMBER, 10000);
            }

            void DamageTaken(Unit* /*attacker*/, uint32& /*damage*/)
            {
                if(!thunder && !me->IsNonMeleeSpellCasted(false) && HealthBelowPct(85))
                {
                    thunder = true;
                    events.RescheduleEvent(EVENT_THUNDERCLAP, 500);
                }
                if(!heal && !me->IsNonMeleeSpellCasted(false) && HealthBelowPct(50))
                {
                    heal = true;
                    events.ScheduleEvent(EVENT_HEALING_TOUCH, 500);
                }
            }

            void JustDied(Unit* /*killer*/)
            {
                _JustDied();
            }

            void UpdateAI(uint32 diff)
            {
                if(!UpdateVictim())
                    return;

                events.Update(diff);

                if(me->HasUnitState(UNIT_STATE_CASTING))
                    return;

                if(uint32 eventId = events.ExecuteEvent())
                {
                    switch(eventId)
                    {
                        case EVENT_LIGHTNING_BOLT:
                            DoCastVictim(SPELL_LIGHTNING_BOLT);
                            events.ScheduleEvent(EVENT_LIGHTNING_BOLT, 4000);
                            break;
                        case EVENT_DRUIDS_SLUMBER:
                            if (Unit* target = SelectTarget(SELECT_TARGET_RANDOM, 0, NonTankTargetSelector(me)))
                                DoCast(target, SPELL_DRUIDS_SLUMBER);
                            events.ScheduleEvent(EVENT_DRUIDS_SLUMBER, 20000);
                            break;
                        case EVENT_HEALING_TOUCH:
                            DoCast(me, SPELL_HEALING_TOUCH);
                            events.ScheduleEvent(EVENT_HEALING_TOUCH, urand(18000, 20000));
                            break;
                        case EVENT_THUNDERCLAP:
                            DoCast(me, SPELL_THUNDERCLAP);
                            events.ScheduleEvent(EVENT_THUNDERCLAP, 60000);
                            break;
                        default:
                            break;
                    }
                }
                DoMeleeAttackIfReady();
            }
    private:
        bool thunder;
        bool heal;
        };

        CreatureAI* GetAI(Creature* creature) const
        {
            return new boss_lord_pythasAI(creature);
        }
};

void AddSC_boss_lord_pythas()
{
    new boss_lord_pythas();
}
