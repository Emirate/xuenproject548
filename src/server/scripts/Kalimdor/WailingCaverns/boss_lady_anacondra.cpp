/*
 * Copyright (C) 2008-2016 TrinityCore <http://www.trinitycore.org/>
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 3 of the License, or (at your
 * option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#include "ScriptMgr.h"
#include "ScriptedCreature.h"
#include "wailing_caverns.h"

enum Spells
{
    SPELL_DRUIDS_SLUMBER =  8040,
    SPELL_HEALING_TOUCH  = 23381,
    SPELL_LIGHTNING_BOLT = 20295,
    SPELL_THORNS_AURA    =  8148
};

enum Events
{
    EVENT_DRUIDS_SLUMBER = 0,
    EVENT_HEALING_TOUCH  = 1,
    EVENT_LIGHTNING_BOLT = 2,
    EVENT_THORNS_AURA    = 3
};

enum Texts
{
    SAY_AGGRO    = 0
};

class boss_lady_anacondra : public CreatureScript
{
    public:
        boss_lady_anacondra() : CreatureScript("boss_lady_anacondra") { }

        struct boss_lady_anacondraAI : public BossAI
        {
            boss_lady_anacondraAI(Creature* creature) : BossAI(creature, TYPE_LADY_ANACONDRA) { }

            void Reset()
            {
                _Reset();
                thorns = false;
                heal   = false;
            }

            void EnterCombat(Unit* /*who*/)
            {
                _EnterCombat();
                Talk(SAY_AGGRO);
                events.ScheduleEvent(EVENT_LIGHTNING_BOLT, 500);
                events.ScheduleEvent(EVENT_DRUIDS_SLUMBER, 10000);
            }

            void DamageTaken(Unit* /*attacker*/, uint32& /*damage*/)
            {
                if(!thorns && !me->IsNonMeleeSpellCasted(false) && HealthBelowPct(85))
                {
                    thorns = true;
                    events.RescheduleEvent(EVENT_THORNS_AURA, 500);
                }
                if(!heal && !me->IsNonMeleeSpellCasted(false) && HealthBelowPct(50))
                {
                    heal = true;
                    events.ScheduleEvent(EVENT_HEALING_TOUCH, 500);
                }
            }

            void JustDied(Unit* /*killer*/)
            {
                _JustDied();
            }

            void UpdateAI(uint32 diff)
            {
                if(!UpdateVictim())
                    return;

                events.Update(diff);

                if(me->HasUnitState(UNIT_STATE_CASTING))
                    return;

                if(uint32 eventId = events.ExecuteEvent())
                {
                    switch(eventId)
                    {
                        case EVENT_LIGHTNING_BOLT:
                            DoCastVictim(SPELL_LIGHTNING_BOLT);
                            events.ScheduleEvent(EVENT_LIGHTNING_BOLT, 4000);
                            break;
                        case EVENT_DRUIDS_SLUMBER:
                            if (Unit* target = SelectTarget(SELECT_TARGET_RANDOM, 0, NonTankTargetSelector(me)))
                                DoCast(target, SPELL_DRUIDS_SLUMBER);
                            events.ScheduleEvent(EVENT_DRUIDS_SLUMBER, 20000);
                            break;
                        case EVENT_HEALING_TOUCH:
                            DoCast(me, SPELL_HEALING_TOUCH);
                            events.ScheduleEvent(EVENT_HEALING_TOUCH, urand(18000, 20000));
                            break;
                        case EVENT_THORNS_AURA:
                            DoCast(me, SPELL_THORNS_AURA);
                            events.ScheduleEvent(EVENT_THORNS_AURA, 60000);
                            break;
                        default:
                            break;
                    }
                }
                DoMeleeAttackIfReady();
            }
    private:
        bool thorns;
        bool heal;
        };

        CreatureAI* GetAI(Creature* creature) const
        {
            return new boss_lady_anacondraAI(creature);
        }
};

void AddSC_boss_lady_anacondra()
{
    new boss_lady_anacondra();
}
