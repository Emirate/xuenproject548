/*
 * Copyright (C) 2008-2016 TrinityCore <http://www.trinitycore.org/>
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 3 of the License, or (at your
 * option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef OPENSSL_CRYPTO_H
#define OPENSSL_CRYPTO_H

/**
* A group of functions which setup OpenSSL crypto module to work properly in multithreaded enviroment.
* If it isn't set up properly - it will crash
*/
namespace OpenSSLCrypto
{
    /// Needs to be called before threads using OpenSSL are spawned
    void threadsSetup();
    /// Needs to be called after threads using OpenSSL are despawned
    void threadsCleanup();
}

#endif